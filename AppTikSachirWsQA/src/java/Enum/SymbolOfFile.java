/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Enum;

/**
 *
 * @author rachel
 */
public enum SymbolOfFile {

    success("0"),
    null1("800"),
    null2("801");

//   a1("קורות חיים"),
//    2("אישור"),
//    3("");
    private String value;

    private SymbolOfFile(String value) {
        this.value = value;
    }

    public int getValue() {
        return Integer.parseInt(value);
    }
}
