/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import Entity.ManagementUsers;
import Entity.ManagementUsersFacade;
import java.io.IOException;
import java.util.Base64;
import java.util.StringTokenizer;
import javax.ejb.EJB;

/**
 *
 * @author rivka
 */
public class AuthenticationService {

    ManagementUsersFacade usersFacade;
    AuthenticationService(ManagementUsersFacade usersFacade) {
        this.usersFacade = usersFacade;
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public ManagementUsers authenticate(String authCredentials) {


        final String encodedUserPassword = authCredentials.replaceFirst("Basic"
                + " ", "");
        String usernameAndPassword = null;
        try {
            byte[] decodedBytes = Base64.getDecoder().decode(
                    encodedUserPassword);
            usernameAndPassword = new String(decodedBytes, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        final StringTokenizer tokenizer = new StringTokenizer(
                usernameAndPassword, ":");
        final String username = tokenizer.nextToken();
        final String password = tokenizer.nextToken();
//        final String pincode = tokenizer.nextToken();
        System.out.println("Rachel :username "+ username+"  ; password "+ password); 
        
        ManagementUsers user = usersFacade.getUserDetails(username, password);

        return user;
    }

}
