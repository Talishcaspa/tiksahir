/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AttendanceObjectForJson;

/**
 *
 * @author rachel
 */
public class JsonInsertAbsence {

    public JsonInsertAbsence() {
    }

    public JsonInsertAbsence(int absenceType, double latitude, double longitude) {
        this.absenceType = absenceType;
        this.latitude = latitude;
        this.longitude = longitude;
    }
    
    int absenceType;
    double latitude;
    double longitude;

    public int getAbsenceType() {
        return absenceType;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setAbsenceType(int absenceType) {
        this.absenceType = absenceType;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
    
    
}
