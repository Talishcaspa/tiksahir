/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AttendanceEntity;

import Entity.AbstractFacade;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author rivka
 */
@Stateless
public class AttendanceAbsenteeismFacade extends AbstractFacade<AttendanceAbsenteeism> {

    @PersistenceContext(unitName = "AppTikSachirWsPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AttendanceAbsenteeismFacade() {
        super(AttendanceAbsenteeism.class);
    }
        public List<AttendanceAbsenteeism> getAttendanceAbsenteeism(){

//    public AttendanceAbsenteeism getAttendanceAbsenteeism(){
        return em.createNamedQuery("AttendanceAbsenteeism.findAll").getResultList();  

    }
}
