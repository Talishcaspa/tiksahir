/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AttendanceEntity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rivka
 */
@Entity
@Table(name = "app.attendance")
@XmlRootElement
@NamedQueries({
      @NamedQuery(name = "Attendance.findAll", query = "SELECT a FROM Attendance a")
    , @NamedQuery(name = "Attendance.findByInId", query = "SELECT a FROM Attendance a WHERE a.inId = :inId")
    , @NamedQuery(name = "Attendance.findByUser", query = "SELECT a FROM Attendance a WHERE a.user = :user")
    , @NamedQuery(name = "Attendance.findByDate", query = "SELECT a FROM Attendance a WHERE a.date BETWEEN :fromD AND :toD")
    , @NamedQuery(name = "Attendance.findByDayname", query = "SELECT a FROM Attendance a WHERE a.dayname = :dayname")
    , @NamedQuery(name = "Attendance.findByStartTime", query = "SELECT a FROM Attendance a WHERE a.startTime = :startTime")
    , @NamedQuery(name = "Attendance.findByEndTime", query = "SELECT a FROM Attendance a WHERE a.endTime = :endTime")
    , @NamedQuery(name = "Attendance.findByType", query = "SELECT a FROM Attendance a WHERE a.type = :type")
    , @NamedQuery(name = "Attendance.findByAbsenteeism", query = "SELECT a FROM Attendance a WHERE a.absenteeism = :absenteeism")
    , @NamedQuery(name = "Attendance.findBySumForDay", query = "SELECT a FROM Attendance a WHERE a.sumForDay = :sumForDay")
    , @NamedQuery(name = "Attendance.findBySumFor100", query = "SELECT a FROM Attendance a WHERE a.sumFor100 = :sumFor100")
    , @NamedQuery(name = "Attendance.findBySumFor125", query = "SELECT a FROM Attendance a WHERE a.sumFor125 = :sumFor125")
    , @NamedQuery(name = "Attendance.findBySumFor150", query = "SELECT a FROM Attendance a WHERE a.sumFor150 = :sumFor150")
    , @NamedQuery(name = "Attendance.findBySumFor200", query = "SELECT a FROM Attendance a WHERE a.sumFor200 = :sumFor200")
    , @NamedQuery(name = "Attendance.findBySource", query = "SELECT a FROM Attendance a WHERE a.source = :source")
    , @NamedQuery(name = "Attendance.findByTihnounYomi", query = "SELECT a FROM Attendance a WHERE a.tihnounYomi = :tihnounYomi")
    , @NamedQuery(name = "Attendance.findByTakzirEtmol", query = "SELECT a FROM Attendance a WHERE a.takzirEtmol = :takzirEtmol")
    , @NamedQuery(name = "Attendance.findByPathFile", query = "SELECT a FROM Attendance a WHERE a.pathFile = :pathFile")
    , @NamedQuery(name = "Attendance.findByUserDate", query = "SELECT a FROM Attendance a WHERE a.user = :user AND a.date = :date")
    , @NamedQuery(name = "Attendance.findByGroupByAttendanceDates", query = "SELECT a FROM Attendance a WHERE a.date BETWEEN :fromD AND :toD GROUP BY a.date")
    ,@NamedQuery(name = "Attendance.findByUserDates", query = "SELECT a FROM Attendance a WHERE a.user = :user AND a.date BETWEEN :fromD AND :toD AND a.endTime <> '00:00:00'")})

public class Attendance implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "in_id")
    private Integer inId;
//    @Basic(optional = false)
//    @NotNull
//    @Size(min = 1, max = 50)
//    @Column(name = "user")
//    private String user;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "dayname")
    private String dayname;
    @Basic(optional = false)
    @NotNull
    @Column(name = "start_time")
    @Temporal(TemporalType.TIME)
    private Date startTime;
    @Basic(optional = false)
    @NotNull
    @Column(name = "end_time")
    @Temporal(TemporalType.TIME)
    private Date endTime;
    @Basic(optional = false)
    @NotNull
    @Column(name = "type")
    private int type;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "absenteeism")
    private String absenteeism;
    @Basic(optional = false)
    @NotNull
    @Column(name = "sum_for_day")
    @Temporal(TemporalType.TIME)
    private Date sumForDay;
    @Basic(optional = false)
    @NotNull
    @Column(name = "sum_for_100")
    @Temporal(TemporalType.TIME)
    private Date sumFor100;
    @Basic(optional = false)
    @NotNull
    @Column(name = "sum_for_125")
    @Temporal(TemporalType.TIME)
    private Date sumFor125;
    @Basic(optional = false)
    @NotNull
    @Column(name = "sum_for_150")
    @Temporal(TemporalType.TIME)
    private Date sumFor150;
    @Basic(optional = false)
    @NotNull
    @Column(name = "sum_for_200")
    @Temporal(TemporalType.TIME)
    private Date sumFor200;
    @Size(max = 45)
    @Column(name = "source")
    private String source;
    @Size(max = 200)
    @Column(name = "tihnoun_yomi")
    private String tihnounYomi;
    @Size(max = 200)
    @Column(name = "takzir_etmol")
    private String takzirEtmol;
    @Size(max = 45)
    @Column(name = "path_file")
    private String pathFile;
    
    @ManyToOne(cascade={CascadeType.ALL})
    @JoinColumn(name = "user", referencedColumnName = "user_name")
    private ManagementUsersAttendance user;

    public Attendance() {
    }
    
    public Attendance(Date d) {
        this.date = d;
    }

    public Attendance(Integer inId) {
        this.inId = inId;
    }

    public Attendance(Integer inId, String user, Date date, String dayname, Date startTime, Date endTime, int type, String absenteeism, Date sumForDay, Date sumFor100, Date sumFor125, Date sumFor150, Date sumFor200) {
        this.inId = inId;
//        this.user = user;
        this.date = date;
        this.dayname = dayname;
        this.startTime = startTime;
        this.endTime = endTime;
        this.type = type;
        this.absenteeism = absenteeism;
        this.sumForDay = sumForDay;
        this.sumFor100 = sumFor100;
        this.sumFor125 = sumFor125;
        this.sumFor150 = sumFor150;
        this.sumFor200 = sumFor200;
    }
    
    public Attendance( Date date,  Date startTime, Date endTime, Date sumFor100, Date sumFor125) {
    
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.sumFor100 = sumFor100;
        this.sumFor125 = sumFor125;
    }
    public Integer getInId() {
        return inId;
    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }

    public ManagementUsersAttendance getUser() {
        return user;
    }

    public void setUser(ManagementUsersAttendance user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDayname() {
        return dayname;
    }

    public void setDayname(String dayname) {
        this.dayname = dayname;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getAbsenteeism() {
        return absenteeism;
    }

    public void setAbsenteeism(String absenteeism) {
        this.absenteeism = absenteeism;
    }

    public Date getSumForDay() {
        return sumForDay;
    }

    public void setSumForDay(Date sumForDay) {
        this.sumForDay = sumForDay;
    }

    public Date getSumFor100() {
        return sumFor100;
    }

    public void setSumFor100(Date sumFor100) {
        this.sumFor100 = sumFor100;
    }

    public Date getSumFor125() {
        return sumFor125;
    }

    public void setSumFor125(Date sumFor125) {
        this.sumFor125 = sumFor125;
    }

    public Date getSumFor150() {
        return sumFor150;
    }

    public void setSumFor150(Date sumFor150) {
        this.sumFor150 = sumFor150;
    }

    public Date getSumFor200() {
        return sumFor200;
    }

    public void setSumFor200(Date sumFor200) {
        this.sumFor200 = sumFor200;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTihnounYomi() {
        return tihnounYomi;
    }

    public void setTihnounYomi(String tihnounYomi) {
        this.tihnounYomi = tihnounYomi;
    }

    public String getTakzirEtmol() {
        return takzirEtmol;
    }

    public void setTakzirEtmol(String takzirEtmol) {
        this.takzirEtmol = takzirEtmol;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inId != null ? inId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Attendance)) {
            return false;
        }
        Attendance other = (Attendance) object;
        if ((this.inId == null && other.inId != null) || (this.inId != null && !this.inId.equals(other.inId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Attendance[ inId=" + inId + " ]";
    }
    
}
