/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AttendanceEntity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rivka
 */
@Entity
@Table(name = "erp.sug_message_contacts")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SugMessageContacts.findAll", query = "SELECT s FROM SugMessageContacts s")
    , @NamedQuery(name = "SugMessageContacts.findByInId", query = "SELECT s FROM SugMessageContacts s WHERE s.inId = :inId")
    , @NamedQuery(name = "SugMessageContacts.findBySugMessageInid", query = "SELECT s FROM SugMessageContacts s WHERE s.sugMessageInid = :sugMessageInid")
    , @NamedQuery(name = "SugMessageContacts.findByEboxContactInid", query = "SELECT s FROM SugMessageContacts s WHERE s.eboxContactInid = :eboxContactInid")})
public class SugMessageContacts implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "in_id")
    private Integer inId;
    
    @ManyToOne(cascade={CascadeType.MERGE,CascadeType.REMOVE})
    @JoinColumn(name = "sug_message_inid", referencedColumnName = "in_id")
    private SugMessage sugMessageInid;
    
    @ManyToOne(cascade={CascadeType.MERGE,CascadeType.REMOVE})
    @JoinColumn(name = "ebox_contact_inid", referencedColumnName = "in_id")
    private Contacts eboxContactInid;
    
//    @Basic(optional = false)
//    @NotNull
//    @Column(name = "sug_message_inid")
//    private int sugMessageInid;
    
//    @Basic(optional = false)
//    @NotNull
//    @Column(name = "ebox_contact_inid")
//    private int eboxContactInid;

    public SugMessageContacts() {
    }

    public SugMessageContacts(Integer inId) {
        this.inId = inId;
    }

    public SugMessageContacts(Integer inId, SugMessage sugMessageInid, Contacts eboxContactInid) {
        this.inId = inId;
        this.sugMessageInid = sugMessageInid;
        this.eboxContactInid = eboxContactInid;
    }

    public Integer getInId() {
        return inId;
    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }

//    public int getSugMessageInid() {
//        return sugMessageInid;
//    }
//
//    public void setSugMessageInid(int sugMessageInid) {
//        this.sugMessageInid = sugMessageInid;
//    }
//
//    public int getEboxContactInid() {
//        return eboxContactInid;
//    }
//
//    public void setEboxContactInid(int eboxContactInid) {
//        this.eboxContactInid = eboxContactInid;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inId != null ? inId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SugMessageContacts)) {
            return false;
        }
        SugMessageContacts other = (SugMessageContacts) object;
        if ((this.inId == null && other.inId != null) || (this.inId != null && !this.inId.equals(other.inId))) {
            return false;
        }
        return true;
    }

    public SugMessage getSugMessageInid() {
        return sugMessageInid;
    }

    public void setSugMessageInid(SugMessage sugMessageInid) {
        this.sugMessageInid = sugMessageInid;
    }

    public Contacts getEboxContactInid() {
        return eboxContactInid;
    }

    public void setEboxContactInid(Contacts eboxContactInid) {
        this.eboxContactInid = eboxContactInid;
    }

    @Override
    public String toString() {
        return "entities.SugMessageContacts[ inId=" + inId + " ]";
    }
    
}
