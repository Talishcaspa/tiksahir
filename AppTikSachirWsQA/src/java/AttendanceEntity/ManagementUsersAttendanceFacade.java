/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AttendanceEntity;

import Entity.AbstractFacade;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author rivka
 */
@Stateless
public class ManagementUsersAttendanceFacade extends AbstractFacade<ManagementUsersAttendance> {

    @PersistenceContext(unitName = "AppTikSachirWsPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ManagementUsersAttendanceFacade() {
        super(ManagementUsersAttendance.class);
    }

    public int checkUserDetails(String userName, String userPass, String pinCode) {

        List<ManagementUsersAttendance> managementUsersList = em.createNamedQuery("ManagementUsers.findByUserAndPinCode").setParameter("userName", userName).setParameter("pinCode", pinCode).getResultList();

        if (managementUsersList.size() > 0) {
            try {
                ManagementUsersAttendance user = managementUsersList.get(0);
                String md5 = null;

                MessageDigest md = MessageDigest.getInstance("MD5");
                md.update(userPass.getBytes());
                byte[] digest = md.digest();
                md5 = new BigInteger(1, digest).toString(16);

                if (md5.equals(user.getUserPass())) {
                    return user.getInId();
//                    return user.getInId().orElse(0); // my ass or else

                }
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(ManagementUsersAttendanceFacade.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return -1;//there is no such a user

    }

    public ManagementUsersAttendance getUserDetails(String userName, String userPass, String pinCode) {

        List<ManagementUsersAttendance> managementUsersList = em.createNamedQuery("ManagementUsers.findByUserAndPinCode").setParameter("userName", userName).setParameter("pinCode", pinCode).getResultList();

        ManagementUsersAttendance user = new ManagementUsersAttendance();
        user.setInId(-1);
        if (managementUsersList.size() > 0) {
            try {
                user = managementUsersList.get(0);
                String md5 = null;

                MessageDigest md = MessageDigest.getInstance("MD5");
                md.update(userPass.getBytes());
                byte[] digest = md.digest();
                md5 = new BigInteger(1, digest).toString(16);

//                if (md5.equals(user.getUserPass())) {
//                    return user;
//                }
                if (!md5.equals(user.getUserPass())) {
                    ManagementUsersAttendance user1 = new ManagementUsersAttendance();
                    user1.setInId(-1);

                    return user1;
                }
            

        }catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(ManagementUsersAttendanceFacade.class.getName()).log(Level.SEVERE, null, ex);
            }

    }
    return user ;
}

}
