/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AttendanceEntity;

import Entity.AbstractFacade;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author rivka
 */
@Stateless
public class SendMailTamplateFacade extends AbstractFacade<SendMailTamplate> {

    @PersistenceContext(unitName = "AppTikSachirWsPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SendMailTamplateFacade() {
        super(SendMailTamplate.class);
    }
    
    public SendMailTamplate getMailTemplate(){
         return (SendMailTamplate) em.createNamedQuery("SendMailTamplate.findByTemplateName").setParameter("templateName", "send_mail_update_attendance").getResultList().get(0);
    }
}
