/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AttendanceEntity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rivka
 */
@Entity
@Table(name = "app.lat_long_user")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LatLongUser.findAll", query = "SELECT l FROM LatLongUser l")
    , @NamedQuery(name = "LatLongUser.findByInId", query = "SELECT l FROM LatLongUser l WHERE l.inId = :inId")
    , @NamedQuery(name = "LatLongUser.findByInIdUser", query = "SELECT l FROM LatLongUser l WHERE l.inIdUser = :inIdUser")
    , @NamedQuery(name = "LatLongUser.findByLatitude", query = "SELECT l FROM LatLongUser l WHERE l.latitude = :latitude")
    , @NamedQuery(name = "LatLongUser.findByLongitude", query = "SELECT l FROM LatLongUser l WHERE l.longitude = :longitude")
    , @NamedQuery(name = "LatLongUser.findByTime", query = "SELECT l FROM LatLongUser l WHERE l.time = :time")
    , @NamedQuery(name = "LatLongUser.findByLocation", query = "SELECT l FROM LatLongUser l WHERE l.location = :location")})
public class LatLongUser implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "in_id")
    private Integer inId;
//    @Basic(optional = false)
//    @NotNull
//    @Column(name = "in_id_user")
//    private int inIdUser;
    @Basic(optional = false)
    @NotNull
    @Column(name = "latitude")
    private double latitude;
    @Basic(optional = false)
    @NotNull
    @Column(name = "longitude")
    private double longitude;
    @Column(name = "_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date time;
    @Size(max = 100)
    @Column(name = "location")
    private String location;
    
    @ManyToOne(cascade={CascadeType.ALL})
    @JoinColumn(name = "in_id_user", referencedColumnName = "in_id")
    private ManagementUsersAttendance inIdUser;

    public LatLongUser() {
    }

    public LatLongUser(Integer inId) {
        this.inId = inId;
    }

    public LatLongUser(Integer inId, int inIdUser, double latitude, double longitude) {
        this.inId = inId;
//        this.inIdUser = inIdUser;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Integer getInId() {
        return inId;
    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }

    public ManagementUsersAttendance getInIdUser() {
        return inIdUser;
    }

    public void setInIdUser(ManagementUsersAttendance inIdUser) {
        this.inIdUser = inIdUser;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inId != null ? inId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LatLongUser)) {
            return false;
        }
        LatLongUser other = (LatLongUser) object;
        if ((this.inId == null && other.inId != null) || (this.inId != null && !this.inId.equals(other.inId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.LatLongUser[ inId=" + inId + " ]";
    }
    
}
