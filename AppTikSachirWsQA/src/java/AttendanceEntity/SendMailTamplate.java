/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AttendanceEntity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rivka
 */
@Entity
@Table(name = "erp.send_mail_tamplate")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SendMailTamplate.findAll", query = "SELECT s FROM SendMailTamplate s")
    , @NamedQuery(name = "SendMailTamplate.findByInId", query = "SELECT s FROM SendMailTamplate s WHERE s.inId = :inId")
    , @NamedQuery(name = "SendMailTamplate.findByTemplateName", query = "SELECT s FROM SendMailTamplate s WHERE s.templateName = :templateName")
    , @NamedQuery(name = "SendMailTamplate.findByDate", query = "SELECT s FROM SendMailTamplate s WHERE s.date = :date")
    , @NamedQuery(name = "SendMailTamplate.findByMinute", query = "SELECT s FROM SendMailTamplate s WHERE s.minute = :minute")
    , @NamedQuery(name = "SendMailTamplate.findByHour", query = "SELECT s FROM SendMailTamplate s WHERE s.hour = :hour")
    , @NamedQuery(name = "SendMailTamplate.findByDayInMonth", query = "SELECT s FROM SendMailTamplate s WHERE s.dayInMonth = :dayInMonth")
    , @NamedQuery(name = "SendMailTamplate.findByMonth", query = "SELECT s FROM SendMailTamplate s WHERE s.month = :month")
    , @NamedQuery(name = "SendMailTamplate.findByDayInWeek", query = "SELECT s FROM SendMailTamplate s WHERE s.dayInWeek = :dayInWeek")
    , @NamedQuery(name = "SendMailTamplate.findByFromAddress", query = "SELECT s FROM SendMailTamplate s WHERE s.fromAddress = :fromAddress")
    , @NamedQuery(name = "SendMailTamplate.findByToAddress", query = "SELECT s FROM SendMailTamplate s WHERE s.toAddress = :toAddress")
    , @NamedQuery(name = "SendMailTamplate.findBySubject", query = "SELECT s FROM SendMailTamplate s WHERE s.subject = :subject")
    , @NamedQuery(name = "SendMailTamplate.findByFile", query = "SELECT s FROM SendMailTamplate s WHERE s.file = :file")
    , @NamedQuery(name = "SendMailTamplate.findByParamFix", query = "SELECT s FROM SendMailTamplate s WHERE s.paramFix = :paramFix")
    , @NamedQuery(name = "SendMailTamplate.findByIfCrontab", query = "SELECT s FROM SendMailTamplate s WHERE s.ifCrontab = :ifCrontab")})
public class SendMailTamplate implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "in_id")
    private Integer inId;
    @Size(max = 45)
    @Column(name = "template_name")
    private String templateName;
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @Lob
    @Size(max = 16777215)
    @Column(name = "template")
    private String template;
    @Lob
    @Size(max = 65535)
    @Column(name = "query")
    private String query;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "minute")
    private String minute;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "hour")
    private String hour;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "day_in_month")
    private String dayInMonth;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "month")
    private String month;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "day_in_week")
    private String dayInWeek;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "from_address")
    private String fromAddress;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "to_address")
    private String toAddress;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "to_address_query")
    private String toAddressQuery;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "cc_array")
    private String ccArray;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "bcc_array")
    private String bccArray;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "subject")
    private String subject;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 2147483647)
    @Column(name = "message")
    private String message;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "file")
    private String file;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "param_fix")
    private String paramFix;
    @Basic(optional = false)
    @NotNull
    @Column(name = "if_crontab")
    private boolean ifCrontab;

    public SendMailTamplate() {
    }

    public SendMailTamplate(Integer inId) {
        this.inId = inId;
    }

    public SendMailTamplate(Integer inId, String minute, String hour, String dayInMonth, String month, String dayInWeek, String fromAddress, String toAddress, String toAddressQuery, String ccArray, String bccArray, String subject, String message, String file, String paramFix, boolean ifCrontab) {
        this.inId = inId;
        this.minute = minute;
        this.hour = hour;
        this.dayInMonth = dayInMonth;
        this.month = month;
        this.dayInWeek = dayInWeek;
        this.fromAddress = fromAddress;
        this.toAddress = toAddress;
        this.toAddressQuery = toAddressQuery;
        this.ccArray = ccArray;
        this.bccArray = bccArray;
        this.subject = subject;
        this.message = message;
        this.file = file;
        this.paramFix = paramFix;
        this.ifCrontab = ifCrontab;
    }

    public Integer getInId() {
        return inId;
    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getMinute() {
        return minute;
    }

    public void setMinute(String minute) {
        this.minute = minute;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getDayInMonth() {
        return dayInMonth;
    }

    public void setDayInMonth(String dayInMonth) {
        this.dayInMonth = dayInMonth;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDayInWeek() {
        return dayInWeek;
    }

    public void setDayInWeek(String dayInWeek) {
        this.dayInWeek = dayInWeek;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public String getToAddress() {
        return toAddress;
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }

    public String getToAddressQuery() {
        return toAddressQuery;
    }

    public void setToAddressQuery(String toAddressQuery) {
        this.toAddressQuery = toAddressQuery;
    }

    public String getCcArray() {
        return ccArray;
    }

    public void setCcArray(String ccArray) {
        this.ccArray = ccArray;
    }

    public String getBccArray() {
        return bccArray;
    }

    public void setBccArray(String bccArray) {
        this.bccArray = bccArray;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getParamFix() {
        return paramFix;
    }

    public void setParamFix(String paramFix) {
        this.paramFix = paramFix;
    }

    public boolean getIfCrontab() {
        return ifCrontab;
    }

    public void setIfCrontab(boolean ifCrontab) {
        this.ifCrontab = ifCrontab;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inId != null ? inId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SendMailTamplate)) {
            return false;
        }
        SendMailTamplate other = (SendMailTamplate) object;
        if ((this.inId == null && other.inId != null) || (this.inId != null && !this.inId.equals(other.inId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.SendMailTamplate[ inId=" + inId + " ]";
    }
    
}
