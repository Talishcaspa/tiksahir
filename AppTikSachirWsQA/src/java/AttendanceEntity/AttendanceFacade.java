/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AttendanceEntity;

import Entity.AbstractFacade;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author rivka
 */
@Stateless
public class AttendanceFacade extends AbstractFacade<Attendance> {

    @PersistenceContext(unitName = "AppTikSachirWsPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AttendanceFacade() {
        super(Attendance.class);
    }
    
    public Attendance getTodayUserAttendance(ManagementUsersAttendance user){
        List<Attendance> attendances = em.createNamedQuery("Attendance.findByUserDate").setParameter("user", user).setParameter("date", new Date()).getResultList(); 
        Attendance a = new Attendance();
        if(attendances.size()>0){
            a= attendances.get(0);
        }
        return a;
    }
    
    public List<Attendance> findByUserDates(ManagementUsersAttendance user,Date fromD, Date toD){

        return em.createNamedQuery("Attendance.findByUserDates").setParameter("user", user).setParameter("fromD", fromD).setParameter("toD", toD).getResultList(); 
    }
    
    public List<Attendance> findByGroupByAttendanceDates(Date fromD, Date toD){
        
        return em.createNamedQuery("Attendance.findByGroupByAttendanceDates").setParameter("fromD", fromD).setParameter("toD", toD).getResultList(); 
        
    }
    
    public Attendance findByInId(int inId){
        List<Attendance> attendances = em.createNamedQuery("Attendance.findByInId").setParameter("inId", inId).getResultList();
        Attendance a = new Attendance();
        if(attendances.size()>0){
            a = attendances.get(0);
        }
        return a;
    } 
    
    
}
