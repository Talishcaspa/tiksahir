/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AttendanceEntity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rivka
 */
@Entity
@Table(name = "app.presence_table")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PresenceTable.findAll", query = "SELECT p FROM PresenceTable p")
    , @NamedQuery(name = "PresenceTable.findByInId", query = "SELECT p FROM PresenceTable p WHERE p.inId = :inId")
    , @NamedQuery(name = "PresenceTable.findByIdUser", query = "SELECT p FROM PresenceTable p WHERE p.idUser = :idUser")
    , @NamedQuery(name = "PresenceTable.findByTimePresence", query = "SELECT p FROM PresenceTable p WHERE p.timePresence = :timePresence")
    , @NamedQuery(name = "PresenceTable.findByType", query = "SELECT p FROM PresenceTable p WHERE p.type = :type")
    , @NamedQuery(name = "PresenceTable.findByLatitude", query = "SELECT p FROM PresenceTable p WHERE p.latitude = :latitude")
    , @NamedQuery(name = "PresenceTable.findByLongitude", query = "SELECT p FROM PresenceTable p WHERE p.longitude = :longitude")
    , @NamedQuery(name = "PresenceTable.findByAbsenceType", query = "SELECT p FROM PresenceTable p WHERE p.absenceType = :absenceType")
    , @NamedQuery(name = "PresenceTable.findCount", query = "SELECT COUNT(p) FROM PresenceTable p where p.timePresence BETWEEN :fromD AND :toD and p.idUser= :idUser and p.type!=3")
})
public class PresenceTable implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "in_id")
    private Integer inId;
    
    
//    @Basic(optional = false)
//    @NotNull
//    @Column(name = "id_user")
//    private int idUser;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "time_presence")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timePresence;
    @Basic(optional = false)
    @NotNull
    @Column(name = "_type")
    private int type;
    @Basic(optional = false)
    @NotNull
    @Column(name = "latitude")
    private double latitude;
    @Basic(optional = false)
    @NotNull
    @Column(name = "longitude")
    private double longitude;
    @Basic(optional = false)
    @NotNull
    @Column(name = "absence_type")
    private int absenceType;
    
    @ManyToOne(cascade={CascadeType.ALL})
    @JoinColumn(name = "id_user", referencedColumnName = "in_id")
    private ManagementUsersAttendance idUser;

    public PresenceTable() {
    }

    public PresenceTable(Integer inId) {
        this.inId = inId;
    }

    public PresenceTable(Integer inId, int idUser, Date timePresence, int type, double latitude, double longitude, int absenceType) {
        this.inId = inId;
//        this.idUser = idUser;
        this.timePresence = timePresence;
        this.type = type;
        this.latitude = latitude;
        this.longitude = longitude;
        this.absenceType = absenceType;
    }

    public Integer getInId() {
        return inId;
    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }

//        public int getIdUser() {
//        return idUser;
//    }
//
//    public void setIdUser(int idUser) {
//        this.idUser = idUser;
//    }
    
    public ManagementUsersAttendance getIdUser() {
        return idUser;
    }

    public void setIdUser(ManagementUsersAttendance idUser) {
        this.idUser = idUser;
    }

    public Date getTimePresence() {
        return timePresence;
    }

    public void setTimePresence(Date timePresence) {
        this.timePresence = timePresence;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getAbsenceType() {
        return absenceType;
    }

    public void setAbsenceType(int absenceType) {
        this.absenceType = absenceType;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inId != null ? inId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PresenceTable)) {
            return false;
        }
        PresenceTable other = (PresenceTable) object;
        if ((this.inId == null && other.inId != null) || (this.inId != null && !this.inId.equals(other.inId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.PresenceTable[ inId=" + inId + " ]";
    }

}
