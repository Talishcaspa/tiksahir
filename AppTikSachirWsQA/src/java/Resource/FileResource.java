/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Resource;

import Entity.FormList;
import Entity.FormListFacade;
import Entity.UploadingFilesShirim;
import Entity.UploadingFilesShirimFacade;
import ObjectForJson.GetFile;
import ObjectForJson.MyResponse;
import Tools.Mistake;
import Tools.StringsConverter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.json.Json;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author rachel
 */
@Path("generic")
public class FileResource {

    @EJB
    UploadingFilesShirimFacade UploadingFilesShirimFAC;
    @EJB
    FormListFacade FormListFAC;

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of FileResource
     */
    public FileResource() {
    }

    //        <editor-fold defaultstate="collapsed"  desc="getFile">
    @POST
    @Path("getFile")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getUserDetails(String st) {

        System.out.println("Rachel start!!!! ");

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        Gson gsonReturn = builder.create();

        MyResponse response;
        String file;
        String path = null;
        UploadingFilesShirim uploudFile = new UploadingFilesShirim();

        //  Disassembles the JSON
        GetFile JsonGetFile = gson.fromJson(st, GetFile.class);
        System.out.println("Rachel :    GetFile JsonGetFile = gson.fromJson(st, GetFile.class);    ");

        file = JsonGetFile.getFile();
        if (!"".equals(file)) {

            path = JsonGetFile.getWorker_id() + "_" + (new Date().getTime()) + "." + JsonGetFile.getFile_type();
        }

        FormList findBySymbol = FormListFAC.findBySymbol(JsonGetFile.getFile_symbol());

//        FormList f= new FormList();
//        f.setSymbol(JsonGetFile.getFile_symbol());
        //  Building Object uploudFile  
        uploudFile.setWorkerId(JsonGetFile.getWorker_id());
//        uploudFile.setWorkerId(JsonGetFile.getWorker_id());

        uploudFile.setFormList(findBySymbol);
//        uploudFile.setFormList(f);

        uploudFile.setNote(JsonGetFile.getNote());
//        uploudFile.setNote(JsonGetFile.getNote());

        uploudFile.setFileType(JsonGetFile.getFile_type());
//        uploudFile.setFileType(JsonGetFile.getFile_type());

        uploudFile.setFileName(JsonGetFile.getFileName());
//        uploudFile.setFileName(JsonGetFile.getFileName());

        uploudFile.setFilepath(path);
//        uploudFile.setFilepath(path);

        uploudFile.setUserName(JsonGetFile.getUser_name());

//        uploudFile.setUserName(JsonGetFile.getUser_name());
        uploudFile.setDateUpload(new Date());

//        uploudFile.setDateUpload(new Date());
        uploudFile.setCompCid(JsonGetFile.getCompCid());
//        uploudFile.setCompCid(JsonGetFile.getCompCid());

        uploudFile.setYear(JsonGetFile.getYear());
//        uploudFile.setYear(JsonGetFile.getYear());

        uploudFile.setMonth(JsonGetFile.getMonth());
//                uploudFile.setMonth(JsonGetFile.getMonth());

        uploudFile.setDay(JsonGetFile.getDay());
//                uploudFile.setDay(JsonGetFile.getDay());

        uploudFile.setAmount(JsonGetFile.getAmount());
//        uploudFile.setAmount(JsonGetFile.getAmount());

        uploudFile.setPaid(Boolean.FALSE);
//        uploudFile.setPaid(Boolean.FALSE);
 
List<UploadingFilesShirim> luploudFile=new ArrayList<>();
luploudFile.add(uploudFile);
findBySymbol.setListUploadingFilesShirim(luploudFile);

FormListFAC.edit(findBySymbol);
//        UploadingFilesShirimFAC.create(uploudFile);

//        FormListFAC.remove(f);
        // Converts the string from JSON to file    
        StringsConverter StringsConverter = new StringsConverter();
        if (!StringsConverter.base64StringToFile(file, path)) {
            response = new MyResponse(new Mistake(1, "problem in uploud file"), null);
        } else {
            response = new MyResponse(new Mistake(), path);
        }
        String json = gsonReturn.toJson(response);
        return Response.ok().entity(json).type("text/plain;charset=UTF-8").build();

    }

//    </editor-fold>
    
    //    <editor-fold defaultstate="collapsed"  desc="deleteFile">
    @POST
    @Path("deleteFile")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getListForm(String st) {

        System.out.println("Rachel :   start getListForm  !!!!!!!!! ");

        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();

        Gson gsonReturn = builder.create();

        // get inid from json
        javax.json.JsonReader reader = Json.createReader(new StringReader(st));
        javax.json.JsonObject jsonObject = reader.readObject();

        int inId = jsonObject.getInt("inId");
//        StringsConverter StringsConverter = new StringsConverter();
//        boolean deleteFile = StringsConverter.deleteFile(path);

        UploadingFilesShirim fileRemove = new UploadingFilesShirim(inId);
        UploadingFilesShirimFAC.remove(fileRemove);

        MyResponse response = new MyResponse(new Mistake(), " remove file of inid  :" + inId);
        String jsonReturn = gsonReturn.toJson(response);
        return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
    }

    //</editor-fold>
}
