/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Resource;

import Entity.Applications;
import Entity.DetailsOfAppInstalls;
import Entity.DetailsOfAppInstallsFacade;
import Entity.VersionManagerOfApp;
import Entity.VersionManagerOfAppFacade;
import ObjectForJson.MyResponse;
import Tools.Mistake;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import javax.ejb.EJB;
import javax.json.Json;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author rachel
 */
@Path("generic")
public class VersionsResource {
    
    @EJB
    DetailsOfAppInstallsFacade  DetailsOfAppInstallsFAC;
    @EJB
    VersionManagerOfAppFacade VersionManagerOfAppFAC;

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of GenericResource_1
     */
    public VersionsResource() {
    }

    /**
     * Retrieves representation of an instance of Resource.VersionsResource
     *
     * @return an instance of java.lang.String
     * 
     */
    //    <editor-fold defaultstate="collapsed"  desc="getDownloadingVersions">
    // Update details of phone downloading version;
    @POST
    @Path("getDownloadingVersions")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDownloadingVersions(String st) {

        System.out.println("Rachel :   start getListForm  !!!!!!!!! ");

        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();
//        Gson gsonGet = builder.create();

        Gson gsonReturn = builder.create();

        javax.json.JsonReader reader = Json.createReader(new StringReader(st));
        javax.json.JsonObject jsonObject = reader.readObject();

        DetailsOfAppInstalls detailsOfAppInstalls = new DetailsOfAppInstalls();
        detailsOfAppInstalls.setUuid(jsonObject.getString("UUID"));
        detailsOfAppInstalls.setMechin(jsonObject.getString( "mechin"));
        detailsOfAppInstalls.setVersion(jsonObject.getString("version"));
        detailsOfAppInstalls.setNameOfDevice(jsonObject.getString("nameOfDevice"));
        detailsOfAppInstalls.setLanguage(jsonObject.getString("language"));

        detailsOfAppInstalls.setDateInstalls(new Date());
//        detailsOfAppInstalls.setVersionManagerOfApp(jsonObject.getInt("VersionOfTheApp"));
        DetailsOfAppInstallsFAC.create(detailsOfAppInstalls);

        MyResponse response = new MyResponse(new Mistake(),detailsOfAppInstalls );
        String jsonReturn = gsonReturn.toJson(response);

        return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
}

//</editor-fold>
     
    //    <editor-fold defaultstate="collapsed"  desc="getNewVersion">
    // Add a new version of the appellation;
    @POST
    @Path("getNewVersion")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getNewVersion(String st) throws ParseException {

        System.out.println("Rachel :   start getListForm  !!!!!!!!! ");

        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();
//        Gson gsonGet = builder.create();

        Gson gsonReturn = builder.create();

        javax.json.JsonReader reader = Json.createReader(new StringReader(st));
        javax.json.JsonObject jsonObject = reader.readObject();
        DateFormat format = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy", Locale.ENGLISH);
//        Applications applications = new Applications(jsonObject.getInt("inIdApplication"));

        VersionManagerOfApp versionManagerOfApp = new VersionManagerOfApp();
//        versionManagerOfApp.setInIdApplication(new Applications(jsonObject.getInt("inIdApplication")));
//        versionManagerOfApp.setApplications(new Applications(jsonObject.getInt("inIdApplication")));
        versionManagerOfApp.setVersionReleaseDate(format.parse(jsonObject.getString("VersionReleaseDate")));
         versionManagerOfApp.setNumVersion(Double.parseDouble(jsonObject.getString("numVersion")));
       versionManagerOfApp.setVersionName(jsonObject.getString("VersionName"));
        versionManagerOfApp.setDescription(jsonObject.getString("description"));
        versionManagerOfApp.setDirectorOfPublishing(jsonObject.getString("DirectorOfPublishing"));
        versionManagerOfApp.setIsIphone( jsonObject.getBoolean("IsIphone"));
        versionManagerOfApp.setIsVersionFix(jsonObject.getBoolean("IsVersionFix"));
        VersionManagerOfAppFAC.create(versionManagerOfApp);

        MyResponse response = new MyResponse(new Mistake(),versionManagerOfApp );
        String jsonReturn = gsonReturn.toJson(response);

        return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
    }
        //</editor-fold>

}

   

