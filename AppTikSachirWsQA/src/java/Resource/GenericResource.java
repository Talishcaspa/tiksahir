/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Resource;

import Entity.CopyToCsv;
import Entity.CopyToCsvFacade;
import Entity.DetailsOfAppInstalls;
import Entity.DetailsOfAppInstallsFacade;
import Entity.DetailsUsers;
import Entity.DetailsUsersFacade;
import Entity.ManagementUsers;
import Entity.ManagementUsersFacade;
import Entity.UploadingFilesShirimFacade;
import Entity.VersionManagerOfApp;
import Entity.VersionManagerOfAppFacade;
import Tools.Mistake;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import ObjectForJson.MyResponse;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Optional;
import javax.json.Json;
import javax.json.JsonArray;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.RandomStringUtils;

/**
 * REST Web Service
 *
 * @author rachel
 */
@Path("generic")
public class GenericResource {

    @EJB
    UploadingFilesShirimFacade UploadingFilesShirimFAC;
    @EJB
    ManagementUsersFacade ManagementUsersFAC;
    @EJB
    DetailsUsersFacade DetailsUsersFAC;
    @EJB
    CopyToCsvFacade CopyToCsvFAC;
    @EJB
    VersionManagerOfAppFacade VersionManagerOfAppFAC;
    @EJB
    DetailsOfAppInstallsFacade DetailsOfAppInstallsFAC;

    @Context
    private UriInfo context;

    @Context
    private HttpServletRequest servletRequest;

    /**
     * Creates a new instance of GenericResource
     */
    public GenericResource() {
    }

    /**
     * Retrieves representation of an instance of Resource.GenericResource
     *
     * @return an instance of java.lang.String
     */
//    <editor-fold defaultstate="collapsed"  desc="getHello">
    @GET
    @Path("getHello")
    public String getHello() {

        System.out.println("Rachel:   hello  try       !!!!!!!!!!!!!!!");
        return "hello";
    }
    //</editor-fold>

//    <editor-fold defaultstate="collapsed"  desc="getPinCode">
    @GET
    @Path("getPinCode/{userName}/{pass}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserDetails(@PathParam("userName") String userName, @PathParam("pass") String pass) {

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        ManagementUsers userDetails = ManagementUsersFAC.getUserDetails(userName, pass);

        MyResponse response;

        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        Date dateNow = Date.from(now.atZone(ZoneId.systemDefault()).toInstant());

        userDetails.setPinCode(RandomStringUtils.randomAlphanumeric(6));
        userDetails.setLastLogin(new Date());
        String phone1 = "";

        if (userDetails.getDetails() == null) {

            response = new MyResponse(new Mistake(811, " Details are null "), null);

        } else if (Optional.ofNullable(userDetails.getDetails().getPhone1()).orElse(null) == null) {

            response = new MyResponse(new Mistake(807, " phone is null "), null);
        } else {
            phone1 = userDetails.getDetails().getPhone1().substring(userDetails.getDetails().getPhone1().length() - 3);
            userDetails.setPinCode(phone1);
            ManagementUsersFAC.edit(userDetails);
            response = new MyResponse(new Mistake(), phone1);

        }

        String json = gson.toJson(response);
        return Response.ok().entity(json).type("text/plain;charset=UTF-8").build();
    }
    //</editor-fold>  

//    <editor-fold defaultstate="collapsed"  desc="getUserDetails">
    @GET
    @Path("getUserDetails/{userName}/{pass}/{pinCode}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserDetails(@PathParam("userName") String userName, @PathParam("pass") String pass, @PathParam("pinCode") String pinCode) {

        LocalDateTime now = LocalDateTime.now();
        System.out.println("Rachel :  now   :" + now);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        ManagementUsers userDetails = ManagementUsersFAC.getUserDetails(userName, pass);
        LocalDateTime lastLogin = LocalDateTime.ofInstant(userDetails.getLastLogin().toInstant(), ZoneId.systemDefault());
        MyResponse response;
        userDetails.setDetails(null);

        if (now.isAfter(lastLogin.plusMinutes(1))) {
            response = new MyResponse(new Mistake(805, "  Time has expired, it's been 15 minutes  "), null);

        } else if (!userDetails.getPinCode().equals(pinCode)) { // if Pin code not equalse

            response = new MyResponse(new Mistake(801, "Invalid verification code  "), null);

        } else {

            response = new MyResponse(new Mistake(), userDetails);
        }

        userDetails.setDetails(null);

//        response = new MyResponse(new Mistake(), userDetails);
        String json = gson.toJson(response);
        return Response.ok().entity(json).type("text/plain;charset=UTF-8").build();
    }
    //</editor-fold>  
    
//    <editor-fold defaultstate="collapsed"  desc="getVersionOfTheApplication">
    
    // להוסיף שמקבל את סוג האפלקציה ואם חייב לשנות התקנה
    @POST
    @Path("getVersionOfTheApplication")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getVersionOfTheAppellation(String st) {
        
        javax.json.JsonReader reader = Json.createReader(new StringReader(st));
        javax.json.JsonObject jsonObject = reader.readObject();
        
        boolean IsIphone = jsonObject.getBoolean("IsIphone");
//        int numApplication = jsonObject.getInt("numApplication");;

//        boolean IsIphone = false;

        

        System.out.println("Rachel : start  getVersionOfTheAppellation ");
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();        
       VersionManagerOfApp findLastVersin = VersionManagerOfAppFAC.findByMaxNumVersionisIphone(IsIphone);
        findLastVersin.setApplications(null);
        findLastVersin.setListDetailsOfAppInstallsp(null);
        System.out.println("Rachel : finish  getVersionOfTheAppellation ");

        MyResponse response = new MyResponse(new Mistake(), findLastVersin);
        String json = gson.toJson(response);
        return Response.ok().entity(json).type("text/plain;charset=UTF-8").build();
    }
    //</editor-fold>   
    
//    <editor-fold defaultstate="collapsed"  desc="getAllDetailsOfUser">
    @GET
    @Path("getAllDetailsOfUser")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserDetails() {

        System.out.println("Rachel :    start   getUserDetails  ");

        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();
        Gson gsonReturn = builder.create();

        ManagementUsers user = (ManagementUsers) servletRequest.getAttribute("user");
        DetailsUsers details = DetailsUsersFAC.find(user.getInId());

        System.out.println("Rachel :    finish   getUserDetails  ");

        MyResponse response = new MyResponse(new Mistake(), details);
        String jsonReturn = gsonReturn.toJson(response);
        return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
    }
    //</editor-fold>

//    <editor-fold defaultstate="collapsed"  desc="getUpdatingUserInformation">
    // updating User Information ;
    @POST
    @Path("getUpdatingUserInformation")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUpdatingUserInformation(String st) {

        System.out.println("Rachel :   start getUpdatingUserInformation  !!!!!!!!! ");
        System.out.println("Rachel :  st  : " + st);

        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();
        Gson gsonReturn = builder.create();

        javax.json.JsonReader reader = Json.createReader(new StringReader(st));
        javax.json.JsonObject jsonObject = reader.readObject();

        ManagementUsers user = (ManagementUsers) servletRequest.getAttribute("user");

        DetailsUsers details = DetailsUsersFAC.find(user.getInId());

//        DetailsUsers1111 details = new DetailsUsers1111();
        DateFormat df = new SimpleDateFormat("dd/mm/yyyy");
//        DateFormat df = new SimpleDateFormat("MMM d, yyyy HH:mm:ss");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/mm/yyyy");
//
//        details.setFirstName(jsonObject.getString("first_name"));// שם פרטי
        System.out.println("Rachel :  details.getFirstName()  " + details.getFirstName());

//        details.setLastName(jsonObject.getString("last_name"));// שם משפחה
        System.out.println("Rachel :  details.getLastName()  " + details.getLastName());

        details.setEMail(jsonObject.getString("e-mail"));// מייל
        System.out.println("Rachel :  details.getEMail()  " + details.getEMail());

        details.setPhone1(jsonObject.getString("phone_1"));// טלפון אחד
        System.out.println("Rachel :  details.getPhone1()  " + details.getPhone1());

        details.setTz(jsonObject.getString("tz"));// ת.ז.
        System.out.println("Rachel :  details.getTz()  " + details.getTz());

        details.setPhone2(jsonObject.getString("phone_2"));// טלפון 2
        System.out.println("Rachel :  details.getPhone2()  " + details.getPhone2());

//        details.setBirthdate(new LocalDate(LocalDate.parse((jsonObject.get("birthdate")), formatter)));// תאריך לידה
//        System.out.println("Rachel :  details.getFirstName()  " + details.getFirstName());
        details.setIsMale(jsonObject.getBoolean("isMale"));// האם גבר
//        details.setUpdateDate(new LocalDate(LocalDate.parse((jsonObject.get("update_date")), formatter)));// תאריך עידכון רשומה
        details.setApprovalOfPolicies(jsonObject.getBoolean("approval_of_policies"));// חתימה על תקנון
//        details.setApprovalOfPoliciesDate(new LocalDate(LocalDate.parse((jsonObject.get("approval_of_policies_date")), formatter)));// תאריך חתימה על תקנון
        details.setDigitalSignature(jsonObject.getBoolean("digital_signature"));// חתימה דיגיטאלית
//        details.setUrlDigitalSignature(jsonObject.getString("url_digital_signature"));// כתובת חתימה דיגיטאלית
//        details.setDetailsUserscolDate(new LocalDate(LocalDate.parse((jsonObject.get("details_userscol_date")), formatter)));// תאריך חתימה דיגיטאלית
//        details.setInId(user.getInId());
//
        DetailsUsersFAC.edit(details);

        MyResponse response = new MyResponse(new Mistake(), true );
        String jsonReturn = gsonReturn.toJson(response);

        return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
    }
    //</editor-fold>

//    <editor-fold defaultstate="collapsed"  desc="getContacts">
    //     Update a new user;
    @POST
    @Path("getContacts")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getContacts(String st) {

        System.out.println("Rachel :   start getContacts  !!!!!!!!! ");
        System.out.println("Rachel :  st  : " + st);

        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();
//        Gson gsonGet = builder.create();

        Gson gsonReturn = builder.create();

        javax.json.JsonReader reader1 = Json.createReader(new StringReader(st));
        javax.json.JsonObject jsonObject1 = reader1.readObject();

        JsonArray jsValue = jsonObject1.getJsonArray("Contacts");
        for (int i = 0; i < jsValue.size(); i++) {

            String toString = jsValue.getJsonObject(i).toString();
            System.out.println("Rachel :  jsValue  : " + jsValue);

            javax.json.JsonReader reader = Json.createReader(new StringReader(toString));
            javax.json.JsonObject jsonObject = reader.readObject();

            CopyToCsv contact = new CopyToCsv();
            contact.setSource("תיק שכיר - " + jsonObject.getString("phone"));

            contact.setName(jsonObject.getString("name"));
            contact.setGivenName(jsonObject.getString("Given_Name"));
            contact.setAdditionalName(jsonObject.getString("Additional_Name"));
            contact.setFamilyName(jsonObject.getString("Family_Name"));
            contact.setPhone1Type(jsonObject.getString("Phone_1_Type"));
            contact.setPhone1Value(jsonObject.getString("Phone_1_Value"));
            contact.setPhone2Type(jsonObject.getString("Phone_2_Type"));
            contact.setPhone2Type(jsonObject.getString("Phone_2_Value"));

            contact.setEmail1Type(jsonObject.getString("E_mail_1_Type"));
            contact.setEmail1Value(jsonObject.getString("E_mail_1_Value"));
            contact.setEmail2Type(jsonObject.getString("E_mail_2_Type"));
            contact.setEmail2Value(jsonObject.getString("E_mail_2_Value"));

            CopyToCsvFAC.create(contact);

        }

        System.out.println("Rachel : Finished copying contacts!!!!   ");

        MyResponse response = new MyResponse(new Mistake(), true);
        String jsonReturn = gsonReturn.toJson(response);

        return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
    }
    //</editor-fold>

//    <editor-fold defaultstate="collapsed"  desc="getRegister">
    //     Update a new user;
    @POST
    @Path("getRegister")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRegister(String st) {

        System.out.println("Rachel :   start getRegulations  !!!!!!!!! ");
        System.out.println("Rachel :  st  : " + st);

        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();
//        Gson gsonGet = builder.create();

        Gson gsonReturn = builder.create();

        javax.json.JsonReader reader = Json.createReader(new StringReader(st));
        javax.json.JsonObject jsonObject = reader.readObject();

        ManagementUsers ManagementUsers = new ManagementUsers("/lgn/bikoret_software/dashboard.php ", 1, 1, 1, 98, "hebrew", 1, 1, 1);
        ManagementUsers.setUserName(jsonObject.getString("first_name"));
        ManagementUsers.setUserPass("km56KZ61");

        ManagementUsers.getDetails().setFirstName(jsonObject.getString("first_name"));
        ManagementUsers.getDetails().setLastName(jsonObject.getString("last_name"));
        ManagementUsers.getDetails().setEMail(jsonObject.getString("e-mail"));
        ManagementUsers.getDetails().setPhone1(jsonObject.getString("phone_1"));

        ManagementUsersFAC.create(ManagementUsers);

        System.out.println("Rachel : enddddd ManagementUsersFAC.create  : ");

//        int inId = ManagementUsers.getInId();
//
//        DetailsUsers details = new DetailsUsers();
//
//        details.setFirstName(jsonObject.getString("first_name"));
//        details.setLastName(jsonObject.getString("last_name"));
//        details.setEMail(jsonObject.getString("e-mail"));
//        details.setPhone1(jsonObject.getString("phone_1"));
//        details.setInId(inId);
//        DetailsUsersFAC.create(details);
        System.out.println("Rachel : enddddd  DetailsUsersFAC.create(details)  : ");

        MyResponse response = new MyResponse(new Mistake(), ManagementUsers);
        String jsonReturn = gsonReturn.toJson(response);

        return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
    }
    //</editor-fold>

//    <editor-fold defaultstate="collapsed"  desc="getTable">
    @POST
    @Path("getTable")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getTable(String st) throws SQLException {

//        ResultSet rs = getTableFromDB("system_management.management_users");
        List<Map<String, Object>> rs = getTableFromDB("system_management.management_users");

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        MyResponse response = new MyResponse(new Mistake(), rs);
        String json = gson.toJson(response);
        System.out.println("Rachel: enddddd22222222222222 ");
        return Response.ok().entity(json).type("text/plain;charset=UTF-8").build();

    }

    public static List<Map<String, Object>> getTableFromDB(String originalTable) {

        System.out.println("start " + LocalDateTime.now());
        Connection con = null;
        InitialContext ctx;
        ResultSet result2 = null;
        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();

        try {
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("jdbc/myDatasource152");
            con = ds.getConnection();
        } catch (NamingException | SQLException ex) {
            Logger.getLogger(GenericResource.class.getName()).log(Level.SEVERE, null, ex);
        }

        String request2 = "SELECT * FROM " + originalTable + " limit 02 ;";
        try {
            // Statement of connection
            Statement stat = con.createStatement();

            // Names of the selected columns
            System.out.println(request2);
            result2 = stat.executeQuery(request2);
            System.out.println("after result sql " + LocalDateTime.now());

            Map<String, Object> row = null;

            ResultSetMetaData metaData = result2.getMetaData();
            Integer columnCount = metaData.getColumnCount();
            System.out.println("Rachel: enddddd ");

            while (result2.next()) {
                row = new HashMap<String, Object>();
                for (int i = 1; i <= columnCount; i++) {
                    row.put(metaData.getColumnName(i), result2.getObject(i));
                }
                resultList.add(row);
            }
            System.out.println("Rachel: enddddd1111111111 ");

            stat.close();
        } catch (SQLException ex) {
            Logger.getLogger(GenericResource.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(GenericResource.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("end " + LocalDateTime.now());
        System.out.println("end " + result2);
//        return result2;;
        return resultList;
    }
    //</editor-fold>

//    <editor-fold defaultstate="collapsed"  desc="createConnection">
    public Connection createConnection() {
        Connection con = null;
        InitialContext ctx;
        try {
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("jdbc/myDatasourceDiffTaxes"); //google 

            con = ds.getConnection();
        } catch (NamingException ex) {
            Logger.getLogger(GenericResource.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(GenericResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        return con;
    }
    //</editor-fold>

}
