/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rachel
 */
@Entity
@Table(name = "app_rachel.VersionManagerOfApp")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VersionManagerOfApp.findAll", query = "SELECT v FROM VersionManagerOfApp v")
    , @NamedQuery(name = "VersionManagerOfApp.findByInId", query = "SELECT v FROM VersionManagerOfApp v WHERE v.inId = :inId")
//    , @NamedQuery(name = "VersionManagerOfApp.findByInIdApplication", query = "SELECT v FROM VersionManagerOfApp v WHERE v.inIdApplication = :inIdApplication")
    , @NamedQuery(name = "VersionManagerOfApp.findByVersionReleaseDate", query = "SELECT v FROM VersionManagerOfApp v WHERE v.versionReleaseDate = :versionReleaseDate")
    , @NamedQuery(name = "VersionManagerOfApp.findByNumVersion", query = "SELECT v FROM VersionManagerOfApp v WHERE v.numVersion = :numVersion")
    , @NamedQuery(name = "VersionManagerOfApp.findByVersionName", query = "SELECT v FROM VersionManagerOfApp v WHERE v.versionName = :versionName")
    , @NamedQuery(name = "VersionManagerOfApp.findByDescription", query = "SELECT v FROM VersionManagerOfApp v WHERE v.description = :description")
    , @NamedQuery(name = "VersionManagerOfApp.findByDirectorOfPublishing", query = "SELECT v FROM VersionManagerOfApp v WHERE v.directorOfPublishing = :directorOfPublishing")
    , @NamedQuery(name = "VersionManagerOfApp.findByIsIphone", query = "SELECT v FROM VersionManagerOfApp v WHERE v.isIphone = :isIphone")
    , @NamedQuery(name = "VersionManagerOfApp.findByIsVersionFix", query = "SELECT v FROM VersionManagerOfApp v WHERE v.isVersionFix = :isVersionFix")
    , @NamedQuery(name = "VersionManagerOfApp.findByMaxNumVersionisIphone", query = "SELECT v FROM VersionManagerOfApp v WHERE v.isVersionFix= TRUE AND v.numVersion =( SELECT MAX( ff.numVersion) from  VersionManagerOfApp ff  WHERE ff.isIphone = :isIphone AND  ff.isVersionFix= TRUE)  ")

})
public class VersionManagerOfApp implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "in_id")
    private Integer inId;
//    @Column(name = "inIdApplication")
//    private Integer inIdApplication;
    @Column(name = "VersionReleaseDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date versionReleaseDate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "numVersion")
    private Double numVersion;
    @Size(max = 45)
    @Column(name = "VersionName")
    private String versionName;
    @Size(max = 200)
    @Column(name = "description")
    private String description;
    @Size(max = 45)
    @Column(name = "DirectorOfPublishing")
    private String directorOfPublishing;
    @Column(name = "IsIphone")
    private Boolean isIphone;
    @Column(name = "IsVersionFix")
    private Boolean isVersionFix;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    @JoinColumn(name = "inIdApplication", referencedColumnName = "in_id")
    private Applications Applications;

    @OneToMany(mappedBy = "VersionManagerOfApp", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private List<DetailsOfAppInstalls> ListDetailsOfAppInstallsp;

    public Applications getApplications() {
        return Applications;
    }

    public void setApplications(Applications Applications) {
        this.Applications = Applications;
    }

    public List<DetailsOfAppInstalls> getListDetailsOfAppInstallsp() {
        return ListDetailsOfAppInstallsp;
    }

    public void setListDetailsOfAppInstallsp(List<DetailsOfAppInstalls> ListDetailsOfAppInstallsp) {
        this.ListDetailsOfAppInstallsp = ListDetailsOfAppInstallsp;
    }

    public VersionManagerOfApp() {
    }

    public VersionManagerOfApp(Integer inId) {
        this.inId = inId;
    }

    public Integer getInId() {
        return inId;
    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }

//    public Integer getInIdApplication() {
//        return inIdApplication;
//    }
//
//    public void setInIdApplication(Integer inIdApplication) {
//        this.inIdApplication = inIdApplication;
//    }
    public Date getVersionReleaseDate() {
        return versionReleaseDate;
    }

    public void setVersionReleaseDate(Date versionReleaseDate) {
        this.versionReleaseDate = versionReleaseDate;
    }

    public Double getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(Double numVersion) {
        this.numVersion = numVersion;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDirectorOfPublishing() {
        return directorOfPublishing;
    }

    public void setDirectorOfPublishing(String directorOfPublishing) {
        this.directorOfPublishing = directorOfPublishing;
    }

    public Boolean getIsIphone() {
        return isIphone;
    }

    public void setIsIphone(Boolean isIphone) {
        this.isIphone = isIphone;
    }

    public Boolean getIsVersionFix() {
        return isVersionFix;
    }

    public void setIsVersionFix(Boolean isVersionFix) {
        this.isVersionFix = isVersionFix;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inId != null ? inId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VersionManagerOfApp)) {
            return false;
        }
        VersionManagerOfApp other = (VersionManagerOfApp) object;
        if ((this.inId == null && other.inId != null) || (this.inId != null && !this.inId.equals(other.inId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.VersionManagerOfApp[ inId=" + inId + " ]";
    }

}
