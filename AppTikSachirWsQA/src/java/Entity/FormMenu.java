/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rachel
 */
@Entity
@Table(name = "worker_diff_taxes_reports.formMenu")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FormMenu.findAll", query = "SELECT f FROM FormMenu f")
    , @NamedQuery(name = "FormMenu.findByInId", query = "SELECT f FROM FormMenu f WHERE f.inId = :inId")
    , @NamedQuery(name = "FormMenu.findByName", query = "SELECT f FROM FormMenu f WHERE f.name = :name")
    , @NamedQuery(name = "FormMenu.findByKey", query = "SELECT f FROM FormMenu f WHERE f.key = :key")
    , @NamedQuery(name = "FormMenu.findByIcon", query = "SELECT f FROM FormMenu f WHERE f.icon = :icon")})
public class FormMenu implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "in_id")
    private Integer inId;
    @Size(max = 45)
    @Column(name = "name")
    private String name;
    @Column(name = "`key`")
    private Integer key;
    @Size(max = 45)
    @Column(name = "icon")
    private String icon;
    
    @OneToMany(mappedBy = "FormMenu",cascade={CascadeType.PERSIST, CascadeType.REMOVE})
    private List<FormList> FormList;

    public List<FormList> getFormList() {
        return FormList;
    }

    public void setFormList(List<FormList> FormList) {
        this.FormList = FormList;
    }

    public FormMenu() {
    }

    public FormMenu(Integer key, String name, String icon) {
        this.key = key;
        this.name = name;
        this.icon = icon;
    }

    public FormMenu(Integer inId) {
        this.inId = inId;
    }

    public Integer getInId() {
        return inId;
    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inId != null ? inId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FormMenu)) {
            return false;
        }
        FormMenu other = (FormMenu) object;
        if ((this.inId == null && other.inId != null) || (this.inId != null && !this.inId.equals(other.inId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.FormMenu[ inId=" + inId + " ]";
    }

}
