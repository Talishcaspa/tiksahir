/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author rachel
 */
@Stateless
public class UploadingFilesShirimFacade extends AbstractFacade<UploadingFilesShirim> {

    @PersistenceContext(unitName = "AppTikSachirWsPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UploadingFilesShirimFacade() {
        super(UploadingFilesShirim.class);
    }

    public List<UploadingFilesShirim> findByFileSymbolWorkerId(String symbol, int workerId) {

        return em.createNamedQuery("UploadingFilesShirim.findByFileSymbolWorkerId").setParameter("fileSymbol", symbol).setParameter("workerId", workerId).getResultList();

    }

    public List<UploadingFilesShirim> findByFileSymbolWorkerIdYear(String symbol, int workerId, int year) {

        return em.createNamedQuery("UploadingFilesShirim.findByFileSymbolWorkerIdYear").setParameter("fileSymbol", symbol).setParameter("workerId", workerId).setParameter("year", year).getResultList();

    }

    public List<UploadingFilesShirim> findByWorkerIdPaid(int workerId) {

        return em.createNamedQuery("UploadingFilesShirim.findByWorkerIdPaid").setParameter("workerId", workerId).getResultList();

    }
}
