/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rachel
 */
@Entity
@Table(name = "app_rachel.detailsOfAppInstalls")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailsOfAppInstalls.findAll", query = "SELECT d FROM DetailsOfAppInstalls d")
    , @NamedQuery(name = "DetailsOfAppInstalls.findByInId", query = "SELECT d FROM DetailsOfAppInstalls d WHERE d.inId = :inId")
    , @NamedQuery(name = "DetailsOfAppInstalls.findByDateInstalls", query = "SELECT d FROM DetailsOfAppInstalls d WHERE d.dateInstalls = :dateInstalls")
//    , @NamedQuery(name = "DetailsOfAppInstalls.findByVersionoftheapp", query = "SELECT d FROM DetailsOfAppInstalls d WHERE d.versionoftheapp = :versionoftheapp")
    , @NamedQuery(name = "DetailsOfAppInstalls.findByNameOfDevice", query = "SELECT d FROM DetailsOfAppInstalls d WHERE d.nameOfDevice = :nameOfDevice")
    , @NamedQuery(name = "DetailsOfAppInstalls.findByUuid", query = "SELECT d FROM DetailsOfAppInstalls d WHERE d.uuid = :uuid")
    , @NamedQuery(name = "DetailsOfAppInstalls.findByMechin", query = "SELECT d FROM DetailsOfAppInstalls d WHERE d.mechin = :mechin")
    , @NamedQuery(name = "DetailsOfAppInstalls.findByVersion", query = "SELECT d FROM DetailsOfAppInstalls d WHERE d.version = :version")
    , @NamedQuery(name = "DetailsOfAppInstalls.findByLanguage", query = "SELECT d FROM DetailsOfAppInstalls d WHERE d.language = :language")})
public class DetailsOfAppInstalls implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "in_id")
    private Integer inId;
    @Column(name = "DateInstalls")
    @Temporal(TemporalType.DATE)
    private Date dateInstalls;
//    @Column(name = "Version_of_the_app")
//    private Integer versionoftheapp;
    @Size(max = 45)
    @Column(name = "nameOfDevice")
    private String nameOfDevice;
    @Size(max = 45)
    @Column(name = "UUID")
    private String uuid;
    @Size(max = 45)
    @Column(name = "mechin")
    private String mechin;
    @Size(max = 45)
    @Column(name = "version")
    private String version;
    @Size(max = 45)
    @Column(name = "language")
    private String language;
    
    @ManyToOne(cascade={CascadeType.PERSIST, CascadeType.REMOVE})
    @JoinColumn(name = "Version_of_the_app", referencedColumnName = "in_id")
    private VersionManagerOfApp VersionManagerOfApp;

    public VersionManagerOfApp getVersionManagerOfApp() {
        return VersionManagerOfApp;
    }

    public void setVersionManagerOfApp(VersionManagerOfApp VersionManagerOfApp) {
        this.VersionManagerOfApp = VersionManagerOfApp;
    }

    public DetailsOfAppInstalls() {
    }

    public DetailsOfAppInstalls(Integer inId) {
        this.inId = inId;
    }

    public Integer getInId() {
        return inId;
    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }

    public Date getDateInstalls() {
        return dateInstalls;
    }

    public void setDateInstalls(Date dateInstalls) {
        this.dateInstalls = dateInstalls;
    }

//    public Integer getVersionoftheapp() {
//        return versionoftheapp;
//    }
//
//    public void setVersionoftheapp(Integer versionoftheapp) {
//        this.versionoftheapp = versionoftheapp;
//    }

    public String getNameOfDevice() {
        return nameOfDevice;
    }

    public void setNameOfDevice(String nameOfDevice) {
        this.nameOfDevice = nameOfDevice;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getMechin() {
        return mechin;
    }

    public void setMechin(String mechin) {
        this.mechin = mechin;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inId != null ? inId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailsOfAppInstalls)) {
            return false;
        }
        DetailsOfAppInstalls other = (DetailsOfAppInstalls) object;
        if ((this.inId == null && other.inId != null) || (this.inId != null && !this.inId.equals(other.inId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Attendance.DetailsOfAppInstalls[ inId=" + inId + " ]";
    }
    
}
