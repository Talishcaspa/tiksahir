/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rachel
 */
@Entity
@Table(name = "app_rachel.applications")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Applications.findAll", query = "SELECT a FROM Applications a")
    , @NamedQuery(name = "Applications.findByInId", query = "SELECT a FROM Applications a WHERE a.inId = :inId")
    , @NamedQuery(name = "Applications.findByNameOfApplication", query = "SELECT a FROM Applications a WHERE a.nameOfApplication = :nameOfApplication")
    , @NamedQuery(name = "Applications.findByDescriptionOfAppellation", query = "SELECT a FROM Applications a WHERE a.descriptionOfAppellation = :descriptionOfAppellation")})
public class Applications implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "in_id")
    private Integer inId;
    @Size(max = 45)
    @Column(name = "nameOfApplication")
    private String nameOfApplication;
    @Size(max = 100)
    @Column(name = "descriptionOfAppellation")
    private String descriptionOfAppellation;
    
    @OneToMany(mappedBy = "Applications", cascade={CascadeType.PERSIST, CascadeType.REMOVE})
    private List<VersionManagerOfApp> ListVersionManagerOfApp;

    public List<VersionManagerOfApp> getListVersionManagerOfApp() {
        return ListVersionManagerOfApp;
    }

    public void setListVersionManagerOfApp(List<VersionManagerOfApp> ListVersionManagerOfApp) {
        this.ListVersionManagerOfApp = ListVersionManagerOfApp;
    }

    public Applications() {
    }

    public Applications(Integer inId) {
        this.inId = inId;
    }

    public Integer getInId() {
        return inId;
    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }

    public String getNameOfApplication() {
        return nameOfApplication;
    }

    public void setNameOfApplication(String nameOfApplication) {
        this.nameOfApplication = nameOfApplication;
    }

    public String getDescriptionOfAppellation() {
        return descriptionOfAppellation;
    }

    public void setDescriptionOfAppellation(String descriptionOfAppellation) {
        this.descriptionOfAppellation = descriptionOfAppellation;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inId != null ? inId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Applications)) {
            return false;
        }
        Applications other = (Applications) object;
        if ((this.inId == null && other.inId != null) || (this.inId != null && !this.inId.equals(other.inId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.Applications[ inId=" + inId + " ]";
    }
    
}
