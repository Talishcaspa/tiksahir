/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author rachel
 */
@Stateless
public class FormListFacade extends AbstractFacade<FormList> {

    @PersistenceContext(unitName = "AppTikSachirWsPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FormListFacade() {
        super(FormList.class);
    }
    
        public List<FormList> findAllIsDisplayAppTrue(int type)
    {
     
       return em.createNamedQuery("FormList.findByTypeIsDisplayAppTrue").setParameter("type", type).getResultList();
        
    }
    
    /**
     *
     * @param symbol
     * @return
     */
    public FormList findBySymbol(String symbol)
    {
        System.out.println("Rachel :           findBySymbol       ");
       return  (FormList) em.createNamedQuery("FormList.findBySymbol").setParameter("symbol", symbol).getResultList().get(0);
        
    }
    }
