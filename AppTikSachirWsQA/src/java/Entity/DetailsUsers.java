/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rachel
 */
@Entity
@Table(name = "system_management.details_users")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailsUsers.findAll", query = "SELECT d FROM DetailsUsers d")
    , @NamedQuery(name = "DetailsUsers.findByInId", query = "SELECT d FROM DetailsUsers d WHERE d.inId = :inId")
//    , @NamedQuery(name = "DetailsUsers.findByManagementUsersInId", query = "SELECT d FROM DetailsUsers d WHERE d.managementUsersInId = :managementUsersInId")
    , @NamedQuery(name = "DetailsUsers.findByFirstName", query = "SELECT d FROM DetailsUsers d WHERE d.firstName = :firstName")
    , @NamedQuery(name = "DetailsUsers.findByLastName", query = "SELECT d FROM DetailsUsers d WHERE d.lastName = :lastName")
    , @NamedQuery(name = "DetailsUsers.findByTz", query = "SELECT d FROM DetailsUsers d WHERE d.tz = :tz")
    , @NamedQuery(name = "DetailsUsers.findByEMail", query = "SELECT d FROM DetailsUsers d WHERE d.eMail = :eMail")
    , @NamedQuery(name = "DetailsUsers.findByPhone1", query = "SELECT d FROM DetailsUsers d WHERE d.phone1 = :phone1")
    , @NamedQuery(name = "DetailsUsers.findByPhone2", query = "SELECT d FROM DetailsUsers d WHERE d.phone2 = :phone2")
    , @NamedQuery(name = "DetailsUsers.findByBirthdate", query = "SELECT d FROM DetailsUsers d WHERE d.birthdate = :birthdate")
    , @NamedQuery(name = "DetailsUsers.findByIsMale", query = "SELECT d FROM DetailsUsers d WHERE d.isMale = :isMale")
    , @NamedQuery(name = "DetailsUsers.findByUpdateDate", query = "SELECT d FROM DetailsUsers d WHERE d.updateDate = :updateDate")
    , @NamedQuery(name = "DetailsUsers.findByApprovalOfPolicies", query = "SELECT d FROM DetailsUsers d WHERE d.approvalOfPolicies = :approvalOfPolicies")
    , @NamedQuery(name = "DetailsUsers.findByApprovalOfPoliciesDate", query = "SELECT d FROM DetailsUsers d WHERE d.approvalOfPoliciesDate = :approvalOfPoliciesDate")
    , @NamedQuery(name = "DetailsUsers.findByDigitalSignature", query = "SELECT d FROM DetailsUsers d WHERE d.digitalSignature = :digitalSignature")
    , @NamedQuery(name = "DetailsUsers.findByUrlDigitalSignature", query = "SELECT d FROM DetailsUsers d WHERE d.urlDigitalSignature = :urlDigitalSignature")
    , @NamedQuery(name = "DetailsUsers.findByDetailsUserscolDate", query = "SELECT d FROM DetailsUsers d WHERE d.detailsUserscolDate = :detailsUserscolDate")})
public class DetailsUsers implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "in_id")
    private Integer inId;
    @OneToOne(cascade={CascadeType.PERSIST,CascadeType.REMOVE})
    @JoinColumn(name = "ManagementUsersIn_Id", referencedColumnName = "in_id")
    private ManagementUsers managementUsersInId;
    
//    @Column(name = "ManagementUsersIn_Id")
//    private Integer managementUsersInId;
    @Size(max = 45)
    @Column(name = "first_name")
    private String firstName;
    @Size(max = 45)
    @Column(name = "last_name")
    private String lastName;
    @Size(max = 45)
    @Column(name = "tz")
    private String tz;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 45)
    @Column(name = "`e-mail`")
    private String eMail;
    @Size(max = 45)
    @Column(name = "phone_1")
    private String phone1;
    @Size(max = 45)
    @Column(name = "phone_2")
    private String phone2;
    @Column(name = "birthdate")
    @Temporal(TemporalType.DATE)
    private Date birthdate;
    @Column(name = "is_male")
    private Boolean isMale;
    @Column(name = "update_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "approval_of_policies")
    private Boolean approvalOfPolicies;
    @Column(name = "approval_of_policies_date")
    @Temporal(TemporalType.DATE)
    private Date approvalOfPoliciesDate;
    @Column(name = "digital_signature")
    private Boolean digitalSignature;
    @Size(max = 60)
    @Column(name = "url_digital_signature")
    private String urlDigitalSignature;
    @Column(name = "details_userscol_date")
    @Temporal(TemporalType.DATE)
    private Date detailsUserscolDate;

    public DetailsUsers() {
    }

    public DetailsUsers(Integer inId) {
        this.inId = inId;
    }

    public Integer getInId() {
        return inId;
    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }

//    public Integer getManagementUsersInId() {
//        return managementUsersInId;
//    }
//
//    public void setManagementUsersInId(Integer managementUsersInId) {
//        this.managementUsersInId = managementUsersInId;
//    }

    public ManagementUsers getManagementUsersInId() {
        return managementUsersInId;
    }

    public void setManagementUsersInId(ManagementUsers managementUsersInId) {
        this.managementUsersInId = managementUsersInId;
    }
    
    
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTz() {
        return tz;
    }

    public void setTz(String tz) {
        this.tz = tz;
    }

    public String getEMail() {
        return eMail;
    }

    public void setEMail(String eMail) {
        this.eMail = eMail;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public Boolean getIsMale() {
        return isMale;
    }

    public void setIsMale(Boolean isMale) {
        this.isMale = isMale;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Boolean getApprovalOfPolicies() {
        return approvalOfPolicies;
    }

    public void setApprovalOfPolicies(Boolean approvalOfPolicies) {
        this.approvalOfPolicies = approvalOfPolicies;
    }

    public Date getApprovalOfPoliciesDate() {
        return approvalOfPoliciesDate;
    }

    public void setApprovalOfPoliciesDate(Date approvalOfPoliciesDate) {
        this.approvalOfPoliciesDate = approvalOfPoliciesDate;
    }

    public Boolean getDigitalSignature() {
        return digitalSignature;
    }

    public void setDigitalSignature(Boolean digitalSignature) {
        this.digitalSignature = digitalSignature;
    }

    public String getUrlDigitalSignature() {
        return urlDigitalSignature;
    }

    public void setUrlDigitalSignature(String urlDigitalSignature) {
        this.urlDigitalSignature = urlDigitalSignature;
    }

    public Date getDetailsUserscolDate() {
        return detailsUserscolDate;
    }

    public void setDetailsUserscolDate(Date detailsUserscolDate) {
        this.detailsUserscolDate = detailsUserscolDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inId != null ? inId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailsUsers)) {
            return false;
        }
        DetailsUsers other = (DetailsUsers) object;
        if ((this.inId == null && other.inId != null) || (this.inId != null && !this.inId.equals(other.inId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.DetailsUsers[ inId=" + inId + " ]";
    }
    
}
