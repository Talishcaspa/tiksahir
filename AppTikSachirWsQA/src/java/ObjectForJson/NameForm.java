/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ObjectForJson;

/**
 *
 * @author rachel
 */
public class NameForm {

    private String Symbol;
    private String formName;
    private String icon;

    public NameForm(String Symbol, String formName ,String icon) {
        this.Symbol = Symbol;
        this.formName = formName;
        this.icon = icon;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
    
      public NameForm() {
    }

    public String getSymbol() {
        return Symbol;
    }

    public void setSymbol(String Symbol) {
        this.Symbol = Symbol;
    }

    public String getFormName() {
        return formName;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }
    
}
