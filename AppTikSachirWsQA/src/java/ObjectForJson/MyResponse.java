/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ObjectForJson;

import Tools.Mistake;

/**
 *
 * @author rachel
 */
public class MyResponse<T>{

    
    private Mistake error;
    
    private  T result;
 
    public MyResponse() {

    }
    
    public MyResponse(Mistake error, T result) {
        this.error = error;
        this.result = result;
    }

    public Mistake getError() {
        return error;
    }

    public void setError(Mistake error) {
        this.error = error;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }
}