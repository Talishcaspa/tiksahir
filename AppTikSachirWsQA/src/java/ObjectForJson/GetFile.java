/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ObjectForJson;

/**
 *
 * @author rachel
 */
public class GetFile {

    public GetFile(String file, int worker_id, String file_symbol, String note, String file_type, String fileName, String user_name, String compCid, Integer month, Integer year) {
        this.file = file;
        this.worker_id = worker_id;
        this.file_symbol = file_symbol;
        this.note = note;
        this.file_type = file_type;
        this.fileName = fileName;
        this.user_name = user_name;
        this.compCid = compCid;
        this.month = month;
        this.year = year;
    }

    public GetFile(int inId, String note, String fileName, String compCid, int day, Integer month, Integer year, String filepath, boolean paid, double amount) {

        this.inId = inId;
        this.note = note;
        this.fileName = fileName;
        this.compCid = compCid;
        this.day = day;
        this.month = month;
        this.year = year;
        this.filepath = filepath;
        this.paid = paid;
        this.amount = amount;

    }

    public GetFile(int inId, String note, String file_symbol, String fileName, String compCid, int day, Integer month, Integer year, String filepath, boolean paid, double amount) {

        this.inId = inId;
        this.note = note;
        this.file_symbol = file_symbol;
        this.fileName = fileName;
        this.compCid = compCid;
        this.day = day;
        this.month = month;
        this.year = year;
        this.filepath = filepath;
        this.paid = paid;
        this.amount = amount;

    }
    // ID of the file
    private int inId;

    // File conversion to String
    private String file;

    // num worker of employee
    private int worker_id;

    // Type of file
    private String file_symbol;

    // Note
    private String note;

    // File extension
    private String file_type;

    // Name of the file
    private String fileName;

    // Name of the file on the server
    private String filepath;

    // The name of the user
    private String user_name;

    // File upload date
    private String data_upload;

    // Company number
    private String compCid;

    // day
    private int day;

    // month
    private Integer month;

    // year
    private Integer year;

    // if paid
    private Boolean paid;

    // amount of file
    private double amount;

    public int getInId() {
        return inId;
    }

    public void setInId(int inId) {
        this.inId = inId;
    }

    public String getCompCid() {
        return compCid;
    }

    public void setCompCid(String compCid) {
        this.compCid = compCid;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public GetFile() {
    }

    public void setFile(String file) {
        this.file = file;
    }

    public void setWorker_id(int worker_id) {
        this.worker_id = worker_id;
    }

    public void setFile_symbol(String file_symbol) {
        this.file_symbol = file_symbol;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setFile_type(String file_type) {
        this.file_type = file_type;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public void setData_upload(String data_upload) {
        this.data_upload = data_upload;
    }

    public String getFile() {
        return file;
    }

    public int getWorker_id() {
        return worker_id;
    }

    public String getFile_symbol() {
        return file_symbol;
    }

    public String getNote() {
        return note;
    }

    public String getFile_type() {
        return file_type;
    }

    public String getFileName() {
        return fileName;
    }

    public String getFilepath() {
        return filepath;
    }

    public String getUser_name() {
        return user_name;
    }

    public String getData_upload() {
        return data_upload;
    }

    public Boolean getPaid() {
        return paid;
    }

    public void setPaid(Boolean paid) {
        this.paid = paid;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
