package ObjectForJson;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import javax.persistence.Column;
import javax.validation.constraints.Size;

/**
 *
 * @author rachel
 */
public class ManagementUsersNew {

    private Integer inId;
    private Integer id;
    private String fullName;
    private String userName;
    private String userPass;
    private String pinCode;

    public Integer getInId() {
        return inId;
    }

    public Integer getId() {
        return id;
    }

    public String getFullName() {
        return fullName;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserPass() {
        return userPass;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public ManagementUsersNew(Integer inId, String fullName, String userName, String userPass, String pinCode) {
        this.inId = inId;
        this.fullName = fullName;
        this.userName = userName;
        this.userPass = userPass;
        this.pinCode = pinCode;
    }

    public ManagementUsersNew(int inId, int id, String fullName, String userName, String userPass, String pinCode) {
        this.inId = inId;
        this.id = id;
        this.fullName = fullName;
        this.userName = userName;
        this.userPass = userPass;
        this.pinCode = pinCode;

    }

    public ManagementUsersNew(int inId) {
        this.inId = inId;

    }

}
