/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Attendance;

//import Rachel.ManagementUsersNew;
//import Rachel.JsonInsertAbsence;
import AttendanceEntity.Attendance;
import AttendanceEntity.AttendanceAbsenteeism;
import AttendanceEntity.AttendanceAbsenteeismFacade;
import AttendanceEntity.AttendanceFacade;
import AttendanceEntity.Contacts;
import AttendanceEntity.ContactsFacade;
import AttendanceEntity.ManagementUsersAttendance;
import AttendanceEntity.ManagementUsersAttendanceFacade;
import AttendanceEntity.PresenceTable;
import AttendanceEntity.PresenceTableFacade;
import AttendanceEntity.SendMailTamplate;
import AttendanceEntity.SendMailTamplateFacade;
import AttendanceEntity.SugMessage;
import AttendanceEntity.SugMessageContacts;
import AttendanceEntity.SugMessageFacade;
import AttendanceEntity.Tadirut;
import AttendanceObjectForJson.JsonInsertAbsence;
import Entity.ManagementUsers;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.xml.ws.client.RequestContext;
//import entities.Attendance;
//import entities.AttendanceAbsenteeism;
//import entities.AttendanceAbsenteeismFacade;
//import entities.AttendanceFacade;
//import entities.Contacts;
//import entities.ContactsFacade;
//import entities.ManagementUsers;
//import entities.ManagementUsersAttendanceFacade;
//import entities.PresenceTable;
//import entities.PresenceTableFacade;
//import entities.SendMailTamplate;
//import entities.SendMailTamplateFacade;
//import entities.SugMessage;
//import entities.SugMessageContacts;
//import entities.SugMessageFacade;
//import entities.Tadirut;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Dictionary;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.toList;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author rivka
 */
@Path("attendance")
public class AttendanceResource {

    @Context
    private UriInfo context;

    @Context
    private HttpServletRequest servletRequest;
    /**
     * Creates a new instance of AttendanceResource
     */
    @EJB
    ManagementUsersAttendanceFacade usersFacade;
    @EJB
    PresenceTableFacade presenceFacade;
    @EJB
    AttendanceAbsenteeismFacade attendanceAbsenteeismFacade;
    @EJB
    AttendanceFacade attendanceFacade;
    @EJB
    SendMailTamplateFacade sendMailTamplateFacade;
    @EJB
    ContactsFacade contactsFacade;
    @EJB
    SugMessageFacade sugMessageFacade;

    public AttendanceResource() {
    }

    @GET
    @Path("getHello")
    public String getHello() {

        System.out.println("Rachel:   hello  try       !!!!!!!!!!!!!!!");
        return "hello";
    }

    /**
     * Retrieves representation of an instance of service.AttendanceResource
     *
     * @return an instance of java.lang.String
     */
//    @GET
//    @Path("checkType")
//    @Produces(MediaType.APPLICATION_JSON)
//    public List<attendanceAbsenteeismNew> checkType() {
//        List<AttendanceAbsenteeism> attendanceAbsenteeism = new ArrayList<AttendanceAbsenteeism>();
//        System.out.println("Rachel: step 111111111111111111");
//
//        ManagementUsers user = (ManagementUsers) servletRequest.getAttribute("user");
//        int type = presenceFacade.getType(user);
//        System.out.println("Rachel : type " + type);
//        if (type == 1) { // user has to insert entrance.
//            attendanceAbsenteeism = attendanceAbsenteeismFacade.getAttendanceAbsenteeism();
//        }
//        System.out.println("Rachel: step 222222222222222222");
//
//        List<attendanceAbsenteeismNew> Latt = new ArrayList<attendanceAbsenteeismNew>();
//        for (int i = 0; i < attendanceAbsenteeism.size(); i++) {
//
//            attendanceAbsenteeismNew att = new attendanceAbsenteeismNew(attendanceAbsenteeism.get(i).getInId().orElse(0), attendanceAbsenteeism.get(i).getAbsenteeism().orElse(""));
//            Latt.add(att);
//
//        }
//        return Latt;
//    }
    @POST
    @Path("insertAbsence")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
//    public ManagementUsers insertAbsence(MultivaluedMap<String, String> multivaluedMap) {
    public String insertAbsenceNew(String st) {

        System.out.println("Rachel: print json was get: " + st);

        List<AttendanceAbsenteeism> attendanceAbsenteeism = attendanceAbsenteeismFacade.getAttendanceAbsenteeism();

        ManagementUsers user1 = (ManagementUsers) servletRequest.getAttribute("user");
ManagementUsersAttendance user=usersFacade.find(user1.getInId());
        try {
            st = URLDecoder.decode(st, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(AttendanceResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        JsonInsertAbsence JsonInsertAbsence = gson.fromJson(st, JsonInsertAbsence.class);

        System.out.println("Rachel_newwwwww:::::::::::::::::::::::::::::::::: ");
        if (JsonInsertAbsence == null) {
            System.out.println("Rachel: JsonInsertAbsence.isEmpty()");
        } else {

            System.out.println("Rachel: JsonInsertAbsence" + JsonInsertAbsence);
            PresenceTable pr = new PresenceTable();
            pr.setIdUser(user);
//        pr.setIdUser(user.getInId());

            pr.setType(3);

            System.out.println("JsonInsertAbsence.getAbsenceType()" + JsonInsertAbsence.getAbsenceType());
            System.out.println("JsonInsertAbsence.getLatitude()" + JsonInsertAbsence.getLatitude());
            System.out.println("JsonInsertAbsence.getLongitude()" + JsonInsertAbsence.getLongitude());

//            boolean anyMatch = user.getPresences().stream().anyMatch(p-> p.getTimePresence().getDay()==new Date().getDay()).collect(Collectors.toList());
            System.out.println("Rachel : new Date().getDay() " + new Date().getDay());
            Date dateFormat = new Date();

            boolean anyMatch = user.getPresences().stream().anyMatch(p -> p.getTimePresence().getDay() == dateFormat.getDay());

            System.out.println("Rachel : anyMatch " + anyMatch);
            if (anyMatch) {
                return "Absente exit";
            }
            pr.setAbsenceType(JsonInsertAbsence.getAbsenceType());
            pr.setLatitude(JsonInsertAbsence.getLatitude());
            pr.setLongitude(JsonInsertAbsence.getLongitude());
            pr.setTimePresence(new Date());
            user.getPresences().add(pr);
            usersFacade.edit(user);
            System.out.println("Rachel:  end insertAbsence");
//        }

        }
        return ("end insertAbsence");

    }

    @POST
    @Path("insertAttendance")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public String insertAttendance(String st) {

        try {
            st = URLDecoder.decode(st, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(AttendanceResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        JsonInsertAbsence JsonInsertAbsence = gson.fromJson(st, JsonInsertAbsence.class);

        System.out.println("Rachel:  hello   ");

        ManagementUsers user1 = (ManagementUsers) servletRequest.getAttribute("user");
ManagementUsersAttendance user=usersFacade.find(user1.getInId());

        int type = presenceFacade.getType(user);
        System.out.println("Rachel:      type    type   type   type   type   type   type   type   type   :  " + type);

        PresenceTable pr = new PresenceTable();
        pr.setIdUser(user);
        pr.setType(type);
        pr.setAbsenceType(0);
        pr.setLatitude(JsonInsertAbsence.getLatitude());
        pr.setLongitude(JsonInsertAbsence.getLongitude());
        pr.setTimePresence(new Date());
        user.getPresences().add(pr);
        type =2;
        if (type == 1) { // user has to insert entrance.

            System.out.println("Rachel :  entet to if(type=1)");
            Attendance a = new Attendance();

            LocalDateTime now = LocalDateTime.now();
            Date startTimeD = Date.from(now.atZone(ZoneId.systemDefault()).toInstant());
            LocalDateTime endTime = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), 0, 0, 0);
            Date endTimeD = Date.from(endTime.atZone(ZoneId.systemDefault()).toInstant());

            a.setUser(user);
            a.setDate(startTimeD);
            a.setDayname(now.getDayOfWeek().toString());
            a.setStartTime(startTimeD);
            a.setEndTime(endTimeD);
            a.setSumForDay(endTimeD);
            a.setSumFor100(endTimeD);
            a.setSumFor125(endTimeD);
            a.setSumFor150(endTimeD);
            a.setSumFor200(endTimeD);
            a.setAbsenteeism("עבודה");
            a.setSource("app");
            user.getPresences().add(pr);
            user.getAttendances().add(a);

//            try {
            usersFacade.edit(user);
            //        sendMail(user, "כניסה");

//            } catch (Exception e) {
//
//                System.out.println(e.toString() + "*******************************************");
//            }
        } else {
            System.out.println("Rachel :  entet to else if(type=1)");

            LocalDateTime localNow = LocalDate.now().atTime(0, 0);
            Date nowDate = Date.from(localNow.atZone(ZoneId.systemDefault()).toInstant());

            Stream<Attendance> filter = user.getAttendances().stream().filter(p -> p.getDate().equals(nowDate));
            LocalDateTime now1 = LocalDateTime.now();
            LocalTime nowTime1 = LocalTime.now();
            System.out.println("Rachel :   filter. count()  " + filter.count());
            System.out.println("Rachel :   Date.from(nowTime.atDate(LocalDate.of(now.getYear(), now.getMonth(), now.getDayOfMonth())).\n"
                    + "                        atZone(ZoneId.systemDefault()).toInstant())   " + Date.from(nowTime1.atDate(LocalDate.of(now1.getYear(), now1.getMonth(), now1.getDayOfMonth())).
                            atZone(ZoneId.systemDefault()).toInstant()));

            user.getAttendances().stream().filter(p -> p.getDate().equals(nowDate)).forEach((p) -> {
                Date d = p.getStartTime();
                LocalDateTime now = LocalDateTime.now();
                LocalTime nowTime = LocalTime.now();
                LocalTime startTime = d.toInstant().atZone(ZoneId.systemDefault()).toLocalTime();

                p.setEndTime(Date.from(nowTime.atDate(LocalDate.of(now.getYear(), now.getMonth(), now.getDayOfMonth())).
                        atZone(ZoneId.systemDefault()).toInstant()));

                System.out.println("Rachel :   p.getEndTime()   " + p.getEndTime());
                System.out.println("Rachel :   Date.from(nowTime.atDate(LocalDate.of(now.getYear(), now.getMonth(), now.getDayOfMonth())).\n"
                        + "                        atZone(ZoneId.systemDefault()).toInstant())   " + Date.from(nowTime.atDate(LocalDate.of(now.getYear(), now.getMonth(), now.getDayOfMonth())).
                                atZone(ZoneId.systemDefault()).toInstant()));

                Duration between = Duration.between(startTime, nowTime);
                int hours = (int) between.toHours();
                between = between.minusHours(hours);
                int minutes = (int) between.toMinutes();
                between = between.minusMinutes(minutes);
                int seconds = (int) between.getSeconds();

                LocalDateTime diff = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), hours, minutes, seconds);
                Date sumForDayD = Date.from(diff.atZone(ZoneId.systemDefault()).toInstant());
                p.setSumForDay(sumForDayD);

                if (hours < 9) {
                    LocalDateTime sumFor100 = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), hours, minutes, seconds);
                    Date sumFor100D = Date.from(sumFor100.atZone(ZoneId.systemDefault()).toInstant());
                    Date.from(sumFor100.atZone(ZoneId.systemDefault()).toInstant());
                    p.setSumFor100(sumFor100D);
                } else if (hours < 11) {
                    LocalDateTime sumFor100 = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), 9, 0, 0);
                    Date sumFor100D = Date.from(sumFor100.atZone(ZoneId.systemDefault()).toInstant());
                    p.setSumFor100(sumFor100D);
                    diff = diff.minusHours(9);
                    Date sumFor125D = Date.from(diff.atZone(ZoneId.systemDefault()).toInstant());
                    p.setSumFor125(sumFor125D);

                } else {
                    LocalDateTime sumFor100 = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), 9, 0, 0);
                    Date sumFor100D = Date.from(sumFor100.atZone(ZoneId.systemDefault()).toInstant());
                    p.setSumFor100(sumFor100D);
                    diff = diff.minusHours(9);
                    LocalDateTime sumFor125 = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), 2, 0, 0);
                    Date sumFor125D = Date.from(sumFor125.atZone(ZoneId.systemDefault()).toInstant());
                    p.setSumFor125(sumFor125D);
                    diff = diff.minusHours(2);
                    Date sumFor150D = Date.from(diff.atZone(ZoneId.systemDefault()).toInstant());
                    p.setSumFor150(sumFor150D);
                }
            });

            System.out.println("Rachel :   user.getAttendances().get(0).getEndTime()   " + user.getAttendances().get(0).getEndTime());

            usersFacade.edit(user);
            //        sendMail(user, "יציאה");

        }
        System.out.println("Rachel :  removeeeeeee");

        return "finish";
    }

//    @POST
//    @Path("insertAttendance")
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    public String insertAttendance(MultivaluedMap<String, String> multivaluedMap) {
////    public String insertAttendance(MultivaluedMap<String, String> multivaluedMap) {
//
//        System.out.println("Rachel:  hello   ");
//        ManagementUsers user = (ManagementUsers) servletRequest.getAttribute("user");
//        String latitude = multivaluedMap.getFirst("latitude");
//        String longitude = multivaluedMap.getFirst("longitude");
//        int type = presenceFacade.getType(user);
//
//        PresenceTable pr = new PresenceTable();
//        pr.setIdUser(user);
//        pr.setType(type);
//        pr.setAbsenceType(0);
//        pr.setLatitude(Double.parseDouble(latitude));
//        pr.setLongitude(Double.parseDouble(longitude));
//        pr.setTimePresence(new Date());
//        user.getPresences().add(pr);
//
//        if (type == 1) { // user has to insert entrance.
//
//            Attendance a = new Attendance();
//
//            LocalDateTime now = LocalDateTime.now();
//            Date startTimeD = Date.from(now.atZone(ZoneId.systemDefault()).toInstant());
//            LocalDateTime endTime = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), 0, 0, 0);
//            Date endTimeD = Date.from(endTime.atZone(ZoneId.systemDefault()).toInstant());
//
//            a.setUser(user);
//            a.setDate(startTimeD);
//            a.setDayname(now.getDayOfWeek().toString());
//            a.setStartTime(startTimeD);
//            a.setEndTime(endTimeD);
//            a.setSumForDay(endTimeD);
//            a.setSumFor100(endTimeD);
//            a.setSumFor125(endTimeD);
//            a.setSumFor150(endTimeD);
//            a.setSumFor200(endTimeD);
//            a.setAbsenteeism("עבודה");
//            a.setSource("app");
//            user.getPresences().add(pr);
//            user.getAttendances().add(a);
//
////            try {
//            usersFacade.edit(user);
//            //        sendMail(user, "כניסה");
//
////            } catch (Exception e) {
////
////                System.out.println(e.toString() + "*******************************************");
////            }
//        } else {
//
//            LocalDateTime localNow = LocalDate.now().atTime(0, 0);
//            Date nowDate = Date.from(localNow.atZone(ZoneId.systemDefault()).toInstant());
//
//            user.getAttendances().stream().filter(p -> p.getDate().equals(nowDate)).forEach((p) -> {
//                Date d = p.getStartTime();
//                LocalDateTime now = LocalDateTime.now();
//                LocalTime nowTime = LocalTime.now();
//                LocalTime startTime = d.toInstant().atZone(ZoneId.systemDefault()).toLocalTime();
//
//                p.setEndTime(Date.from(nowTime.atDate(LocalDate.of(now.getYear(), now.getMonth(), now.getDayOfMonth())).
//                        atZone(ZoneId.systemDefault()).toInstant()));
//
//                Duration between = Duration.between(startTime, nowTime);
//                int hours = (int) between.toHours();
//                between = between.minusHours(hours);
//                int minutes = (int) between.toMinutes();
//                between = between.minusMinutes(minutes);
//                int seconds = (int) between.getSeconds();
//
//                LocalDateTime diff = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), hours, minutes, seconds);
//                Date sumForDayD = Date.from(diff.atZone(ZoneId.systemDefault()).toInstant());
//                p.setSumForDay(sumForDayD);
//
//                if (hours < 9) {
//                    LocalDateTime sumFor100 = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), hours, minutes, seconds);
//                    Date sumFor100D = Date.from(sumFor100.atZone(ZoneId.systemDefault()).toInstant());
//                    Date.from(sumFor100.atZone(ZoneId.systemDefault()).toInstant());
//                    p.setSumFor100(sumFor100D);
//                } else if (hours < 11) {
//                    LocalDateTime sumFor100 = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), 9, 0, 0);
//                    Date sumFor100D = Date.from(sumFor100.atZone(ZoneId.systemDefault()).toInstant());
//                    p.setSumFor100(sumFor100D);
//                    diff = diff.minusHours(9);
//                    Date sumFor125D = Date.from(diff.atZone(ZoneId.systemDefault()).toInstant());
//                    p.setSumFor125(sumFor125D);
//
//                } else {
//                    LocalDateTime sumFor100 = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), 9, 0, 0);
//                    Date sumFor100D = Date.from(sumFor100.atZone(ZoneId.systemDefault()).toInstant());
//                    p.setSumFor100(sumFor100D);
//                    diff = diff.minusHours(9);
//                    LocalDateTime sumFor125 = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), 2, 0, 0);
//                    Date sumFor125D = Date.from(sumFor125.atZone(ZoneId.systemDefault()).toInstant());
//                    p.setSumFor125(sumFor125D);
//                    diff = diff.minusHours(2);
//                    Date sumFor150D = Date.from(diff.atZone(ZoneId.systemDefault()).toInstant());
//                    p.setSumFor150(sumFor150D);
//                }
//            });
//            usersFacade.edit(user);
//            //        sendMail(user, "יציאה");
//
//        }
//        System.out.println("Rachel :  removeeeeeee");
//
//        return "finish";
//    }
//    @POST
//    @Path("insertExit")
//    @Produces(MediaType.TEXT_PLAIN)
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    public String insertExit(MultivaluedMap<String, String> multivaluedMap) {
//
//        String latitude = multivaluedMap.getFirst("latitude");
//        String longitude = multivaluedMap.getFirst("longitude");
//
//        ManagementUsers user = (ManagementUsers) servletRequest.getAttribute("user");
//
//        LocalDateTime localNow = LocalDate.now().atTime(0, 0);
//        Date nowDate = Date.from(localNow.atZone(ZoneId.systemDefault()).toInstant());
//
//        PresenceTable pr = new PresenceTable();
//        pr.setIdUser(user);
//        pr.setType(2);
//        pr.setAbsenceType(0);
//        pr.setLatitude(Double.parseDouble(latitude));
//        pr.setLongitude(Double.parseDouble(longitude));
//        pr.setTimePresence(new Date());
//        user.getPresences().add(pr);
//
//        user.getAttendances().stream().filter(p -> p.getDate().equals(nowDate)).forEach((p) -> {
//            Date d = p.getStartTime();
//            LocalDateTime now = LocalDateTime.now();
//            LocalTime nowTime = LocalTime.now();
//            LocalTime startTime = d.toInstant().atZone(ZoneId.systemDefault()).toLocalTime();
//
//            p.setEndTime(Date.from(nowTime.atDate(LocalDate.of(now.getYear(), now.getMonth(), now.getDayOfMonth())).
//                    atZone(ZoneId.systemDefault()).toInstant()));
//
//            Duration between = Duration.between(startTime, nowTime);
//            int hours = (int) between.toHours();
//            between = between.minusHours(hours);
//            int minutes = (int) between.toMinutes();
//            between = between.minusMinutes(minutes);
//            int seconds = (int) between.getSeconds();
//
//            LocalDateTime diff = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), hours, minutes, seconds);
//            Date sumForDayD = Date.from(diff.atZone(ZoneId.systemDefault()).toInstant());
//            p.setSumForDay(sumForDayD);
//
//            if (hours < 9) {
//                LocalDateTime sumFor100 = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), hours, minutes, seconds);
//                Date sumFor100D = Date.from(sumFor100.atZone(ZoneId.systemDefault()).toInstant());
//                Date.from(sumFor100.atZone(ZoneId.systemDefault()).toInstant());
//                p.setSumFor100(sumFor100D);
//            } else if (hours < 11) {
//                LocalDateTime sumFor100 = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), 9, 0, 0);
//                Date sumFor100D = Date.from(sumFor100.atZone(ZoneId.systemDefault()).toInstant());
//                p.setSumFor100(sumFor100D);
//                diff = diff.minusHours(9);
//                Date sumFor125D = Date.from(diff.atZone(ZoneId.systemDefault()).toInstant());
//                p.setSumFor125(sumFor125D);
//
//            } else {
//                LocalDateTime sumFor100 = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), 9, 0, 0);
//                Date sumFor100D = Date.from(sumFor100.atZone(ZoneId.systemDefault()).toInstant());
//                p.setSumFor100(sumFor100D);
//                diff = diff.minusHours(9);
//                LocalDateTime sumFor125 = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), 2, 0, 0);
//                Date sumFor125D = Date.from(sumFor125.atZone(ZoneId.systemDefault()).toInstant());
//                p.setSumFor125(sumFor125D);
//                diff = diff.minusHours(2);
//                Date sumFor150D = Date.from(diff.atZone(ZoneId.systemDefault()).toInstant());
//                p.setSumFor150(sumFor150D);
//            }
//        });
//        usersFacade.edit(user);
////        sendMail(user, "יציאה");
//
//        return "finish";
//    }
    public String sendMail(ManagementUsersAttendance user, String subject) {

        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formatDateTime = now.format(formatter);

        DateFormat dateFormatHour = new SimpleDateFormat("HH");
        DateFormat dateFormatMinutes = new SimpleDateFormat("mm");

        Date timeToSend = new Date(System.currentTimeMillis() + 5 * 60 * 1000);

        Tadirut t = new Tadirut();
        t.setDescription(" ");
        t.setMinutes(dateFormatMinutes.format(timeToSend));
        t.setHours(dateFormatHour.format(timeToSend));
        t.setDaysInMonth("*");
        t.setMonth("*");
        t.setDaysInWeek("*");

        SendMailTamplate mailTemplate = sendMailTamplateFacade.getMailTemplate();
        String template = mailTemplate.getTemplate();

        template = template.replace("&fullName&", " ");
        template = template.replace("&employeeName&", user.getFullName());
        template = template.replace("&time&", formatDateTime);
        template = template.replace("&updateType&", subject);

        List<Contacts> contactsList = contactsFacade.getContacts();

        List<SugMessageContacts> messageContactses = new ArrayList<SugMessageContacts>();
        SugMessageContacts smc;

        SugMessage s = new SugMessage();

        for (Contacts c : contactsList) {
            smc = new SugMessageContacts();
            smc.setEboxContactInid(c);
            smc.setSugMessageInid(s);
            messageContactses.add(smc);
        }

        s.setUser("moked.caspabeta");
        s.setHafala(Boolean.TRUE);
        s.setEmail(Boolean.TRUE);
        s.setSms(Boolean.TRUE);
        s.setEmailContent(template);
        s.setEmailTitlle(user.getFullName() + " מדווח על " + subject + " ב: " + formatDateTime);
        s.setSMSContent(user.getFullName() + " מדווח על " + subject + " ב: " + formatDateTime);
        s.setRecordedContent(" ");
        s.setFaxContent(" ");
        s.setTaarihThilatSendMessage(timeToSend);
        s.setSofSendMessage(timeToSend);
        s.setTadirout(t);
        s.setSMSnumber(1);
        s.setFAXnumber(1);
        s.setEmailadress(1);
        s.setRecordToWho(1);
        s.setDescription("נוכחות" + user.getFullName());
        s.setTaarihHahnassa(timeToSend);
        s.setSugMessageContacts(messageContactses);

        sugMessageFacade.create(s);

        return "finish";
    }
}
