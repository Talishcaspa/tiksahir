/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Attendance;

import AttendanceEntity.Attendance;
import AttendanceEntity.ManagementUsersAttendance;
import AttendanceEntity.ManagementUsersAttendanceFacade;
import Entity.FormMenu;
import Entity.ManagementUsers;
import ObjectForJson.MyResponse;
import Tools.Mistake;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonStructure;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * REST Web Service
 *
 * @author rachel
 */

//franck

@Path("AttendanceRachel")
public class AttendanceRachelResource {

    @Context
    private UriInfo context;
    @Context
    private HttpServletRequest servletRequest;

    /**
     * Creates a new instance of AttendanceRachelResource
     */
    @EJB
    ManagementUsersAttendanceFacade usersFacade;

    public AttendanceRachelResource() {
    }

    /**
     * Retrieves representation of an instance of
     * Attendance.AttendanceRachelResource
     *
     * @return an instance of java.lang.String
     */ 
    
    //    <editor-fold defaultstate="collapsed"  desc="getListAttendance">
    // get list of attendance;

    @POST
    @Path("getListAttendance")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getListFormMenu() throws IOException, JSONException {

        System.out.println("Rachel :   start getListForm  !!!!!!!!! ");

        ManagementUsers user1 = (ManagementUsers) servletRequest.getAttribute("user");
        ManagementUsersAttendance user = usersFacade.find(user1.getInId());
        // get list of attendances

        ArrayList<Attendance> attendances = user.getAttendances();
        System.out.println("Rachel :   After downloading the table attendances  " + attendances.size());

        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();
        Gson gsonReturn = builder.create();

        DateFormat df = new SimpleDateFormat("MMM d, yyyy HH:mm:ss");

        Map<String, String> mapJson = new HashMap<String, String>();
        List<Map<String, String>> LMapJson = new ArrayList<Map<String, String>>();
        JsonObject build = null;
        List<JsonObject> LJson = new ArrayList<JsonObject>();

        for (Attendance attendance : attendances) {

            mapJson.put("date",df.format( attendance.getDate()));
            mapJson.put("startTime",df.format( attendance.getStartTime()));
            mapJson.put("endTime", df.format(attendance.getEndTime()));
            mapJson.put("sumFor100",df.format( attendance.getSumFor100()));
            mapJson.put("sumFor125",df.format( attendance.getSumFor125()));

            build =  Json.createObjectBuilder()
                    .add("date",df.format( attendance.getDate()))
                    .add("startTime", attendance.getStartTime().toString())
                    .add("endTime", attendance.getEndTime().toString())
                    .add("sumFor100", attendance.getSumFor100().toString())
                    .add("sumFor125", attendance.getSumFor125().toString())
                    .build();

            LMapJson.add(mapJson);
            LJson.add(build);
        }

        MyResponse response = new MyResponse(new Mistake(),LMapJson);
        String json = gsonReturn.toJson(response);
        return Response.ok().entity(json).type("text/plain;charset=UTF-8").build();

    }
    //</editor-fold>

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of
     * AttendanceRachelResource
     *
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
