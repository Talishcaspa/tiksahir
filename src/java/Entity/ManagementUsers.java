/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rachel
 */
@Entity
@Table(name = "system_management.management_users")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ManagementUsers.findAll", query = "SELECT m FROM ManagementUsers m")
    , @NamedQuery(name = "ManagementUsers.findByInId", query = "SELECT m FROM ManagementUsers m WHERE m.inId = :inId")
    , @NamedQuery(name = "ManagementUsers.findById", query = "SELECT m FROM ManagementUsers m WHERE m.id = :id")
    , @NamedQuery(name = "ManagementUsers.findByFullName", query = "SELECT m FROM ManagementUsers m WHERE m.fullName = :fullName")
    , @NamedQuery(name = "ManagementUsers.findByUserName", query = "SELECT m FROM ManagementUsers m WHERE m.userName = :userName")
    , @NamedQuery(name = "ManagementUsers.findByUserPass", query = "SELECT m FROM ManagementUsers m WHERE m.userPass = :userPass")
    , @NamedQuery(name = "ManagementUsers.findByPinCode", query = "SELECT m FROM ManagementUsers m WHERE m.pinCode = :pinCode")
    , @NamedQuery(name = "ManagementUsers.findByExpiredDatePass", query = "SELECT m FROM ManagementUsers m WHERE m.expiredDatePass = :expiredDatePass")
    , @NamedQuery(name = "ManagementUsers.findByUserMail", query = "SELECT m FROM ManagementUsers m WHERE m.userMail = :userMail")
    , @NamedQuery(name = "ManagementUsers.findByLastLogin", query = "SELECT m FROM ManagementUsers m WHERE m.lastLogin = :lastLogin")
    , @NamedQuery(name = "ManagementUsers.findByNumBadLogin", query = "SELECT m FROM ManagementUsers m WHERE m.numBadLogin = :numBadLogin")
    , @NamedQuery(name = "ManagementUsers.findByBlocked", query = "SELECT m FROM ManagementUsers m WHERE m.blocked = :blocked")
    , @NamedQuery(name = "ManagementUsers.findByStartPage", query = "SELECT m FROM ManagementUsers m WHERE m.startPage = :startPage")
    , @NamedQuery(name = "ManagementUsers.findByGroupDataEditors", query = "SELECT m FROM ManagementUsers m WHERE m.groupDataEditors = :groupDataEditors")
    , @NamedQuery(name = "ManagementUsers.findByGroupDataViewers", query = "SELECT m FROM ManagementUsers m WHERE m.groupDataViewers = :groupDataViewers")
    , @NamedQuery(name = "ManagementUsers.findByGroupViewEditors", query = "SELECT m FROM ManagementUsers m WHERE m.groupViewEditors = :groupViewEditors")
    , @NamedQuery(name = "ManagementUsers.findByGroupViewViewers", query = "SELECT m FROM ManagementUsers m WHERE m.groupViewViewers = :groupViewViewers")
    , @NamedQuery(name = "ManagementUsers.findByCompanyPermission", query = "SELECT m FROM ManagementUsers m WHERE m.companyPermission = :companyPermission")
    , @NamedQuery(name = "ManagementUsers.findByCostPerHour", query = "SELECT m FROM ManagementUsers m WHERE m.costPerHour = :costPerHour")
    , @NamedQuery(name = "ManagementUsers.findByCompany", query = "SELECT m FROM ManagementUsers m WHERE m.company = :company")
    , @NamedQuery(name = "ManagementUsers.findByUserType", query = "SELECT m FROM ManagementUsers m WHERE m.userType = :userType")
    , @NamedQuery(name = "ManagementUsers.findByErpType", query = "SELECT m FROM ManagementUsers m WHERE m.erpType = :erpType")
    , @NamedQuery(name = "ManagementUsers.findByDpayType", query = "SELECT m FROM ManagementUsers m WHERE m.dpayType = :dpayType")
    , @NamedQuery(name = "ManagementUsers.findByBikoretType", query = "SELECT m FROM ManagementUsers m WHERE m.bikoretType = :bikoretType")
    , @NamedQuery(name = "ManagementUsers.findByPhone", query = "SELECT m FROM ManagementUsers m WHERE m.phone = :phone")
    , @NamedQuery(name = "ManagementUsers.findByActive", query = "SELECT m FROM ManagementUsers m WHERE m.active = :active")
    , @NamedQuery(name = "ManagementUsers.findByUserLang", query = "SELECT m FROM ManagementUsers m WHERE m.userLang = :userLang")
    , @NamedQuery(name = "ManagementUsers.findByColor", query = "SELECT m FROM ManagementUsers m WHERE m.color = :color")
    , @NamedQuery(name = "ManagementUsers.findByCalendar", query = "SELECT m FROM ManagementUsers m WHERE m.calendar = :calendar")
    , @NamedQuery(name = "ManagementUsers.findByDashboard", query = "SELECT m FROM ManagementUsers m WHERE m.dashboard = :dashboard")
    , @NamedQuery(name = "ManagementUsers.findByPicture", query = "SELECT m FROM ManagementUsers m WHERE m.picture = :picture")
    , @NamedQuery(name = "ManagementUsers.findByTopBar", query = "SELECT m FROM ManagementUsers m WHERE m.topBar = :topBar")
    , @NamedQuery(name = "ManagementUsers.findByFamily", query = "SELECT m FROM ManagementUsers m WHERE m.family = :family")
    , @NamedQuery(name = "ManagementUsers.findByUserAndPinCode", query = "SELECT m FROM ManagementUsers m WHERE m.userName = :userName AND m.pinCode = :pinCode")
    , @NamedQuery(name = "ManagementUsers.findByUser", query = "SELECT m FROM ManagementUsers m WHERE m.userName = :userName")

})
public class ManagementUsers implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "in_id")
    private Integer inId;
    @Column(name = "id")
    private Integer id;
    @Size(max = 45)
    @Column(name = "full_name")
    private String fullName;
    @Size(max = 45)
    @Column(name = "user_name")
    private String userName;
    @Size(max = 32)
    @Column(name = "user_pass")
    private String userPass;
    @Size(max = 45)
    @Column(name = "pin_code")
    private String pinCode;
    @Column(name = "expired_date_pass")
    @Temporal(TemporalType.DATE)
    private Date expiredDatePass;
    @Size(max = 45)
    @Column(name = "user_mail")
    private String userMail;
    @Column(name = "last_login")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastLogin;
    @Column(name = "num_bad_login")
    private Integer numBadLogin;
    @Column(name = "blocked")
    private Boolean blocked;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "start_page")
    private String startPage;
    @Size(max = 100)
    @Column(name = "group_data_editors")
    private String groupDataEditors;
    @Size(max = 100)
    @Column(name = "group_data_viewers")
    private String groupDataViewers;
    @Size(max = 100)
    @Column(name = "group_view_editors")
    private String groupViewEditors;
    @Size(max = 100)
    @Column(name = "group_view_viewers")
    private String groupViewViewers;
    @Column(name = "company_permission")
    private Integer companyPermission;
    @Column(name = "cost_per_hour")
    private Integer costPerHour;
    @Column(name = "company")
    private Integer company;
    @Basic(optional = false)
    @NotNull
    @Column(name = "user_type")
    private int userType;
    @Basic(optional = false)
    @NotNull
    @Column(name = "erp_type")
    private int erpType;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dpay_type")
    private int dpayType;
    @Basic(optional = false)
    @NotNull
    @Column(name = "bikoret_type")
    private int bikoretType;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 45)
    @Column(name = "phone")
    private String phone;
    @Column(name = "active")
    private Integer active;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "user_lang")
    private String userLang;
    @Size(max = 200)
    @Column(name = "color")
    private String color;
    @Basic(optional = false)
    @NotNull
    @Column(name = "calendar")
    private int calendar;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dashboard")
    private int dashboard;
    @Column(name = "picture")
    private Integer picture;
    @Basic(optional = false)
    @NotNull
    @Column(name = "top_bar")
    private int topBar;
    @Size(max = 60)
    @Column(name = "family")
    private String family;
    
//        @OneToOne(mappedBy = "ManagementUsersIn_Id")
//        private DetailsUsers Details;
            
//    @OneToOne(mappedBy = "managementUsersInId", cascade={CascadeType.PERSIST,CascadeType.REMOVE})
////  //  @JoinColumn(name = "DetailsUsers1111", referencedColumnName = "in_id")
//    private DetailsUsers Details;
    
//    @OneToMany(mappedBy = "NiolVersion", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
//    public Collection<KovatzZicuim2000> KovatzZicuim2000Collection;

    public ManagementUsers() {
    }

//    public DetailsUsers getDetails() {
//        return Details;
//    }
//
//    public void setDetails(DetailsUsers Details) {
//        this.Details = Details;
//    }

    public ManagementUsers(Integer inId) {
        this.inId = inId;
    }

    public ManagementUsers(Integer inId, String startPage, int userType, int erpType, int dpayType, int bikoretType, String userLang, int calendar, int dashboard, int topBar) {
        this.inId = inId;
        this.startPage = startPage;
        this.userType = userType;
        this.erpType = erpType;
        this.dpayType = dpayType;
        this.bikoretType = bikoretType;
        this.userLang = userLang;
        this.calendar = calendar;
        this.dashboard = dashboard;
        this.topBar = topBar;
    }
    
       public ManagementUsers( String startPage, int userType, int erpType, int dpayType, int bikoretType, String userLang, int calendar, int dashboard, int topBar) {
        this.startPage = startPage;
        this.userType = userType;
        this.erpType = erpType;
        this.dpayType = dpayType;
        this.bikoretType = bikoretType;
        this.userLang = userLang;
        this.calendar = calendar;
        this.dashboard = dashboard;
        this.topBar = topBar;
    }
    
       public ManagementUsers(int inId, String fullName, String userName,String userPass,String pinCode ){
        this.inId = inId;
        this.fullName = fullName;
        this.userName = userName;
        this.userPass = userPass;
        this.pinCode = pinCode;

    }

    public Integer getInId() {
        return inId;
    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPass() {
        return userPass;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public Date getExpiredDatePass() {
        return expiredDatePass;
    }

    public void setExpiredDatePass(Date expiredDatePass) {
        this.expiredDatePass = expiredDatePass;
    }

    public String getUserMail() {
        return userMail;
    }

    public void setUserMail(String userMail) {
        this.userMail = userMail;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Integer getNumBadLogin() {
        return numBadLogin;
    }

    public void setNumBadLogin(Integer numBadLogin) {
        this.numBadLogin = numBadLogin;
    }

    public Boolean getBlocked() {
        return blocked;
    }

    public void setBlocked(Boolean blocked) {
        this.blocked = blocked;
    }

    public String getStartPage() {
        return startPage;
    }

    public void setStartPage(String startPage) {
        this.startPage = startPage;
    }

    public String getGroupDataEditors() {
        return groupDataEditors;
    }

    public void setGroupDataEditors(String groupDataEditors) {
        this.groupDataEditors = groupDataEditors;
    }

    public String getGroupDataViewers() {
        return groupDataViewers;
    }

    public void setGroupDataViewers(String groupDataViewers) {
        this.groupDataViewers = groupDataViewers;
    }

    public String getGroupViewEditors() {
        return groupViewEditors;
    }

    public void setGroupViewEditors(String groupViewEditors) {
        this.groupViewEditors = groupViewEditors;
    }

    public String getGroupViewViewers() {
        return groupViewViewers;
    }

    public void setGroupViewViewers(String groupViewViewers) {
        this.groupViewViewers = groupViewViewers;
    }

    public Integer getCompanyPermission() {
        return companyPermission;
    }

    public void setCompanyPermission(Integer companyPermission) {
        this.companyPermission = companyPermission;
    }

    public Integer getCostPerHour() {
        return costPerHour;
    }

    public void setCostPerHour(Integer costPerHour) {
        this.costPerHour = costPerHour;
    }

    public Integer getCompany() {
        return company;
    }

    public void setCompany(Integer company) {
        this.company = company;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public int getErpType() {
        return erpType;
    }

    public void setErpType(int erpType) {
        this.erpType = erpType;
    }

    public int getDpayType() {
        return dpayType;
    }

    public void setDpayType(int dpayType) {
        this.dpayType = dpayType;
    }

    public int getBikoretType() {
        return bikoretType;
    }

    public void setBikoretType(int bikoretType) {
        this.bikoretType = bikoretType;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public String getUserLang() {
        return userLang;
    }

    public void setUserLang(String userLang) {
        this.userLang = userLang;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getCalendar() {
        return calendar;
    }

    public void setCalendar(int calendar) {
        this.calendar = calendar;
    }

    public int getDashboard() {
        return dashboard;
    }

    public void setDashboard(int dashboard) {
        this.dashboard = dashboard;
    }

    public Integer getPicture() {
        return picture;
    }

    public void setPicture(Integer picture) {
        this.picture = picture;
    }

    public int getTopBar() {
        return topBar;
    }

    public void setTopBar(int topBar) {
        this.topBar = topBar;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

//    public DetailsUsers getDetailsUsers() {
//        return DetailsUsers;
//    }
//    
//    public void setDetailsUsers(DetailsUsers DetailsUsers) {
//        this.DetailsUsers = DetailsUsers;
//    }
//    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inId != null ? inId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ManagementUsers)) {
            return false;
        }
        ManagementUsers other = (ManagementUsers) object;
        if ((this.inId == null && other.inId != null) || (this.inId != null && !this.inId.equals(other.inId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.ManagementUsers[ inId=" + inId + " ]";
    }
    
}
