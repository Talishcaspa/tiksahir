/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rachel
 */
@Entity
@Table(name = "app_rachel.Original_Files")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OriginalFiles.findAll", query = "SELECT o FROM OriginalFiles o")
    , @NamedQuery(name = "OriginalFiles.findByInId", query = "SELECT o FROM OriginalFiles o WHERE o.inId = :inId")
    , @NamedQuery(name = "OriginalFiles.findByNumFile", query = "SELECT o FROM OriginalFiles o WHERE o.numFile = :numFile")
    , @NamedQuery(name = "OriginalFiles.findByFileName", query = "SELECT o FROM OriginalFiles o WHERE o.fileName = :fileName")
    , @NamedQuery(name = "OriginalFiles.findByUrlFile", query = "SELECT o FROM OriginalFiles o WHERE o.urlFile = :urlFile")})
public class OriginalFiles implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "in_id")
    private Integer inId;
    @Column(name = "num_file")
    private Integer numFile;
    @Size(max = 45)
    @Column(name = "file_name")
    private String fileName;
    @Size(max = 200)
    @Column(name = "url_file")
    private String urlFile;

    public OriginalFiles() {
    }

    public OriginalFiles(Integer inId) {
        this.inId = inId;
    }

    public Integer getInId() {
        return inId;
    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }
    
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getUrlFile() {
        return urlFile;
    }

    public void setUrlFile(String urlFile) {
        this.urlFile = urlFile;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inId != null ? inId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OriginalFiles)) {
            return false;
        }
        OriginalFiles other = (OriginalFiles) object;
        if ((this.inId == null && other.inId != null) || (this.inId != null && !this.inId.equals(other.inId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.OriginalFiles[ inId=" + inId + " ]";
    }
    
}
