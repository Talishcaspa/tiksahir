/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author rachel
 */
@Stateless
public class UsersFacade extends AbstractFacade<Users> {

    @PersistenceContext(unitName = "AppTikSachirWsPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsersFacade() {
        super(Users.class);
    }

    public List<Users> findListID(String id) {
        return em.createNamedQuery("Users.findById").setParameter("id", id).getResultList();
    }

    public Users getUserDetails(String userName, String userPass) {

        List<Users> UsersList = em.createNamedQuery("Users.findById").setParameter("id", userName).getResultList();

        return check(UsersList, userPass);
    }

    private Users check(List<Users> UsersList, String userPass) {

        Users user = new Users();
        user.setInId(-1);
        if (UsersList.size() > 0) {
            try {

                user = UsersList.get(0);

                String md5 = null;

                MessageDigest md = MessageDigest.getInstance("MD5");

                md.update(userPass.getBytes());

                byte[] digest = md.digest();

                md5 = new BigInteger(1, digest).toString(16);

                if (!md5.equals(user.getPassword())) {
                    Users user1 = new Users();
                    user1.setInId(-1);

                    return user1;
                }
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(ManagementUsersFacade.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        return user;
    }

}
