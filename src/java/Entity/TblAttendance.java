/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rachel
 */
@Entity
@Table(name = "app_rachel.tbl_attendance")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblAttendance.findAll", query = "SELECT t FROM TblAttendance t")
    , @NamedQuery(name = "TblAttendance.findByInId", query = "SELECT t FROM TblAttendance t WHERE t.inId = :inId")
//    , @NamedQuery(name = "TblAttendance.findByInIdUser", query = "SELECT t FROM TblAttendance t WHERE t.inIdUser = :inIdUser")
    , @NamedQuery(name = "TblAttendance.findByYear", query = "SELECT t FROM TblAttendance t WHERE t.year = :year")
    , @NamedQuery(name = "TblAttendance.findByMonth", query = "SELECT t FROM TblAttendance t WHERE t.month = :month")
    , @NamedQuery(name = "TblAttendance.findByDay", query = "SELECT t FROM TblAttendance t WHERE t.day = :day")
    , @NamedQuery(name = "TblAttendance.findByDayName", query = "SELECT t FROM TblAttendance t WHERE t.dayName = :dayName")
    , @NamedQuery(name = "TblAttendance.findByStartTime", query = "SELECT t FROM TblAttendance t WHERE t.startTime = :startTime")
    , @NamedQuery(name = "TblAttendance.findByEndTime", query = "SELECT t FROM TblAttendance t WHERE t.endTime = :endTime")
    , @NamedQuery(name = "TblAttendance.findByIsExit", query = "SELECT t FROM TblAttendance t WHERE t.isExit = :isExit")
    , @NamedQuery(name = "TblAttendance.findBySource", query = "SELECT t FROM TblAttendance t WHERE t.source = :source")})
public class TblAttendance implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "in_id")
    private Integer inId;
//    @Column(name = "in_id_user")
//    private Integer inIdUser;
    @Column(name = "year")
    private Integer year;
    @Column(name = "month")
    private Integer month;
    @Column(name = "day")
    private Integer day;
    @Size(max = 10)
    @Column(name = "day_name")
    private String dayName;
    @Column(name = "start_time")
    @Temporal(TemporalType.TIME)
    private Date startTime;
    @Column(name = "end_time")
    @Temporal(TemporalType.TIME)
    private Date endTime;
    @Column(name = "is_exit")
    private Boolean isExit;
    @Size(max = 45)
    @Column(name = "source")
    private String source;
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    @JoinColumn(name = "in_id_user", referencedColumnName = "in_id")
    private Users Users;

    public Users getUsers() {
        return Users;
    }

    public void setUsers(Users Users) {
        this.Users = Users;
    }

    public TblAttendance() {
    }

     public TblAttendance(Users user) {
         this.Users = user;
    }
     
     public TblAttendance(Integer inId) {
        this.inId = inId;
    }

    public Integer getInId() {
        return inId;
    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }

//    public Integer getInIdUser() {
//        return inIdUser;
//    }
//
//    public void setInIdUser(Integer inIdUser) {
//        this.inIdUser = inIdUser;
//    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date andTime) {
        this.endTime = andTime;
    }

    public Boolean getIsExit() {
        return isExit;
    }

    public void setIsExit(Boolean isExit) {
        this.isExit = isExit;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inId != null ? inId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblAttendance)) {
            return false;
        }
        TblAttendance other = (TblAttendance) object;
        if ((this.inId == null && other.inId != null) || (this.inId != null && !this.inId.equals(other.inId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.TblAttendance[ inId=" + inId + " ]";
    }
    
}
