/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
//import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rachel
 */
@Entity
@Table(name = "app_rachel.users")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Users.findAll", query = "SELECT u FROM Users u")
    , @NamedQuery(name = "Users.findByInId", query = "SELECT u FROM Users u WHERE u.inId = :inId")
    , @NamedQuery(name = "Users.findById", query = "SELECT u FROM Users u WHERE u.id = :id")
    , @NamedQuery(name = "Users.findByStatus", query = "SELECT u FROM Users u WHERE u.status = :status")
    , @NamedQuery(name = "Users.findByDateOfInitialRegistration", query = "SELECT u FROM Users u WHERE u.dateOfInitialRegistration = :dateOfInitialRegistration")
    , @NamedQuery(name = "Users.findByNumberOfTries", query = "SELECT u FROM Users u WHERE u.numberOfTries = :numberOfTries")
    , @NamedQuery(name = "Users.findByLastTrialDate", query = "SELECT u FROM Users u WHERE u.lastTrialDate = :lastTrialDate")
    , @NamedQuery(name = "Users.findByPassword", query = "SELECT u FROM Users u WHERE u.password = :password")
    , @NamedQuery(name = "Users.findByDateCreatedPassword", query = "SELECT u FROM Users u WHERE u.dateCreatedPassword = :dateCreatedPassword")
    , @NamedQuery(name = "Users.findByPinCode", query = "SELECT u FROM Users u WHERE u.pinCode = :pinCode")
    , @NamedQuery(name = "Users.findByDateCreatedPinCode", query = "SELECT u FROM Users u WHERE u.dateCreatedPinCode = :dateCreatedPinCode")
    , @NamedQuery(name = "Users.findByTemporary", query = "SELECT u FROM Users u WHERE u.temporary = :temporary")
    , @NamedQuery(name = "Users.findByIdTemporary", query = "SELECT u FROM Users u WHERE u.id = :id AND u.temporary = FALSE")
})
public class Users implements Serializable {

    private static final long serialVersionUID = 1L;
   @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "in_id")
    private Integer inId;
//    @Basic(optional = false)
//    @NotNull
//    @Size(min = 1, max = 9)
    @Column(name = "id")
    private String id;
    @Column(name = "status")
    private Integer status;
    @Column(name = "date_of_initial_registration")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfInitialRegistration;
    @Column(name = "number_of_tries")
    private Integer numberOfTries;
    @Column(name = "last_trial_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastTrialDate;
    @Size(max = 45)
    @Column(name = "password")
    private String password;
    @Column(name = "date_created_password")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreatedPassword;
    @Size(max = 10)
    @Column(name = "pin_code")
    private String pinCode;
    @Column(name = "date_created_pin_code")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreatedPinCode;
    @Column(name = "Temporary")
    private boolean temporary;

    @OneToOne(mappedBy = "Users", cascade = {CascadeType.REFRESH, CascadeType.DETACH, CascadeType.ALL})
    private DetailsUsers Details;

    public DetailsUsers getDetails() {
        return Details;
    }

    public void setDetails(DetailsUsers Details) {
        this.Details = Details;
    }
    
    @OneToMany(mappedBy = "Users",cascade={CascadeType.ALL})
    private List<ActivityDocumentation> ListActivityDocumentation;

    public List<ActivityDocumentation> getListActivityDocumentation() {
        return ListActivityDocumentation;
    }

    public void setListActivityDocumentation(List<ActivityDocumentation> ListActivityDocumentation) {
        this.ListActivityDocumentation = ListActivityDocumentation;
    }

    @OneToMany(mappedBy = "Users",cascade={CascadeType.ALL})
    private List<TblAttendance> ListTblAttendance;

    public List<TblAttendance> getListTblAttendance() {
        return ListTblAttendance;
    }

    public void setListTblAttendance(List<TblAttendance> ListTblAttendance) {
        this.ListTblAttendance = ListTblAttendance;
    }
        
    @OneToMany(mappedBy = "Users",cascade={CascadeType.ALL})
    private List<TblPayment> ListTblPayment;

    public List<TblPayment> getListTblPayment() {
        return ListTblPayment;
    }

    public void setListTblPayment(List<TblPayment> ListTblPayment) {
        this.ListTblPayment = ListTblPayment;
    }
    
    @OneToMany(mappedBy = "Users",cascade={CascadeType.ALL})
    private List<RequiredUserFiles> ListRequiredUserFiles;

    public List<RequiredUserFiles> getListRequiredUserFiles() {
        return ListRequiredUserFiles;
    }

    public void setListRequiredUserFiles(List<RequiredUserFiles> ListRequiredUserFiles) {
        this.ListRequiredUserFiles = ListRequiredUserFiles;
    }
  
    public Users() {
    }

    public Users(Integer inId) {
        this.inId = inId;
    }

    public Integer getInId() {
        return inId;
    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getDateOfInitialRegistration() {
        return dateOfInitialRegistration;
    }

    public void setDateOfInitialRegistration(Date dateOfInitialRegistration) {
        this.dateOfInitialRegistration = dateOfInitialRegistration;
    }

    public Integer getNumberOfTries() {
        return numberOfTries;
    }

    public void setNumberOfTries(Integer numberOfTries) {
        this.numberOfTries = numberOfTries;
    }

    public Date getLastTrialDate() {
        return lastTrialDate;
    }

    public void setLastTrialDate(Date lastTrialDate) {
        this.lastTrialDate = lastTrialDate;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getDateCreatedPassword() {
        return dateCreatedPassword;
    }

    public void setDateCreatedPassword(Date dateCreatedPassword) {
        this.dateCreatedPassword = dateCreatedPassword;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public Date getDateCreatedPinCode() {
        return dateCreatedPinCode;
    }

    public void setDateCreatedPinCode(Date dateCreatedPinCode) {
        this.dateCreatedPinCode = dateCreatedPinCode;
    }

    public boolean getTemporary() {
        return temporary;
    }

    public void setTemporary(boolean temporary) {
        this.temporary = temporary;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inId != null ? inId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Users)) {
            return false;
        }
        Users other = (Users) object;
        if ((this.inId == null && other.inId != null) || (this.inId != null && !this.inId.equals(other.inId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.Users[ inId=" + inId + " ]";
    }

}
