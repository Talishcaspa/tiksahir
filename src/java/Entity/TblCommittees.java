/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rachel
 */
@Entity
@Table(name = "app_rachel.tbl_committees")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblCommittees.findAll", query = "SELECT t FROM TblCommittees t")
    , @NamedQuery(name = "TblCommittees.findByInId", query = "SELECT t FROM TblCommittees t WHERE t.inId = :inId")
    , @NamedQuery(name = "TblCommittees.findByNameCommittees", query = "SELECT t FROM TblCommittees t WHERE t.nameCommittees = :nameCommittees")})
public class TblCommittees implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "in_id")
    private Integer inId;
    @Size(max = 45)
    @Column(name = "name_committees")
    private String nameCommittees;

    public TblCommittees() {
    }

    public TblCommittees(Integer inId) {
        this.inId = inId;
    }

    public Integer getInId() {
        return inId;
    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }

    public String getNameCommittees() {
        return nameCommittees;
    }

    public void setNameCommittees(String nameCommittees) {
        this.nameCommittees = nameCommittees;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inId != null ? inId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblCommittees)) {
            return false;
        }
        TblCommittees other = (TblCommittees) object;
        if ((this.inId == null && other.inId != null) || (this.inId != null && !this.inId.equals(other.inId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Attendance.TblCommittees[ inId=" + inId + " ]";
    }
    
}
