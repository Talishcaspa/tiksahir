/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rachel
 */
@Entity
@Table(name = "app_rachel.activity_documentation")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ActivityDocumentation.findAll", query = "SELECT a FROM ActivityDocumentation a")
    , @NamedQuery(name = "ActivityDocumentation.findByInId", query = "SELECT a FROM ActivityDocumentation a WHERE a.inId = :inId")
//    , @NamedQuery(name = "ActivityDocumentation.findByInIdUser", query = "SELECT a FROM ActivityDocumentation a WHERE a.inIdUser = :inIdUser")
    , @NamedQuery(name = "ActivityDocumentation.findByDateOfActivity", query = "SELECT a FROM ActivityDocumentation a WHERE a.dateOfActivity = :dateOfActivity")
    , @NamedQuery(name = "ActivityDocumentation.findByTypeOfActivity", query = "SELECT a FROM ActivityDocumentation a WHERE a.typeOfActivity = :typeOfActivity")})
public class ActivityDocumentation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "in_id")
    private Integer inId;
//    @Column(name = "in_id_user")
//    private Integer inIdUser;
    @Column(name = "date_of_activity")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfActivity;
    @Column(name = "type_of_activity")
    private Integer typeOfActivity;
    @Column(name = "file_name")
    private String fileName;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    @JoinColumn(name = "in_id_user", referencedColumnName = "in_id")
    private Users Users;

    public ActivityDocumentation(int typeOfActivity, String nameFile, Users user) {

        this.dateOfActivity = new Date();
        this.typeOfActivity = typeOfActivity;
        this.fileName = nameFile;  
        this.Users=user;

    }

    public ActivityDocumentation(int type, Users user) {
        this.dateOfActivity = new Date();
        this.typeOfActivity = type;
        this.Users=user;
    }

    public Users getUsers() {
        return Users;
    }

    public void setUsers(Users Users) {
        this.Users = Users;
    }

    public ActivityDocumentation() {
    }

    public ActivityDocumentation(Integer inId) {
        this.inId = inId;
    }

    public Integer getInId() {
        return inId;
    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }

//    public Integer getInIdUser() {
//        return inIdUser;
//    }
//
//    public void setInIdUser(Integer inIdUser) {
//        this.inIdUser = inIdUser;
//    }

    public Date getDateOfActivity() {
        return dateOfActivity;
    }

    public void setDateOfActivity(Date dateOfActivity) {
        this.dateOfActivity = dateOfActivity;
    }

    public Integer getTypeOfActivity() {
        return typeOfActivity;
    }

    public void setTypeOfActivity(Integer typeOfActivity) {
        this.typeOfActivity = typeOfActivity;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inId != null ? inId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ActivityDocumentation)) {
            return false;
        }
        ActivityDocumentation other = (ActivityDocumentation) object;
        if ((this.inId == null && other.inId != null) || (this.inId != null && !this.inId.equals(other.inId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.ActivityDocumentation[ inId=" + inId + " ]";
    }

}
