/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rachel
 */
@Entity
@Table(name = "app_rachel.required_user_files")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RequiredUserFiles.findAll", query = "SELECT r FROM RequiredUserFiles r")
    , @NamedQuery(name = "RequiredUserFiles.findByInId", query = "SELECT r FROM RequiredUserFiles r WHERE r.inId = :inId")
//    , @NamedQuery(name = "RequiredUserFiles.findByInIdUser", query = "SELECT r FROM RequiredUserFiles r WHERE r.inIdUser = :inIdUser")
    , @NamedQuery(name = "RequiredUserFiles.findByTypeFile", query = "SELECT r FROM RequiredUserFiles r WHERE r.typeFile = :typeFile")
    , @NamedQuery(name = "RequiredUserFiles.findByExiteFile", query = "SELECT r FROM RequiredUserFiles r WHERE r.existFile = :existFile")
    , @NamedQuery(name = "RequiredUserFiles.findByUrlFile", query = "SELECT r FROM RequiredUserFiles r WHERE r.urlFile = :urlFile")
    , @NamedQuery(name = "RequiredUserFiles.findByDate", query = "SELECT r FROM RequiredUserFiles r WHERE r.date = :date")})
public class RequiredUserFiles implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "in_id")
    private Integer inId;
//    @Column(name = "in_id_user")
//    private Integer inIdUser;
    @Column(name = "type_file")
    private Integer typeFile;
    @Column(name = "exist_file")
    private Boolean existFile;
    @Size(max = 100)
    @Column(name = "url_file")
    private String urlFile;
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    @JoinColumn(name = "in_id_user", referencedColumnName = "in_id")
    private Users Users;       

    public Users getUsers() {
        return Users;
    }

    public void setUsers(Users Users) {
        this.Users = Users;
    }
    

    public RequiredUserFiles() {
    }

    public RequiredUserFiles(Integer typeFile,  String urlFile, Users Users) {
        this.typeFile = typeFile;
        existFile = Boolean.TRUE;
        this.urlFile = urlFile;
        date = new Date();
        this.Users = Users;
    }

    public RequiredUserFiles(Integer inId) {
        this.inId = inId;
    }

    public Integer getInId() {
        return inId;
    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }

//    public Integer getInIdUser() {
//        return inIdUser;
//    }
//
//    public void setInIdUser(Integer inIdUser) {
//        this.inIdUser = inIdUser;
//    }

    public Integer getTypeFile() {
        return typeFile;
    }

    public void setTypeFile(Integer typeFile) {
        this.typeFile = typeFile;
    }

    public Boolean getExistFile() {
        return existFile;
    }

    public void setExistFile(Boolean existFile) {
        this.existFile = existFile;
    }

    public String getUrlFile() {
        return urlFile;
    }

    public void setUrlFile(String urlFile) {
        this.urlFile = urlFile;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inId != null ? inId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RequiredUserFiles)) {
            return false;
        }
        RequiredUserFiles other = (RequiredUserFiles) object;
        if ((this.inId == null && other.inId != null) || (this.inId != null && !this.inId.equals(other.inId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.RequiredUserFiles[ inId=" + inId + " ]";
    }
    
}
