/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rachel
 */
@Entity
@Table(name = "worker_diff_taxes_reports.formList")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FormList.findAll", query = "SELECT f FROM FormList f")
    , @NamedQuery(name = "FormList.findByInId", query = "SELECT f FROM FormList f WHERE f.inId = :inId")
    , @NamedQuery(name = "FormList.findByFormName", query = "SELECT f FROM FormList f WHERE f.formName = :formName")
    , @NamedQuery(name = "FormList.findByIdWorker", query = "SELECT f FROM FormList f WHERE f.idWorker = :idWorker")
    , @NamedQuery(name = "FormList.findByExist", query = "SELECT f FROM FormList f WHERE f.exist = :exist")
    , @NamedQuery(name = "FormList.findBySymbol", query = "SELECT f FROM FormList f WHERE f.symbol = :symbol")
    , @NamedQuery(name = "FormList.findByUsername", query = "SELECT f FROM FormList f WHERE f.username = :username")
    , @NamedQuery(name = "FormList.findByIsDisplayApp", query = "SELECT f FROM FormList f WHERE f.isDisplayApp = :isDisplayApp")
    , @NamedQuery(name = "FormList.findByType", query = "SELECT f FROM FormList f WHERE f.FormMenu.key  = :type")   
//    , @NamedQuery(name = "FormList.findByTypeIsDisplayAppTrue", query = "SELECT f FROM FormList f WHERE f.type = :type AND f.isDisplayApp = TRUE ")
    , @NamedQuery(name = "FormList.findByTypeIsDisplayAppTrue", query = "SELECT f FROM FormList f WHERE f.FormMenu.key = :type AND f.isDisplayApp = TRUE ")
    , @NamedQuery(name = "FormMenu.findByIcon", query = "SELECT f FROM FormMenu f WHERE f.icon = :icon")
    , @NamedQuery(name = "FormList.findByPaymentRequired", query = "SELECT f FROM FormList f WHERE f.paymentRequired = :paymentRequired")
    , @NamedQuery(name = "FormList.findByPossibilityToUploadFiles", query = "SELECT f FROM FormList f WHERE f.possibilityToUploadFiles = :possibilityToUploadFiles")
})
public class FormList implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "in_id")
    private Integer inId;
    @Size(max = 200)
    @Column(name = "formName")
    private String formName;
    @Size(max = 45)
    @Column(name = "id_worker")
    private String idWorker;
    @Size(max = 45)
    @Column(name = "exist")
    private String exist;
    @Size(max = 45)
    @Column(name = "symbol")
    private String symbol;
    @Size(max = 45)
    @Column(name = "username")
    private String username;
    @Column(name = "is_display_app")
    private Short isDisplayApp;
      @Column(name = "paymentRequired")
    private Boolean paymentRequired;
    @Column(name = "possibilityToUploadFiles")
    private Boolean possibilityToUploadFiles;
//    @Size(max = 45)
//    @Column(name = "type")
//    private Integer type;
    @Column(name = "icon")
    private String icon;
    
//    @JoinColumn(name = "type", referencedColumnName = "`key`" )
//    @ManyToOne(cascade={CascadeType.PERSIST, CascadeType.REMOVE})
//    private FormMenu type;
    
    @ManyToOne(cascade={CascadeType.PERSIST, CascadeType.REMOVE})
    @JoinColumn(name = "type", referencedColumnName = "`key`")
    private FormMenu FormMenu;

    @OneToMany(mappedBy = "FormList",cascade={CascadeType.PERSIST, CascadeType.REMOVE})
    private List<UploadingFilesShirim> ListUploadingFilesShirim;

    public List<UploadingFilesShirim> getListUploadingFilesShirim() {
        return ListUploadingFilesShirim;
    }

    public void setListUploadingFilesShirim(List<UploadingFilesShirim> ListUploadingFilesShirim) {
        this.ListUploadingFilesShirim = ListUploadingFilesShirim;
    }


    public FormMenu getFormMenu() {
        return FormMenu;
    }

    public void setFormMenu(FormMenu FormMenu) {
        this.FormMenu = FormMenu;
    }

    public FormList() {
    }

    public FormList(Integer inId) {
        this.inId = inId;
    }

    public Integer getInId() {
        return inId;
    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }

    public String getFormName() {
        return formName;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }

    public String getIdWorker() {
        return idWorker;
    }

    public void setIdWorker(String idWorker) {
        this.idWorker = idWorker;
    }

    public String getExist() {
        return exist;
    }

    public void setExist(String exist) {
        this.exist = exist;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Short getIsDisplayApp() {
        return isDisplayApp;
    }

    public void setIsDisplayApp(Short isDisplayApp) {
        this.isDisplayApp = isDisplayApp;
    }

//    public Integer getType() {
//        return type;
//    }
//
//    public void setType(Integer type) {
//        this.type = type;
//    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
    
        public Boolean getPaymentRequired() {
        return paymentRequired;
    }

    public void setPaymentRequired(Boolean paymentRequired) {
        this.paymentRequired = paymentRequired;
    }

    public Boolean getPossibilityToUploadFiles() {
        return possibilityToUploadFiles;
    }

    public void setPossibilityToUploadFiles(Boolean possibilityToUploadFiles) {
        this.possibilityToUploadFiles = possibilityToUploadFiles;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inId != null ? inId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FormList)) {
            return false;
        }
        FormList other = (FormList) object;
        if ((this.inId == null && other.inId != null) || (this.inId != null && !this.inId.equals(other.inId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
//        return  "in_id: "+inId" formName:"+formName;
        return "Entity.FormList[ inId=" + inId + " ]";

    }
    
}
