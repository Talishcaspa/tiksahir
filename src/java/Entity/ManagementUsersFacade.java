/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author rachel
 */
@Stateless
public class ManagementUsersFacade extends AbstractFacade<ManagementUsers> {

    @PersistenceContext(unitName = "AppTikSachirWsPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ManagementUsersFacade() {
        super(ManagementUsers.class);
    }

    public ManagementUsers getUserDetails(String userName, String userPass) {

        List<ManagementUsers> managementUsersList = em.createNamedQuery("ManagementUsers.findByUser").setParameter("userName", userName).getResultList();

        return check(managementUsersList, userPass);
    }

    private ManagementUsers check(List<ManagementUsers> managementUsersList, String userPass) {

        ManagementUsers user = new ManagementUsers();
        user.setInId(-1);
        if (managementUsersList.size() > 0) {
            try {

                user = managementUsersList.get(0);

                String md5 = null;

                MessageDigest md = MessageDigest.getInstance("MD5");

                md.update(userPass.getBytes());

                byte[] digest = md.digest();

                md5 = new BigInteger(1, digest).toString(16);

                if (!md5.equals(user.getUserPass())) {
                    ManagementUsers user1 = new ManagementUsers();
                    user1.setInId(-1);

                    return user1;
                }
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(ManagementUsersFacade.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        return user;
    }

}
