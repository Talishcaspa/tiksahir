/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rachel
 */
@Entity
@Table(name = "contactApi.copy_to_csv")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CopyToCsv.findAll", query = "SELECT c FROM CopyToCsv c")
    , @NamedQuery(name = "CopyToCsv.findByInId", query = "SELECT c FROM CopyToCsv c WHERE c.inId = :inId")
    , @NamedQuery(name = "CopyToCsv.findBySource", query = "SELECT c FROM CopyToCsv c WHERE c.source = :source")
    , @NamedQuery(name = "CopyToCsv.findByName", query = "SELECT c FROM CopyToCsv c WHERE c.name = :name")
    , @NamedQuery(name = "CopyToCsv.findByGivenName", query = "SELECT c FROM CopyToCsv c WHERE c.givenName = :givenName")
    , @NamedQuery(name = "CopyToCsv.findByAdditionalName", query = "SELECT c FROM CopyToCsv c WHERE c.additionalName = :additionalName")
    , @NamedQuery(name = "CopyToCsv.findByFamilyName", query = "SELECT c FROM CopyToCsv c WHERE c.familyName = :familyName")
    , @NamedQuery(name = "CopyToCsv.findByYomiName", query = "SELECT c FROM CopyToCsv c WHERE c.yomiName = :yomiName")
    , @NamedQuery(name = "CopyToCsv.findByGivenNameYomi", query = "SELECT c FROM CopyToCsv c WHERE c.givenNameYomi = :givenNameYomi")
    , @NamedQuery(name = "CopyToCsv.findByAdditionalNameYomi", query = "SELECT c FROM CopyToCsv c WHERE c.additionalNameYomi = :additionalNameYomi")
    , @NamedQuery(name = "CopyToCsv.findByFamilyNameYomi", query = "SELECT c FROM CopyToCsv c WHERE c.familyNameYomi = :familyNameYomi")
    , @NamedQuery(name = "CopyToCsv.findByNamePrefix", query = "SELECT c FROM CopyToCsv c WHERE c.namePrefix = :namePrefix")
    , @NamedQuery(name = "CopyToCsv.findByNameSuffix", query = "SELECT c FROM CopyToCsv c WHERE c.nameSuffix = :nameSuffix")
    , @NamedQuery(name = "CopyToCsv.findByInitials", query = "SELECT c FROM CopyToCsv c WHERE c.initials = :initials")
    , @NamedQuery(name = "CopyToCsv.findByNickname", query = "SELECT c FROM CopyToCsv c WHERE c.nickname = :nickname")
    , @NamedQuery(name = "CopyToCsv.findByShortName", query = "SELECT c FROM CopyToCsv c WHERE c.shortName = :shortName")
    , @NamedQuery(name = "CopyToCsv.findByMaidenName", query = "SELECT c FROM CopyToCsv c WHERE c.maidenName = :maidenName")
    , @NamedQuery(name = "CopyToCsv.findByBirthday", query = "SELECT c FROM CopyToCsv c WHERE c.birthday = :birthday")
    , @NamedQuery(name = "CopyToCsv.findByGender", query = "SELECT c FROM CopyToCsv c WHERE c.gender = :gender")
    , @NamedQuery(name = "CopyToCsv.findByLocation", query = "SELECT c FROM CopyToCsv c WHERE c.location = :location")
    , @NamedQuery(name = "CopyToCsv.findByBillingInformation", query = "SELECT c FROM CopyToCsv c WHERE c.billingInformation = :billingInformation")
    , @NamedQuery(name = "CopyToCsv.findByDirectoryServer", query = "SELECT c FROM CopyToCsv c WHERE c.directoryServer = :directoryServer")
    , @NamedQuery(name = "CopyToCsv.findByMileage", query = "SELECT c FROM CopyToCsv c WHERE c.mileage = :mileage")
    , @NamedQuery(name = "CopyToCsv.findByOccupation", query = "SELECT c FROM CopyToCsv c WHERE c.occupation = :occupation")
    , @NamedQuery(name = "CopyToCsv.findByHobby", query = "SELECT c FROM CopyToCsv c WHERE c.hobby = :hobby")
    , @NamedQuery(name = "CopyToCsv.findBySensitivity", query = "SELECT c FROM CopyToCsv c WHERE c.sensitivity = :sensitivity")
    , @NamedQuery(name = "CopyToCsv.findByPriority", query = "SELECT c FROM CopyToCsv c WHERE c.priority = :priority")
    , @NamedQuery(name = "CopyToCsv.findBySubject", query = "SELECT c FROM CopyToCsv c WHERE c.subject = :subject")
    , @NamedQuery(name = "CopyToCsv.findByNotes", query = "SELECT c FROM CopyToCsv c WHERE c.notes = :notes")
    , @NamedQuery(name = "CopyToCsv.findByGroupMembership", query = "SELECT c FROM CopyToCsv c WHERE c.groupMembership = :groupMembership")
    , @NamedQuery(name = "CopyToCsv.findByEmail1Type", query = "SELECT c FROM CopyToCsv c WHERE c.email1Type = :email1Type")
    , @NamedQuery(name = "CopyToCsv.findByEmail1Value", query = "SELECT c FROM CopyToCsv c WHERE c.email1Value = :email1Value")
    , @NamedQuery(name = "CopyToCsv.findByEmail2Type", query = "SELECT c FROM CopyToCsv c WHERE c.email2Type = :email2Type")
    , @NamedQuery(name = "CopyToCsv.findByEmail2Value", query = "SELECT c FROM CopyToCsv c WHERE c.email2Value = :email2Value")
    , @NamedQuery(name = "CopyToCsv.findByEmail3Type", query = "SELECT c FROM CopyToCsv c WHERE c.email3Type = :email3Type")
    , @NamedQuery(name = "CopyToCsv.findByEmail3Value", query = "SELECT c FROM CopyToCsv c WHERE c.email3Value = :email3Value")
    , @NamedQuery(name = "CopyToCsv.findByEmail4Type", query = "SELECT c FROM CopyToCsv c WHERE c.email4Type = :email4Type")
    , @NamedQuery(name = "CopyToCsv.findByEmail4Value", query = "SELECT c FROM CopyToCsv c WHERE c.email4Value = :email4Value")
    , @NamedQuery(name = "CopyToCsv.findByPhone1Type", query = "SELECT c FROM CopyToCsv c WHERE c.phone1Type = :phone1Type")
    , @NamedQuery(name = "CopyToCsv.findByPhone1Value", query = "SELECT c FROM CopyToCsv c WHERE c.phone1Value = :phone1Value")
    , @NamedQuery(name = "CopyToCsv.findByPhone2Type", query = "SELECT c FROM CopyToCsv c WHERE c.phone2Type = :phone2Type")
    , @NamedQuery(name = "CopyToCsv.findByPhone2Value", query = "SELECT c FROM CopyToCsv c WHERE c.phone2Value = :phone2Value")
    , @NamedQuery(name = "CopyToCsv.findByPhone3Type", query = "SELECT c FROM CopyToCsv c WHERE c.phone3Type = :phone3Type")
    , @NamedQuery(name = "CopyToCsv.findByPhone3Value", query = "SELECT c FROM CopyToCsv c WHERE c.phone3Value = :phone3Value")
    , @NamedQuery(name = "CopyToCsv.findByPhone4Type", query = "SELECT c FROM CopyToCsv c WHERE c.phone4Type = :phone4Type")
    , @NamedQuery(name = "CopyToCsv.findByPhone4Value", query = "SELECT c FROM CopyToCsv c WHERE c.phone4Value = :phone4Value")
    , @NamedQuery(name = "CopyToCsv.findByOrganization1Type", query = "SELECT c FROM CopyToCsv c WHERE c.organization1Type = :organization1Type")
    , @NamedQuery(name = "CopyToCsv.findByOrganization1Name", query = "SELECT c FROM CopyToCsv c WHERE c.organization1Name = :organization1Name")
    , @NamedQuery(name = "CopyToCsv.findByOrganization1YomiName", query = "SELECT c FROM CopyToCsv c WHERE c.organization1YomiName = :organization1YomiName")
    , @NamedQuery(name = "CopyToCsv.findByOrganization1Title", query = "SELECT c FROM CopyToCsv c WHERE c.organization1Title = :organization1Title")
    , @NamedQuery(name = "CopyToCsv.findByOrganization1Department", query = "SELECT c FROM CopyToCsv c WHERE c.organization1Department = :organization1Department")
    , @NamedQuery(name = "CopyToCsv.findByOrganization1Symbol", query = "SELECT c FROM CopyToCsv c WHERE c.organization1Symbol = :organization1Symbol")
    , @NamedQuery(name = "CopyToCsv.findByOrganization1Location", query = "SELECT c FROM CopyToCsv c WHERE c.organization1Location = :organization1Location")
    , @NamedQuery(name = "CopyToCsv.findByOrganization1JobDescription", query = "SELECT c FROM CopyToCsv c WHERE c.organization1JobDescription = :organization1JobDescription")
    , @NamedQuery(name = "CopyToCsv.findByCustomField1Type", query = "SELECT c FROM CopyToCsv c WHERE c.customField1Type = :customField1Type")
    , @NamedQuery(name = "CopyToCsv.findByCustomField1Value", query = "SELECT c FROM CopyToCsv c WHERE c.customField1Value = :customField1Value")
    , @NamedQuery(name = "CopyToCsv.findByCustomField2Type", query = "SELECT c FROM CopyToCsv c WHERE c.customField2Type = :customField2Type")
    , @NamedQuery(name = "CopyToCsv.findByCustomField2Value", query = "SELECT c FROM CopyToCsv c WHERE c.customField2Value = :customField2Value")
    , @NamedQuery(name = "CopyToCsv.findByCustomField3Type", query = "SELECT c FROM CopyToCsv c WHERE c.customField3Type = :customField3Type")
    , @NamedQuery(name = "CopyToCsv.findByCustomField3Value", query = "SELECT c FROM CopyToCsv c WHERE c.customField3Value = :customField3Value")
    , @NamedQuery(name = "CopyToCsv.findByCustomField4Type", query = "SELECT c FROM CopyToCsv c WHERE c.customField4Type = :customField4Type")
    , @NamedQuery(name = "CopyToCsv.findByCustomField4Value", query = "SELECT c FROM CopyToCsv c WHERE c.customField4Value = :customField4Value")
    , @NamedQuery(name = "CopyToCsv.findByCustomField5Type", query = "SELECT c FROM CopyToCsv c WHERE c.customField5Type = :customField5Type")
    , @NamedQuery(name = "CopyToCsv.findByCustomField5Value", query = "SELECT c FROM CopyToCsv c WHERE c.customField5Value = :customField5Value")
    , @NamedQuery(name = "CopyToCsv.findByCustomField6Type", query = "SELECT c FROM CopyToCsv c WHERE c.customField6Type = :customField6Type")
    , @NamedQuery(name = "CopyToCsv.findByCustomField6Value", query = "SELECT c FROM CopyToCsv c WHERE c.customField6Value = :customField6Value")
    , @NamedQuery(name = "CopyToCsv.findByCustomField7Type", query = "SELECT c FROM CopyToCsv c WHERE c.customField7Type = :customField7Type")
    , @NamedQuery(name = "CopyToCsv.findByCustomField7Value", query = "SELECT c FROM CopyToCsv c WHERE c.customField7Value = :customField7Value")})
public class CopyToCsv implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "in_id")
    private Integer inId;
    @Size(max = 45)
    @Column(name = "source")
    private String source;
    @Size(max = 45)
    @Column(name = "name")
    private String name;
    @Size(max = 45)
    @Column(name = "Given_Name")
    private String givenName;
    @Size(max = 45)
    @Column(name = "Additional_Name")
    private String additionalName;
    @Size(max = 45)
    @Column(name = "Family_Name")
    private String familyName;
    @Size(max = 45)
    @Column(name = "Yomi_Name")
    private String yomiName;
    @Size(max = 45)
    @Column(name = "Given_Name_Yomi")
    private String givenNameYomi;
    @Size(max = 45)
    @Column(name = "Additional_Name_Yomi")
    private String additionalNameYomi;
    @Size(max = 45)
    @Column(name = "Family_Name_Yomi")
    private String familyNameYomi;
    @Size(max = 45)
    @Column(name = "Name_Prefix")
    private String namePrefix;
    @Size(max = 45)
    @Column(name = "Name_Suffix")
    private String nameSuffix;
    @Size(max = 45)
    @Column(name = "Initials")
    private String initials;
    @Size(max = 45)
    @Column(name = "Nickname")
    private String nickname;
    @Size(max = 45)
    @Column(name = "Short_Name")
    private String shortName;
    @Size(max = 45)
    @Column(name = "Maiden_Name")
    private String maidenName;
    @Size(max = 45)
    @Column(name = "Birthday")
    private String birthday;
    @Size(max = 45)
    @Column(name = "Gender")
    private String gender;
    @Size(max = 45)
    @Column(name = "Location")
    private String location;
    @Size(max = 45)
    @Column(name = "Billing_Information")
    private String billingInformation;
    @Size(max = 45)
    @Column(name = "Directory_Server")
    private String directoryServer;
    @Size(max = 45)
    @Column(name = "Mileage")
    private String mileage;
    @Size(max = 45)
    @Column(name = "Occupation")
    private String occupation;
    @Size(max = 45)
    @Column(name = "Hobby")
    private String hobby;
    @Size(max = 45)
    @Column(name = "Sensitivity")
    private String sensitivity;
    @Size(max = 45)
    @Column(name = "Priority")
    private String priority;
    @Size(max = 45)
    @Column(name = "Subject")
    private String subject;
    @Size(max = 45)
    @Column(name = "Notes")
    private String notes;
    @Size(max = 45)
    @Column(name = "Group_Membership")
    private String groupMembership;
    @Size(max = 45)
    @Column(name = "E_mail_1_Type")
    private String email1Type;
    @Size(max = 45)
    @Column(name = "E_mail_1_Value")
    private String email1Value;
    @Size(max = 45)
    @Column(name = "E_mail_2_Type")
    private String email2Type;
    @Size(max = 45)
    @Column(name = "E_mail_2_Value")
    private String email2Value;
    @Size(max = 45)
    @Column(name = "E_mail_3_Type")
    private String email3Type;
    @Size(max = 45)
    @Column(name = "E_mail_3_Value")
    private String email3Value;
    @Size(max = 45)
    @Column(name = "E_mail_4_Type")
    private String email4Type;
    @Size(max = 45)
    @Column(name = "E_mail_4_Value")
    private String email4Value;
    @Size(max = 45)
    @Column(name = "Phone_1_Type")
    private String phone1Type;
    @Size(max = 45)
    @Column(name = "Phone_1_Value")
    private String phone1Value;
    @Size(max = 45)
    @Column(name = "Phone_2_Type")
    private String phone2Type;
    @Size(max = 45)
    @Column(name = "Phone_2_Value")
    private String phone2Value;
    @Size(max = 45)
    @Column(name = "Phone_3_Type")
    private String phone3Type;
    @Size(max = 45)
    @Column(name = "Phone_3_Value")
    private String phone3Value;
    @Size(max = 45)
    @Column(name = "Phone_4_Type")
    private String phone4Type;
    @Size(max = 45)
    @Column(name = "Phone_4_Value")
    private String phone4Value;
    @Size(max = 45)
    @Column(name = "Organization_1_Type")
    private String organization1Type;
    @Size(max = 45)
    @Column(name = "Organization_1_Name")
    private String organization1Name;
    @Size(max = 45)
    @Column(name = "Organization_1_Yomi_Name")
    private String organization1YomiName;
    @Size(max = 45)
    @Column(name = "Organization_1_Title")
    private String organization1Title;
    @Size(max = 45)
    @Column(name = "Organization_1_Department")
    private String organization1Department;
    @Size(max = 45)
    @Column(name = "Organization_1_Symbol")
    private String organization1Symbol;
    @Size(max = 45)
    @Column(name = "Organization_1_Location")
    private String organization1Location;
    @Size(max = 45)
    @Column(name = "Organization_1_Job_Description")
    private String organization1JobDescription;
    @Size(max = 45)
    @Column(name = "Custom_Field_1_Type")
    private String customField1Type;
    @Size(max = 45)
    @Column(name = "Custom_Field_1_Value")
    private String customField1Value;
    @Size(max = 45)
    @Column(name = "Custom_Field_2_Type")
    private String customField2Type;
    @Size(max = 45)
    @Column(name = "Custom_Field_2_Value")
    private String customField2Value;
    @Size(max = 45)
    @Column(name = "Custom_Field_3_Type")
    private String customField3Type;
    @Size(max = 45)
    @Column(name = "Custom_Field_3_Value")
    private String customField3Value;
    @Size(max = 45)
    @Column(name = "Custom_Field_4_Type")
    private String customField4Type;
    @Size(max = 45)
    @Column(name = "Custom_Field_4_Value")
    private String customField4Value;
    @Size(max = 45)
    @Column(name = "Custom_Field_5_Type")
    private String customField5Type;
    @Size(max = 45)
    @Column(name = "Custom_Field_5_Value")
    private String customField5Value;
    @Size(max = 45)
    @Column(name = "Custom_Field_6_Type")
    private String customField6Type;
    @Size(max = 45)
    @Column(name = "Custom_Field_6_Value")
    private String customField6Value;
    @Size(max = 45)
    @Column(name = "Custom_Field_7_Type")
    private String customField7Type;
    @Size(max = 45)
    @Column(name = "Custom_Field_7_Value")
    private String customField7Value;

    public CopyToCsv() {
    }

    public CopyToCsv(Integer inId) {
        this.inId = inId;
    }

    public Integer getInId() {
        return inId;
    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getAdditionalName() {
        return additionalName;
    }

    public void setAdditionalName(String additionalName) {
        this.additionalName = additionalName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getYomiName() {
        return yomiName;
    }

    public void setYomiName(String yomiName) {
        this.yomiName = yomiName;
    }

    public String getGivenNameYomi() {
        return givenNameYomi;
    }

    public void setGivenNameYomi(String givenNameYomi) {
        this.givenNameYomi = givenNameYomi;
    }

    public String getAdditionalNameYomi() {
        return additionalNameYomi;
    }

    public void setAdditionalNameYomi(String additionalNameYomi) {
        this.additionalNameYomi = additionalNameYomi;
    }

    public String getFamilyNameYomi() {
        return familyNameYomi;
    }

    public void setFamilyNameYomi(String familyNameYomi) {
        this.familyNameYomi = familyNameYomi;
    }

    public String getNamePrefix() {
        return namePrefix;
    }

    public void setNamePrefix(String namePrefix) {
        this.namePrefix = namePrefix;
    }

    public String getNameSuffix() {
        return nameSuffix;
    }

    public void setNameSuffix(String nameSuffix) {
        this.nameSuffix = nameSuffix;
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getMaidenName() {
        return maidenName;
    }

    public void setMaidenName(String maidenName) {
        this.maidenName = maidenName;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getBillingInformation() {
        return billingInformation;
    }

    public void setBillingInformation(String billingInformation) {
        this.billingInformation = billingInformation;
    }

    public String getDirectoryServer() {
        return directoryServer;
    }

    public void setDirectoryServer(String directoryServer) {
        this.directoryServer = directoryServer;
    }

    public String getMileage() {
        return mileage;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    public String getSensitivity() {
        return sensitivity;
    }

    public void setSensitivity(String sensitivity) {
        this.sensitivity = sensitivity;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getGroupMembership() {
        return groupMembership;
    }

    public void setGroupMembership(String groupMembership) {
        this.groupMembership = groupMembership;
    }

    public String getEmail1Type() {
        return email1Type;
    }

    public void setEmail1Type(String email1Type) {
        this.email1Type = email1Type;
    }

    public String getEmail1Value() {
        return email1Value;
    }

    public void setEmail1Value(String email1Value) {
        this.email1Value = email1Value;
    }

    public String getEmail2Type() {
        return email2Type;
    }

    public void setEmail2Type(String email2Type) {
        this.email2Type = email2Type;
    }

    public String getEmail2Value() {
        return email2Value;
    }

    public void setEmail2Value(String email2Value) {
        this.email2Value = email2Value;
    }

    public String getEmail3Type() {
        return email3Type;
    }

    public void setEmail3Type(String email3Type) {
        this.email3Type = email3Type;
    }

    public String getEmail3Value() {
        return email3Value;
    }

    public void setEmail3Value(String email3Value) {
        this.email3Value = email3Value;
    }

    public String getEmail4Type() {
        return email4Type;
    }

    public void setEmail4Type(String email4Type) {
        this.email4Type = email4Type;
    }

    public String getEmail4Value() {
        return email4Value;
    }

    public void setEmail4Value(String email4Value) {
        this.email4Value = email4Value;
    }

    public String getPhone1Type() {
        return phone1Type;
    }

    public void setPhone1Type(String phone1Type) {
        this.phone1Type = phone1Type;
    }

    public String getPhone1Value() {
        return phone1Value;
    }

    public void setPhone1Value(String phone1Value) {
        this.phone1Value = phone1Value;
    }

    public String getPhone2Type() {
        return phone2Type;
    }

    public void setPhone2Type(String phone2Type) {
        this.phone2Type = phone2Type;
    }

    public String getPhone2Value() {
        return phone2Value;
    }

    public void setPhone2Value(String phone2Value) {
        this.phone2Value = phone2Value;
    }

    public String getPhone3Type() {
        return phone3Type;
    }

    public void setPhone3Type(String phone3Type) {
        this.phone3Type = phone3Type;
    }

    public String getPhone3Value() {
        return phone3Value;
    }

    public void setPhone3Value(String phone3Value) {
        this.phone3Value = phone3Value;
    }

    public String getPhone4Type() {
        return phone4Type;
    }

    public void setPhone4Type(String phone4Type) {
        this.phone4Type = phone4Type;
    }

    public String getPhone4Value() {
        return phone4Value;
    }

    public void setPhone4Value(String phone4Value) {
        this.phone4Value = phone4Value;
    }

    public String getOrganization1Type() {
        return organization1Type;
    }

    public void setOrganization1Type(String organization1Type) {
        this.organization1Type = organization1Type;
    }

    public String getOrganization1Name() {
        return organization1Name;
    }

    public void setOrganization1Name(String organization1Name) {
        this.organization1Name = organization1Name;
    }

    public String getOrganization1YomiName() {
        return organization1YomiName;
    }

    public void setOrganization1YomiName(String organization1YomiName) {
        this.organization1YomiName = organization1YomiName;
    }

    public String getOrganization1Title() {
        return organization1Title;
    }

    public void setOrganization1Title(String organization1Title) {
        this.organization1Title = organization1Title;
    }

    public String getOrganization1Department() {
        return organization1Department;
    }

    public void setOrganization1Department(String organization1Department) {
        this.organization1Department = organization1Department;
    }

    public String getOrganization1Symbol() {
        return organization1Symbol;
    }

    public void setOrganization1Symbol(String organization1Symbol) {
        this.organization1Symbol = organization1Symbol;
    }

    public String getOrganization1Location() {
        return organization1Location;
    }

    public void setOrganization1Location(String organization1Location) {
        this.organization1Location = organization1Location;
    }

    public String getOrganization1JobDescription() {
        return organization1JobDescription;
    }

    public void setOrganization1JobDescription(String organization1JobDescription) {
        this.organization1JobDescription = organization1JobDescription;
    }

    public String getCustomField1Type() {
        return customField1Type;
    }

    public void setCustomField1Type(String customField1Type) {
        this.customField1Type = customField1Type;
    }

    public String getCustomField1Value() {
        return customField1Value;
    }

    public void setCustomField1Value(String customField1Value) {
        this.customField1Value = customField1Value;
    }

    public String getCustomField2Type() {
        return customField2Type;
    }

    public void setCustomField2Type(String customField2Type) {
        this.customField2Type = customField2Type;
    }

    public String getCustomField2Value() {
        return customField2Value;
    }

    public void setCustomField2Value(String customField2Value) {
        this.customField2Value = customField2Value;
    }

    public String getCustomField3Type() {
        return customField3Type;
    }

    public void setCustomField3Type(String customField3Type) {
        this.customField3Type = customField3Type;
    }

    public String getCustomField3Value() {
        return customField3Value;
    }

    public void setCustomField3Value(String customField3Value) {
        this.customField3Value = customField3Value;
    }

    public String getCustomField4Type() {
        return customField4Type;
    }

    public void setCustomField4Type(String customField4Type) {
        this.customField4Type = customField4Type;
    }

    public String getCustomField4Value() {
        return customField4Value;
    }

    public void setCustomField4Value(String customField4Value) {
        this.customField4Value = customField4Value;
    }

    public String getCustomField5Type() {
        return customField5Type;
    }

    public void setCustomField5Type(String customField5Type) {
        this.customField5Type = customField5Type;
    }

    public String getCustomField5Value() {
        return customField5Value;
    }

    public void setCustomField5Value(String customField5Value) {
        this.customField5Value = customField5Value;
    }

    public String getCustomField6Type() {
        return customField6Type;
    }

    public void setCustomField6Type(String customField6Type) {
        this.customField6Type = customField6Type;
    }

    public String getCustomField6Value() {
        return customField6Value;
    }

    public void setCustomField6Value(String customField6Value) {
        this.customField6Value = customField6Value;
    }

    public String getCustomField7Type() {
        return customField7Type;
    }

    public void setCustomField7Type(String customField7Type) {
        this.customField7Type = customField7Type;
    }

    public String getCustomField7Value() {
        return customField7Value;
    }

    public void setCustomField7Value(String customField7Value) {
        this.customField7Value = customField7Value;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inId != null ? inId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CopyToCsv)) {
            return false;
        }
        CopyToCsv other = (CopyToCsv) object;
        if ((this.inId == null && other.inId != null) || (this.inId != null && !this.inId.equals(other.inId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Attendance.CopyToCsv[ inId=" + inId + " ]";
    }
    
}
