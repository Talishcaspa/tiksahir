/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Optional;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rachel
 */
@Entity
@Table(name = "worker_diff_taxes_reports.uploading_files_shirim")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UploadingFilesShirim.findAll", query = "SELECT u FROM UploadingFilesShirim u")
    , @NamedQuery(name = "UploadingFilesShirim.findByInId", query = "SELECT u FROM UploadingFilesShirim u WHERE u.inId = :inId")
    , @NamedQuery(name = "UploadingFilesShirim.findByCompCid", query = "SELECT u FROM UploadingFilesShirim u WHERE u.compCid = :compCid")
    , @NamedQuery(name = "UploadingFilesShirim.findByWorkerId", query = "SELECT u FROM UploadingFilesShirim u WHERE u.workerId = :workerId  ")
    , @NamedQuery(name = "UploadingFilesShirim.findByDay", query = "SELECT u FROM UploadingFilesShirim u WHERE u.day  = :day")
    , @NamedQuery(name = "UploadingFilesShirim.findByMonth", query = "SELECT u FROM UploadingFilesShirim u WHERE u.month = :month")
    , @NamedQuery(name = "UploadingFilesShirim.findByYear", query = "SELECT u FROM UploadingFilesShirim u WHERE u.year = :year")
//    , @NamedQuery(name = "UploadingFilesShirim.findByFileSymbol", query = "SELECT u FROM UploadingFilesShirim u WHERE u.fileSymbol = :fileSymbol")
//    , @NamedQuery(name = "UploadingFilesShirim.findByFileSymbol", query = "SELECT u FROM UploadingFilesShirim u WHERE u.FormList.symbol = :fileSymbol")
    , @NamedQuery(name = "UploadingFilesShirim.findByNote", query = "SELECT u FROM UploadingFilesShirim u WHERE u.note = :note")
    , @NamedQuery(name = "UploadingFilesShirim.findByFileType", query = "SELECT u FROM UploadingFilesShirim u WHERE u.fileType = :fileType")
    , @NamedQuery(name = "UploadingFilesShirim.findByFileName", query = "SELECT u FROM UploadingFilesShirim u WHERE u.fileName = :fileName")
    , @NamedQuery(name = "UploadingFilesShirim.findByFilepath", query = "SELECT u FROM UploadingFilesShirim u WHERE u.filepath = :filepath")
    , @NamedQuery(name = "UploadingFilesShirim.findByUserName", query = "SELECT u FROM UploadingFilesShirim u WHERE u.userName = :userName")
    , @NamedQuery(name = "UploadingFilesShirim.findByDateUpload", query = "SELECT u FROM UploadingFilesShirim u WHERE u.dateUpload = :dateUpload")
    , @NamedQuery(name = "UploadingFilesShirim.findBySource", query = "SELECT u FROM UploadingFilesShirim u WHERE u.source = :source")
    , @NamedQuery(name = "UploadingFilesShirim.findByAmount", query = "SELECT u FROM UploadingFilesShirim u WHERE u.amount = :amount")
    , @NamedQuery(name = "UploadingFilesShirim.findBySumHisahon", query = "SELECT u FROM UploadingFilesShirim u WHERE u.sumHisahon = :sumHisahon")
    , @NamedQuery(name = "UploadingFilesShirim.findByNumberOfPages", query = "SELECT u FROM UploadingFilesShirim u WHERE u.numberOfPages = :numberOfPages")
    , @NamedQuery(name = "UploadingFilesShirim.findByNameRequest", query = "SELECT u FROM UploadingFilesShirim u WHERE u.nameRequest = :nameRequest")
    , @NamedQuery(name = "UploadingFilesShirim.findByReportNumber", query = "SELECT u FROM UploadingFilesShirim u WHERE u.reportNumber = :reportNumber")
    , @NamedQuery(name = "UploadingFilesShirim.findByAppendixNumber", query = "SELECT u FROM UploadingFilesShirim u WHERE u.appendixNumber = :appendixNumber")
    , @NamedQuery(name = "UploadingFilesShirim.findBySendToCustomer", query = "SELECT u FROM UploadingFilesShirim u WHERE u.sendToCustomer = :sendToCustomer")
    , @NamedQuery(name = "UploadingFilesShirim.findByPaid", query = "SELECT u FROM UploadingFilesShirim u WHERE u.paid = :paid")
    , @NamedQuery(name = "UploadingFilesShirim.findByFileSymbolWorkerId", query = "SELECT u FROM UploadingFilesShirim u WHERE u.FormList.symbol = :fileSymbol AND u.workerId = :workerId ")
    , @NamedQuery(name = "UploadingFilesShirim.findByFileSymbolWorkerIdYear", query = "SELECT u FROM UploadingFilesShirim u WHERE u.FormList.symbol = :fileSymbol AND u.workerId = :workerId AND u.year = :year ")
    , @NamedQuery(name = "UploadingFilesShirim.findByWorkerIdPaid", query = "SELECT u FROM UploadingFilesShirim u WHERE u.workerId = :workerId AND u.paid = FALSE")
//    , @NamedQuery(name = "UploadingFilesShirim.findByWorkerIdPaid", query = "SELECT u FROM UploadingFilesShirim u WHERE u.workerId = :workerId AND u.paid = FALSE AND u.FormList.FormMenu.key IN ( 102 ,  103 ) ")
//    , @NamedQuery(name = "UploadingFilesShirim.findByWorkerIdPaid", query = "SELECT u FROM UploadingFilesShirim u WHERE u.workerId = :workerId AND u.paid = FALSE AND u.FormList.FormMenu.key  IN ( 102 ,  103 ) ")
    , @NamedQuery(name = "UploadingFilesShirim.findByLastOpeningDate", query = "SELECT u FROM UploadingFilesShirim u WHERE u.lastOpeningDate = :lastOpeningDate")
    , @NamedQuery(name = "UploadingFilesShirim.findByIsOpened", query = "SELECT u FROM UploadingFilesShirim u WHERE u.isOpened = :isOpened")
    , @NamedQuery(name = "UploadingFilesShirim.findByIsAuthorizedToOpen", query = "SELECT u FROM UploadingFilesShirim u WHERE u.isAuthorizedToOpen = :isAuthorizedToOpen")
})
public class UploadingFilesShirim implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "in_id")
    private Integer inId;
    @Size(max = 45)
    @Column(name = "comp_cid")
    private String compCid;
    @Size(max = 45)
    @Column(name = "comp_name")
    private String compName;
    @Column(name = "worker_id")
    private Integer workerId;
    @Column(name = "day")
    private Integer day;
    @Column(name = "month")
    private Integer month;
    @Column(name = "year")
    private Integer year;
//    @Size(max = 45)
//    @Column(name = "file_symbol")
//    private String fileSymbol;
    @Size(max = 200)
    @Column(name = "note")
    private String note;
    @Size(max = 200)
    @Column(name = "file_type")
    private String fileType;
    @Size(max = 200)
    @Column(name = "fileName")
    private String fileName;
    @Size(max = 200)
    @Column(name = "filepath")
    private String filepath;
    @Size(max = 200)
    @Column(name = "user_name")
    private String userName;
    @Column(name = "date_upload")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpload;
    @Size(max = 45)
    @Column(name = "source")
    private String source;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "amount")
    private Double amount;
    @Column(name = "sum_hisahon")
    private Double sumHisahon;
    @Column(name = "number_of_pages")
    private Integer numberOfPages;
    @Size(max = 200)
    @Column(name = "name_request")
    private String nameRequest;
    @Column(name = "report_number")
    private Integer reportNumber;
    @Column(name = "appendix_number")
    private Integer appendixNumber;
    @Column(name = "send_to_customer")
    private Boolean sendToCustomer;
    @Column(name = "paid")
    private Boolean paid;
    @Column(name = "last_opening_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastOpeningDate;
    @Column(name = "is_opened")
    private Boolean isOpened;
    @Column(name = "is_authorized_to_open")
    private Boolean isAuthorizedToOpen;

//    @ManyToOne(cascade={CascadeType.REMOVE})
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    @JoinColumn(name = "file_symbol", referencedColumnName = "symbol")
    private FormList FormList;

    public FormList getFormList() {
        return FormList;
    }

    public void setFormList(FormList FormList) {
        this.FormList = FormList;
    }

    public UploadingFilesShirim() {
    }

    public UploadingFilesShirim(Integer inId) {
        this.inId = inId;
    }

    public Integer getInId() {
        return inId;
    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }

    public String getCompCid() {
        return compCid;
    }

    public void setCompCid(String compCid) {
        this.compCid = compCid;
    }

    public Integer getWorkerId() {
        return workerId;
    }

    public void setWorkerId(Integer workerId) {
        this.workerId = workerId;
    }

    public Optional<Integer> getDay() {
        return Optional.ofNullable(day);
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

//    public String getFileSymbol() {
//        return fileSymbol;
//    }
//
//    public void setFileSymbol(String fileSymbol) {
//        this.fileSymbol = fileSymbol;
//    }
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getDateUpload() {
        return dateUpload;
    }

    public void setDateUpload(Date dateUpload) {
        this.dateUpload = dateUpload;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Optional<Double> getAmount() {
        return Optional.ofNullable(amount);
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getSumHisahon() {
        return sumHisahon;
    }

    public void setSumHisahon(Double sumHisahon) {
        this.sumHisahon = sumHisahon;
    }

    public Integer getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(Integer numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public String getNameRequest() {
        return nameRequest;
    }

    public void setNameRequest(String nameRequest) {
        this.nameRequest = nameRequest;
    }

    public Integer getReportNumber() {
        return reportNumber;
    }

    public void setReportNumber(Integer reportNumber) {
        this.reportNumber = reportNumber;
    }

    public Integer getAppendixNumber() {
        return appendixNumber;
    }

    public void setAppendixNumber(Integer appendixNumber) {
        this.appendixNumber = appendixNumber;
    }

    public Boolean getSendToCustomer() {
        return sendToCustomer;
    }

    public void setSendToCustomer(Boolean sendToCustomer) {
        this.sendToCustomer = sendToCustomer;
    }

    public Optional<Boolean> getPaid() {
        return Optional.ofNullable(paid);
    }

    public void setPaid(Boolean paid) {
        this.paid = paid;
    }

    public Date getLastOpeningDate() {
        return lastOpeningDate;
    }

    public void setLastOpeningDate(Date lastOpeningDate) {
        this.lastOpeningDate = lastOpeningDate;
    }

    public Boolean getIsOpened() {
        return isOpened;
    }

    public void setIsOpened(Boolean isOpened) {
        this.isOpened = isOpened;
    }

    public Boolean getIsAuthorizedToOpen() {
        return isAuthorizedToOpen;
    }

    public void setIsAuthorizedToOpen(Boolean isAuthorizedToOpen) {
        this.isAuthorizedToOpen = isAuthorizedToOpen;
    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inId != null ? inId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UploadingFilesShirim)) {
            return false;
        }
        UploadingFilesShirim other = (UploadingFilesShirim) object;
        if ((this.inId == null && other.inId != null) || (this.inId != null && !this.inId.equals(other.inId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.UploadingFilesShirim[ inId=" + inId + " ]";
    }

}
