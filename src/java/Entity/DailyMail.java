/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rachel
 */
@Entity
@Table(name = "erp.daily_mail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DailyMail.findAll", query = "SELECT d FROM DailyMail d")
    , @NamedQuery(name = "DailyMail.findByInId", query = "SELECT d FROM DailyMail d WHERE d.inId = :inId")
    , @NamedQuery(name = "DailyMail.findByFromAddress", query = "SELECT d FROM DailyMail d WHERE d.fromAddress = :fromAddress")
    , @NamedQuery(name = "DailyMail.findByToAddress", query = "SELECT d FROM DailyMail d WHERE d.toAddress = :toAddress")
    , @NamedQuery(name = "DailyMail.findByDateInsert", query = "SELECT d FROM DailyMail d WHERE d.dateInsert = :dateInsert")
    , @NamedQuery(name = "DailyMail.findBySubject", query = "SELECT d FROM DailyMail d WHERE d.subject = :subject")
    , @NamedQuery(name = "DailyMail.findByFile", query = "SELECT d FROM DailyMail d WHERE d.file = :file")
    , @NamedQuery(name = "DailyMail.findByStatus", query = "SELECT d FROM DailyMail d WHERE d.status = :status")
    , @NamedQuery(name = "DailyMail.findByDateSend", query = "SELECT d FROM DailyMail d WHERE d.dateSend = :dateSend")})
public class DailyMail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "in_id")
    private Integer inId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "from_address")
    private String fromAddress;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "to_address")
    private String toAddress;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "cc_array")
    private String ccArray;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "bcc_array")
    private String bccArray;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_insert")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateInsert;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "subject")
    private String subject;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 2147483647)
    @Column(name = "message")
    private String message;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "file")
    private String file;
    @Basic(optional = false)
    @NotNull
    @Column(name = "status")
    private int status;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 2147483647)
    @Column(name = "error")
    private String error;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_send")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateSend;

    public DailyMail() {
    }

    public DailyMail(Integer inId) {
        this.inId = inId;
    }

    public DailyMail(Integer inId, String fromAddress, String toAddress, String ccArray, String bccArray, Date dateInsert, String subject, String message, String file, int status, String error, Date dateSend) {
        this.inId = inId;
        this.fromAddress = fromAddress;
        this.toAddress = toAddress;
        this.ccArray = ccArray;
        this.bccArray = bccArray;
        this.dateInsert = dateInsert;
        this.subject = subject;
        this.message = message;
        this.file = file;
        this.status = status;
        this.error = error;
        this.dateSend = dateSend;
    }

    public Integer getInId() {
        return inId;
    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public String getToAddress() {
        return toAddress;
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }

    public String getCcArray() {
        return ccArray;
    }

    public void setCcArray(String ccArray) {
        this.ccArray = ccArray;
    }

    public String getBccArray() {
        return bccArray;
    }

    public void setBccArray(String bccArray) {
        this.bccArray = bccArray;
    }

    public Date getDateInsert() {
        return dateInsert;
    }

    public void setDateInsert(Date dateInsert) {
        this.dateInsert = dateInsert;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Date getDateSend() {
        return dateSend;
    }

    public void setDateSend(Date dateSend) {
        this.dateSend = dateSend;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inId != null ? inId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DailyMail)) {
            return false;
        }
        DailyMail other = (DailyMail) object;
        if ((this.inId == null && other.inId != null) || (this.inId != null && !this.inId.equals(other.inId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.DailyMail[ inId=" + inId + " ]";
    }
    
}
