/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rachel
 */
@Entity
@Table(name = "app_rachel.tbl_payment")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblPaymentNew.findAll", query = "SELECT t FROM TblPaymentNew t")
    , @NamedQuery(name = "TblPaymentNew.findByInId", query = "SELECT t FROM TblPaymentNew t WHERE t.inId = :inId")
    , @NamedQuery(name = "TblPaymentNew.findByCompCid", query = "SELECT t FROM TblPaymentNew t WHERE t.compCid = :compCid")
    , @NamedQuery(name = "TblPaymentNew.findByCompName", query = "SELECT t FROM TblPaymentNew t WHERE t.compName = :compName")
//    , @NamedQuery(name = "TblPaymentNew.findByWorkerId", query = "SELECT t FROM TblPaymentNew t WHERE t.workerId = :workerId")
    , @NamedQuery(name = "TblPaymentNew.findByDay", query = "SELECT t FROM TblPaymentNew t WHERE t.day = :day")
    , @NamedQuery(name = "TblPaymentNew.findByMonth", query = "SELECT t FROM TblPaymentNew t WHERE t.month = :month")
    , @NamedQuery(name = "TblPaymentNew.findByYear", query = "SELECT t FROM TblPaymentNew t WHERE t.year = :year")
    , @NamedQuery(name = "TblPaymentNew.findByFilepath", query = "SELECT t FROM TblPaymentNew t WHERE t.filepath = :filepath")
    , @NamedQuery(name = "TblPaymentNew.findByAmount", query = "SELECT t FROM TblPaymentNew t WHERE t.amount = :amount")
    , @NamedQuery(name = "TblPaymentNew.findByPaid", query = "SELECT t FROM TblPaymentNew t WHERE t.paid = :paid")})
public class TblPaymentNew implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "in_id")
    private Integer inId;
    @Size(max = 45)
    @Column(name = "comp_cid")
    private String compCid;
    @Size(max = 45)
    @Column(name = "comp_name")
    private String compName;
    @Column(name = "worker_id")
    private Integer workerId;
    @Column(name = "day")
    private Integer day;
    @Column(name = "month")
    private Integer month;
    @Column(name = "year")
    private Integer year;
    @Size(max = 200)
    @Column(name = "filepath")
    private String filepath;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "amount")
    private Double amount;
    @Column(name = "paid")
    private Boolean paid;

    public TblPaymentNew() {
    }

    public TblPaymentNew(String compCid, String compName, Integer day, Integer month, Integer year, String filepath, Double amount) {
        this.compCid = compCid;
        this.compName = compName;
        this.day = day;
        this.month = month;
        this.year = year;
        this.filepath = filepath;
        this.amount = amount;
    }

    public TblPaymentNew(Integer inId) {
        this.inId = inId;
    }



    public Integer getInId() {
        return inId;
    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }

    public String getCompCid() {
        return compCid;
    }

    public void setCompCid(String compCid) {
        this.compCid = compCid;
    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public Integer getWorkerId() {
        return workerId;
    }

    public void setWorkerId(Integer workerId) {
        this.workerId = workerId;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Boolean getPaid() {
        return paid;
    }

    public void setPaid(Boolean paid) {
        this.paid = paid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inId != null ? inId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblPaymentNew)) {
            return false;
        }
        TblPaymentNew other = (TblPaymentNew) object;
        if ((this.inId == null && other.inId != null) || (this.inId != null && !this.inId.equals(other.inId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.TblPaymentNew[ inId=" + inId + " ]";
    }
    
}
