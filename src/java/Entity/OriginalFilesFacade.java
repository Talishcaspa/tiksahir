/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author rachel
 */
@Stateless
public class OriginalFilesFacade extends AbstractFacade<OriginalFiles> {

    @PersistenceContext(unitName = "AppTikSachirWsPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public OriginalFilesFacade() {
        super(OriginalFiles.class);
    }
    
     public List<OriginalFiles> findURL(int numFile) {
        return em.createNamedQuery("OriginalFiles.findByNumFile").setParameter("numFile", numFile).getResultList();
    }
    
}
