/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rachel
 */
@Entity
@Table(name = "app_rachel.details_users")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailsUsers.findAll", query = "SELECT d FROM DetailsUsers d")
    , @NamedQuery(name = "DetailsUsers.findByInId", query = "SELECT d FROM DetailsUsers d WHERE d.inId = :inId")
//    , @NamedQuery(name = "DetailsUsers.findByUserInId", query = "SELECT d FROM DetailsUsers d WHERE d.userInId = :userInId")
    , @NamedQuery(name = "DetailsUsers.findByFirstName", query = "SELECT d FROM DetailsUsers d WHERE d.firstName = :firstName")
    , @NamedQuery(name = "DetailsUsers.findByLastName", query = "SELECT d FROM DetailsUsers d WHERE d.lastName = :lastName")
    , @NamedQuery(name = "DetailsUsers.findByTz", query = "SELECT d FROM DetailsUsers d WHERE d.tz = :tz")
    , @NamedQuery(name = "DetailsUsers.findByEMail", query = "SELECT d FROM DetailsUsers d WHERE d.eMail = :eMail")
    , @NamedQuery(name = "DetailsUsers.findByPhone1", query = "SELECT d FROM DetailsUsers d WHERE d.phone1 = :phone1")
    , @NamedQuery(name = "DetailsUsers.findByPhone2", query = "SELECT d FROM DetailsUsers d WHERE d.phone2 = :phone2")
    , @NamedQuery(name = "DetailsUsers.findByBirthdate", query = "SELECT d FROM DetailsUsers d WHERE d.birthdate = :birthdate")
    , @NamedQuery(name = "DetailsUsers.findByIsMale", query = "SELECT d FROM DetailsUsers d WHERE d.isMale = :isMale")
    , @NamedQuery(name = "DetailsUsers.findByInIdCommittee", query = "SELECT d FROM DetailsUsers d WHERE d.inIdCommittee = :inIdCommittee")
    , @NamedQuery(name = "DetailsUsers.findByUpdateDate", query = "SELECT d FROM DetailsUsers d WHERE d.updateDate = :updateDate")
    , @NamedQuery(name = "DetailsUsers.findByApprovalOfPolicies", query = "SELECT d FROM DetailsUsers d WHERE d.approvalOfPolicies = :approvalOfPolicies")
    , @NamedQuery(name = "DetailsUsers.findByApprovalOfPoliciesDate", query = "SELECT d FROM DetailsUsers d WHERE d.approvalOfPoliciesDate = :approvalOfPoliciesDate")
    , @NamedQuery(name = "DetailsUsers.findByDigitalSignature", query = "SELECT d FROM DetailsUsers d WHERE d.digitalSignature = :digitalSignature")
    , @NamedQuery(name = "DetailsUsers.findByUrlDigitalSignature", query = "SELECT d FROM DetailsUsers d WHERE d.urlDigitalSignature = :urlDigitalSignature")
    , @NamedQuery(name = "DetailsUsers.findByDigitalSignatureDate", query = "SELECT d FROM DetailsUsers d WHERE d.digitalSignatureDate = :digitalSignatureDate")
    , @NamedQuery(name = "DetailsUsers.findByPhotocopyOfIdentityCards", query = "SELECT d FROM DetailsUsers d WHERE d.photocopyOfIdentityCards = :photocopyOfIdentityCards")
    , @NamedQuery(name = "DetailsUsers.findByUrlPhotocopyOfIdentityCards", query = "SELECT d FROM DetailsUsers d WHERE d.urlPhotocopyOfIdentityCards = :urlPhotocopyOfIdentityCards")
    , @NamedQuery(name = "DetailsUsers.findByPhotocopyOfIdentityCardsDate", query = "SELECT d FROM DetailsUsers d WHERE d.photocopyOfIdentityCardsDate = :photocopyOfIdentityCardsDate")
    , @NamedQuery(name = "DetailsUsers.findByPowerOfAttorney", query = "SELECT d FROM DetailsUsers d WHERE d.powerOfAttorney = :powerOfAttorney")
    , @NamedQuery(name = "DetailsUsers.findByUrlPowerOfAttorney", query = "SELECT d FROM DetailsUsers d WHERE d.urlPowerOfAttorney = :urlPowerOfAttorney")
    , @NamedQuery(name = "DetailsUsers.findByPowerOfAttorneyDate", query = "SELECT d FROM DetailsUsers d WHERE d.powerOfAttorneyDate = :powerOfAttorneyDate")})

public class DetailsUsers implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
//    @NotNull
    @Column(name = "in_id")
    private Integer inId;
//    @Column(name = "user_in_id")
//    private Integer userInId;
    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "user_in_id", referencedColumnName = "in_id")
    private Users Users;

    public Users getUsers() {
        return Users;
    }

    public void setUsers(Users Users) {
        this.Users = Users;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }
    @Size(max = 45)
    @Column(name = "first_name")
    private String firstName;
    @Size(max = 45)
    @Column(name = "last_name")
    private String lastName;
    @Size(max = 9)
    @Column(name = "tz")
    private String tz;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 45)
    @Column(name = "`e-mail`")
    private String eMail;
    @Size(max = 45)
    @Column(name = "phone_1")
    private String phone1;
    @Size(max = 45)
    @Column(name = "phone_2")
    private String phone2;
    @Column(name = "birthdate")
    @Temporal(TemporalType.DATE)
    private Date birthdate;
    @Column(name = "is_male")
    private Boolean isMale;
    @Column(name = "in_id_committee")
    private Integer inIdCommittee;

    @Column(name = "update_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "approval_of_policies")
    private Boolean approvalOfPolicies;
    @Column(name = "approval_of_policies_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date approvalOfPoliciesDate;
    @Column(name = "digital_signature")
    private Boolean digitalSignature;
    @Size(max = 60)
    @Column(name = "url_digital_signature")
    private String urlDigitalSignature;
    @Column(name = "digital_signature_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date digitalSignatureDate;
    @Column(name = "photocopy_of_identity_cards")
    private Boolean photocopyOfIdentityCards;
    @Size(max = 60)
    @Column(name = "url_photocopy_of_identity_cards")
    private String urlPhotocopyOfIdentityCards;
    @Column(name = "photocopy_of_identity_cards_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date photocopyOfIdentityCardsDate;
    @Column(name = "power_of_attorney")
    private Boolean powerOfAttorney;
    @Size(max = 60)
    @Column(name = "url_power_of_attorney")
    private String urlPowerOfAttorney;
    @Column(name = "power_of_attorney_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date powerOfAttorneyDate;

    public Boolean getPowerOfAttorney() {
        return powerOfAttorney;
    }

    public void setPowerOfAttorney(Boolean powerOfAttorney) {
        this.powerOfAttorney = powerOfAttorney;
    }

    public String getUrlPowerOfAttorney() {
        return urlPowerOfAttorney;
    }

    public void setUrlPowerOfAttorney(String urlPowerOfAttorney) {
        this.urlPowerOfAttorney = urlPowerOfAttorney;
    }

    public Date getPowerOfAttorneyDate() {
        return powerOfAttorneyDate;
    }

    public void setPowerOfAttorneyDate(Date powerOfAttorneyDate) {
        this.powerOfAttorneyDate = powerOfAttorneyDate;
    }

    public DetailsUsers() {
    }

    public DetailsUsers(Integer inId) {
        this.inId = inId;
    }

    public Integer getInId() {
        return inId;
    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }

//    public Integer getUserInId() {
//        return userInId;
//    }
//
//    public void setUserInId(Integer userInId) {
//        this.userInId = userInId;
//    }
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTz() {
        return tz;
    }

    public void setTz(String tz) {
        this.tz = tz;
    }

    public String getEMail() {
        return eMail;
    }

    public void setEMail(String eMail) {
        this.eMail = eMail;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public Boolean getIsMale() {
        return isMale;
    }

    public void setIsMale(Boolean isMale) {
        this.isMale = isMale;
    }

    public Integer getInIdCommittee() {
        return inIdCommittee;
    }

    public void setInIdCommittee(Integer inIdCommittee) {
        this.inIdCommittee = inIdCommittee;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Boolean getApprovalOfPolicies() {
        return approvalOfPolicies;
    }

    public void setApprovalOfPolicies(Boolean approvalOfPolicies) {
        this.approvalOfPolicies = approvalOfPolicies;
    }

    public Date getApprovalOfPoliciesDate() {
        return approvalOfPoliciesDate;
    }

    public void setApprovalOfPoliciesDate(Date approvalOfPoliciesDate) {
        this.approvalOfPoliciesDate = approvalOfPoliciesDate;
    }

    public Boolean getDigitalSignature() {
        return digitalSignature;
    }

    public void setDigitalSignature(Boolean digitalSignature) {
        this.digitalSignature = digitalSignature;
    }

    public String getUrlDigitalSignature() {
        return urlDigitalSignature;
    }

    public void setUrlDigitalSignature(String urlDigitalSignature) {
        this.urlDigitalSignature = urlDigitalSignature;
    }

    public Date getDigitalSignatureDate() {
        return digitalSignatureDate;
    }

    public void setDigitalSignatureDate(Date digitalSignatureDate) {
        this.digitalSignatureDate = digitalSignatureDate;
    }

    public Boolean getPhotocopyOfIdentityCards() {
        return photocopyOfIdentityCards;
    }

    public void setPhotocopyOfIdentityCards(Boolean photocopyOfIdentityCards) {
        this.photocopyOfIdentityCards = photocopyOfIdentityCards;
    }

    public String getUrlPhotocopyOfIdentityCards() {
        return urlPhotocopyOfIdentityCards;
    }

    public void setUrlPhotocopyOfIdentityCards(String urlPhotocopyOfIdentityCards) {
        this.urlPhotocopyOfIdentityCards = urlPhotocopyOfIdentityCards;
    }

    public Date getPhotocopyOfIdentityCardsDate() {
        return photocopyOfIdentityCardsDate;
    }

    public void setPhotocopyOfIdentityCardsDate(Date photocopyOfIdentityCardsDate) {
        this.photocopyOfIdentityCardsDate = photocopyOfIdentityCardsDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inId != null ? inId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailsUsers)) {
            return false;
        }
        DetailsUsers other = (DetailsUsers) object;
        if ((this.inId == null && other.inId != null) || (this.inId != null && !this.inId.equals(other.inId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Attendance.DetailsUsers[ inId=" + inId + " ]";
    }

}
