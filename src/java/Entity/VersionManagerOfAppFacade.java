/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author rachel
 */
@Stateless
public class VersionManagerOfAppFacade extends AbstractFacade<VersionManagerOfApp> {

    @PersistenceContext(unitName = "AppTikSachirWsPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
         
    public  VersionManagerOfApp findByMaxNumVersionisIphone(boolean isIphone)
    {
       return (VersionManagerOfApp) em.createNamedQuery("VersionManagerOfApp.findByMaxNumVersionisIphone").setParameter("isIphone", isIphone).getResultList().get(0);
    }

    public VersionManagerOfAppFacade() {
        super(VersionManagerOfApp.class);
    }
    
}
