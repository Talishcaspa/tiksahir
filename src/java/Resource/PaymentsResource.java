/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Resource;

import Entity.ActivityDocumentation;
import Entity.TblPayment;
import Entity.TblPaymentFacade;
import Entity.TblPaymentNew;
import Entity.TblPaymentNewFacade;
import Entity.UploadingFilesShirim;
import Entity.UploadingFilesShirimFacade;
import Entity.Users;
import Entity.UsersFacade;
import Enum.EnumTypesOfActivity;
import ObjectForJson.MyResponse;
import Tools.Mistake;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.StringReader;
import java.util.ArrayList;
import javax.ejb.EJB;
import javax.json.Json;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
//@javax.ejb.Stateless
/**
 * REST Web Service
 *
 * @author rachel
 */
@Path("generic")
public class PaymentsResource {
    
    @EJB
    UploadingFilesShirimFacade UploadingFilesShirimFAC;
    @EJB
    TblPaymentFacade TblPaymentFAC;
    @EJB
    UsersFacade UsersFAC;
    @EJB
    TblPaymentNewFacade TblPaymentNewFAC; 
    
    @Context
    private UriInfo context;

    /**
     * Creates a new instance of PaymentsResource
     */
    public PaymentsResource() {
    }

    /**
     * Retrieves representation of an instance of Resource.PaymentsResource
     * @return an instance of java.lang.String
     */
       
    //    <editor-fold defaultstate="collapsed"  desc="getPayingByPaycheck">
    // pay file by paychek;
    @POST   
    @Path("getPayingByPaycheck")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPayingByPaycheck(String st) {

        System.out.println("Rachel :   start getPayingByPaycheck  !!!!!!!!! ");
        System.out.println("Rachel :   st  "+st);
        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();
        Gson gsonReturn = builder.create();
        MyResponse response;
        javax.json.JsonReader reader = Json.createReader(new StringReader(st));
        javax.json.JsonObject jsonObject = reader.readObject();
        int in_id_file = jsonObject.getInt("in_id");
        UploadingFilesShirim file = UploadingFilesShirimFAC.find(in_id_file);
        if(file == null){
        
            response = new MyResponse(new Mistake(666, "הקובץ לא נמצא"), false);
            String jsonReturn = gsonReturn.toJson(response);
            return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
             
        }
       
        if(file.getWorkerId()== null){
            
            response = new MyResponse(new Mistake(661, "לא קיים משתמש"), false);
            String jsonReturn = gsonReturn.toJson(response);
            return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
              
        }
       Users user = UsersFAC.find(file.getWorkerId()); 
        System.out.println("Rachel:   user.getInId() :   "+user.getInId());

        if(user==null){
        
            response = new MyResponse(new Mistake(661, "לא קיים משתמש"), false);
            String jsonReturn = gsonReturn.toJson(response);
            return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
        }
        
        if( user.getListActivityDocumentation()==null){
            user.setListActivityDocumentation(new ArrayList<ActivityDocumentation>());
        }
        
        // log
        ActivityDocumentation activityDocumentation = new ActivityDocumentation(EnumTypesOfActivity.File_transferred_to_payment.getNumOfActivity(),file.getFileName(), user);
        user.getListActivityDocumentation().add(activityDocumentation);
        
        TblPayment payment1 = new TblPayment("123", "123", 1, 2, 2, "123", 0.0, Boolean.TRUE, user);

        TblPaymentNew payment = new TblPaymentNew();
        payment.setCompCid(file.getCompCid());
        payment.setCompName(file.getCompName());
        payment.setDay(file.getDay().orElse(0));
        payment.setMonth(file.getMonth());
        payment.setYear(file.getYear());
        payment.setFilepath(file.getFilepath());
        payment.setAmount(file.getAmount().orElse(0.0));
        payment.setPaid(Boolean.TRUE);
        payment.setWorkerId(user.getInId());
//        if(user.getListTblPayment() == null){
//        user.setListTblPayment(new ArrayList<TblPayment>());
//        }
//        user.getListTblPayment().add(payment);
        
//        UsersFAC.edit(user);   

        TblPaymentNewFAC.create(payment);
        file.setPaid(Boolean.TRUE);
        UploadingFilesShirimFAC.edit(file);
//        1968
        response =new MyResponse(new Mistake(), true);
        
        String jsonReturn = gsonReturn.toJson(response);
        return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
    }

}
