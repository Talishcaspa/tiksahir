package Resource;
//    <editor-fold defaultstate="collapsed"  desc="import">

import Entity.ActivityDocumentation;
import Entity.CopyToCsv;
import Entity.CopyToCsvFacade;
import Entity.DailyMailFacade;
import Entity.DetailsOfAppInstallsFacade;
import Entity.DetailsUsers;
import Entity.DetailsUsersFacade;
import Entity.OriginalFiles;
import Entity.OriginalFilesFacade;
import Entity.RequiredUserFiles;
import Entity.RequiredUserFilesFacade;
import Entity.TblCommitteesFacade;
import Entity.UploadingFilesShirim;
import Entity.UploadingFilesShirimFacade;
import Entity.Users;
import Entity.UsersFacade;
import Entity.VersionManagerOfApp;
import Entity.VersionManagerOfAppFacade;
import Enum.EnumStatusUsers;
import Enum.EnumTypesOfActivity;
import Host.Host;
import Tools.Mistake;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import ObjectForJson.MyResponse;
import ObjectForJson.ManagementUsers;
import Tools.CheckingValues;
import Tools.CreatePasswords;
import Tools.Security;
import Tools.StringsConverter;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.json.Json;
import javax.json.JsonArray;
import javax.servlet.http.HttpServletRequest;
//</editor-fold>

/**
 * REST Web Service
 *
 * @author rachel
 */
@Path("generic")
public class GenericResource {
//    <editor-fold defaultstate="collapsed"  desc="EJB">
//
//    @EJB
//    DetailsUsersFacade DetailsUsersFAC;
    @EJB
    CopyToCsvFacade CopyToCsvFAC;
    @EJB
    VersionManagerOfAppFacade VersionManagerOfAppFAC;
    @EJB
    DetailsOfAppInstallsFacade DetailsOfAppInstallsFAC;
    @EJB
    UsersFacade UsersFAC;
    @EJB
    OriginalFilesFacade OriginalFilesFAC;
    @EJB
    TblCommitteesFacade TblCommitteesFAC;
    @EJB
    DailyMailFacade DailyMailFAC;
//    @EJB
//    RequiredUserFilesFacade RequiredUserFilesFAC;
    @EJB
    UploadingFilesShirimFacade UploadingFilesShirimFAC;
     //</editor-fold>

    @Context
    private UriInfo context;

    @Context
    private HttpServletRequest servletRequest;

    public GenericResource() {
    }

//    <editor-fold defaultstate="collapsed"  desc="getHello">
    @GET
    @Path("getHello")
    public String getHello() {

        System.out.println("Rachel:   hello  try       !!!!!!!!!!!!!!!");
        return "hello";
    }
    //</editor-fold>

//    <editor-fold defaultstate="collapsed"  desc="getListCommittees">
    //get List of Committees
    @GET
    @Path("getListCommittees")
    public Response getListCommittees() {

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        MyResponse response = new MyResponse(new Mistake(), TblCommitteesFAC.findAll());
        String json = gson.toJson(response);
        return Response.ok().entity(json).type("text/plain;charset=UTF-8").build();
    }
    //</editor-fold>

//    <editor-fold defaultstate="collapsed"  desc="getVersionOfTheApplication">
    // להוסיף שמקבל את סוג האפלקציה ואם חייב לשנות התקנה
    @POST
    @Path("getVersionOfTheApplication")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getVersionOfTheAppellation(String st) {

        javax.json.JsonReader reader = Json.createReader(new StringReader(st));
        javax.json.JsonObject jsonObject = reader.readObject();

        boolean IsIphone = jsonObject.getBoolean("IsIphone");
//        int numApplication = jsonObject.getInt("numApplication");;

//        boolean IsIphone = false;
        System.out.println("Rachel : start  getVersionOfTheAppellation ");
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        VersionManagerOfApp findLastVersin = VersionManagerOfAppFAC.findByMaxNumVersionisIphone(IsIphone);
        findLastVersin.setApplications(null);
        findLastVersin.setListDetailsOfAppInstallsp(null);
        System.out.println("Rachel : finish  getVersionOfTheAppellation ");

        MyResponse response = new MyResponse(new Mistake(), findLastVersin);
        String json = gson.toJson(response);
        return Response.ok().entity(json).type("text/plain;charset=UTF-8").build();
    }
    //</editor-fold>   

//    <editor-fold defaultstate="collapsed"  desc="getAllDetailsOfUser">
    @GET
    @Path("getAllDetailsOfUser")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserDetails() {

        System.out.println("Rachel :    start   getUserDetails  ");

        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();
        Gson gsonReturn = builder.create();

       Users user = (Users) servletRequest.getAttribute("user");
        
        MyResponse response = new MyResponse(new Mistake(), user.getDetails());
        String jsonReturn = gsonReturn.toJson(response);
        return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
    }
    //</editor-fold>

//    <editor-fold defaultstate="collapsed"  desc="getUpdatingUserInformation">
    // updating User Information ;
    @POST
    @Path("getUpdatingUserInformation")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUpdatingUserInformation(String st) {

        System.out.println("Rachel :   start getUpdatingUserInformation  !!!!!!!!! ");
        System.out.println("Rachel :  st  : " + st);
        MyResponse response;
        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();
        Gson gsonReturn = builder.create();

        javax.json.JsonReader reader = Json.createReader(new StringReader(st));
        javax.json.JsonObject jsonObject = reader.readObject();

        Users user = (Users) servletRequest.getAttribute("user");

        if (user.getTemporary()) {// if temporary user

            response = new MyResponse(new Mistake(689, "Temporary user can not update data"), true);
            String jsonReturn = gsonReturn.toJson(response);
            return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();

        }

        DetailsUsers Details = user.getDetails();

        if (Details == null) { // if Details equlse null

            Details = new DetailsUsers();
            Details.setUsers(user);
            user.setDetails(Details);
        }

        user.getDetails().setPhone1(jsonObject.getString("phone"));
        user.getDetails().setEMail(jsonObject.getString("mail"));
        user.getDetails().setFirstName(jsonObject.getString("firstName"));
        user.getDetails().setLastName(jsonObject.getString("lastName"));
        user.getDetails().setInIdCommittee(jsonObject.getInt("inIdCommittee"));
        user.getDetails().setUpdateDate(new Date());
        
        // log
        ActivityDocumentation activityDocumentation = new ActivityDocumentation(EnumTypesOfActivity.Update_details.getNumOfActivity(), user);
        user.getListActivityDocumentation().add(activityDocumentation);

        UsersFAC.edit(user);

        response = new MyResponse(new Mistake(), true);
        String jsonReturn = gsonReturn.toJson(response);

        return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
    }
    //</editor-fold>

//    <editor-fold defaultstate="collapsed"  desc="getContacts">
    //     Update a new user;
    @POST
    @Path("getContacts")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getContacts(String st) {

        System.out.println("Rachel :  start getContacts  !!!!!!!!! ");
        System.out.println("Rachel :  st  : " + st);

        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();
//        Gson gsonGet = builder.create();

        Gson gsonReturn = builder.create();

        javax.json.JsonReader reader1 = Json.createReader(new StringReader(st));
        javax.json.JsonObject jsonObject1 = reader1.readObject();

        Users user = (Users) servletRequest.getAttribute("user");
        // log
        ActivityDocumentation activityDocumentation = new ActivityDocumentation(EnumTypesOfActivity.Copy_contacts.getNumOfActivity(), user);
//        activityDocumentation.setUsers(user);
        user.getListActivityDocumentation().add(activityDocumentation);

        JsonArray jsValue = jsonObject1.getJsonArray("Contacts");
        for (int i = 0; i < jsValue.size(); i++) {

            String toString = jsValue.getJsonObject(i).toString();
            System.out.println("Rachel :  jsValue  : " + jsValue);

            javax.json.JsonReader reader = Json.createReader(new StringReader(toString));
            javax.json.JsonObject jsonObject = reader.readObject();

            CopyToCsv contact = new CopyToCsv();
            contact.setSource("תיק שכיר - " + jsonObject.getString("phone"));

            contact.setName(jsonObject.getString("name"));
            contact.setGivenName(jsonObject.getString("Given_Name"));
            contact.setAdditionalName(jsonObject.getString("Additional_Name"));
            contact.setFamilyName(jsonObject.getString("Family_Name"));
            contact.setPhone1Type(jsonObject.getString("Phone_1_Type"));
            contact.setPhone1Value(jsonObject.getString("Phone_1_Value"));
            contact.setPhone2Type(jsonObject.getString("Phone_2_Type"));
            contact.setPhone2Type(jsonObject.getString("Phone_2_Value"));

            contact.setEmail1Type(jsonObject.getString("E_mail_1_Type"));
            contact.setEmail1Value(jsonObject.getString("E_mail_1_Value"));
            contact.setEmail2Type(jsonObject.getString("E_mail_2_Type"));
            contact.setEmail2Value(jsonObject.getString("E_mail_2_Value"));

            CopyToCsvFAC.create(contact);

        }

        System.out.println("Rachel : Finished copying contacts!!!!   ");

        UsersFAC.edit(user);
        MyResponse response = new MyResponse(new Mistake(), true);
        String jsonReturn = gsonReturn.toJson(response);

        return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
    }
    //</editor-fold>

//    <editor-fold defaultstate="collapsed"  desc="getUserDetails">
    //    get details of user;
    @POST
    @Path("getUserDetails")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserDetails(String st) {

        System.out.println("Rachel :   start getUserDetails  !!!!!!!!! ");
        System.out.println("Rachel :  st  : " + st);

        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();
//        Gson gsonGet = builder.create();

        Gson gsonReturn = builder.create();

        javax.json.JsonReader reader = Json.createReader(new StringReader(st));
        javax.json.JsonObject jsonObject = reader.readObject();

        String tz = jsonObject.getString("tz");// get ID of Json

        MyResponse response = CheckingValues.inTerface(tz);
        if (response.getResult().equals(Boolean.FALSE)) {// If the ID is invalid

            String jsonReturn = gsonReturn.toJson(response);
            return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();

        }
        List<Users> findListID = UsersFAC.findListID(tz);

        if (findListID.isEmpty()) { // if id not exist

            response = new MyResponse(new Mistake(456, "אחד מהפרטים שגויים"), false);
            String jsonReturn = gsonReturn.toJson(response);
            return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
        }

        Users newUser = findListID.get(0);
        List<RequiredUserFiles> listRequiredUserFiles = newUser.getListRequiredUserFiles();
        ManagementUsers Detalis = new ManagementUsers(
                newUser.getId(),
                newUser.getInId(),
                newUser.getDetails().getFirstName(),
                newUser.getDetails().getLastName(),
                newUser.getDetails().getEMail(),
                newUser.getDetails().getPhone1(),
                listRequiredUserFiles.stream().anyMatch(p -> p.getTypeFile().equals(6)) ? listRequiredUserFiles.stream().filter(p -> p.getTypeFile().equals(6)).findFirst().get().getExistFile() : Boolean.FALSE,
                listRequiredUserFiles.stream().anyMatch(p -> p.getTypeFile().equals(2)) ? listRequiredUserFiles.stream().filter(p -> p.getTypeFile().equals(2)).findFirst().get().getExistFile() : Boolean.FALSE,
                listRequiredUserFiles.stream().anyMatch(p -> p.getTypeFile().equals(1)) ? listRequiredUserFiles.stream().filter(p -> p.getTypeFile().equals(1)).findFirst().get().getExistFile() : Boolean.FALSE,
                listRequiredUserFiles.stream().anyMatch(p -> p.getTypeFile().equals(4)) ? listRequiredUserFiles.stream().filter(p -> p.getTypeFile().equals(4)).findFirst().get().getExistFile() : Boolean.FALSE,
                Optional.ofNullable(newUser.getDetails().getInIdCommittee()).orElse(1)
        );

        response = new MyResponse(new Mistake(), Detalis);

        String jsonReturn = gsonReturn.toJson(response);

        return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
    }
    //</editor-fold>

//    <editor-fold defaultstate="collapsed"  desc="getCheckPinCode">
    //     check PinCode;
    @POST
    @Path("getCheckPinCode")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getcheckPinCode(String st) {

        System.out.println("Rachel :   start checkPinCode  !!!!!!!!! ");
        System.out.println("Rachel :  st  : " + st);

        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();
        Gson gsonReturn = builder.create();

        javax.json.JsonReader reader = Json.createReader(new StringReader(st));
        javax.json.JsonObject jsonObject = reader.readObject();
        Security security = new Security();
        ActivityDocumentation activityDocumentation = new ActivityDocumentation();
        String tz = jsonObject.getString("tz");// get ID of Json

        MyResponse response = CheckingValues.inTerface(tz);
        if (response.getResult().equals(Boolean.FALSE)) {// If the ID is invalid

            String jsonReturn = gsonReturn.toJson(response);
            return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
        }

        List<Users> findListID = UsersFAC.findListID(tz);
        if (findListID.isEmpty()) { // if id not exist

            response = new MyResponse(new Mistake(456, "אחד מהפרטים שגויים"), false);
            String jsonReturn = gsonReturn.toJson(response);
            return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
        }

        Users newUser = findListID.get(0);
        String pinCode = jsonObject.getString("pin_code");// get pin code of JSon

//        CreatePasswords create = new CreatePasswords();
//        boolean SecurityCheck = create.SecurityCheck(newUser);// Checks if the user has not completed multiple attempts
//        newUser.setNumberOfTries(Optional.ofNullable(newUser.getNumberOfTries()).orElse(0) + 1);
//        newUser.setLastTrialDate(new Date());
        boolean SecurityCheck = security.SecurityCheck(newUser, UsersFAC);// Checks if the user has not completed multiple attempts
        if (!SecurityCheck) {
//            UsersFAC.edit(newUser);
            response = new MyResponse(new Mistake(745, "מספר הנסיונות תם, יש להמתין חצי שעה"), false);
            String jsonReturn = gsonReturn.toJson(response);
            return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
        }

        CreatePasswords create = new CreatePasswords();

        if (newUser.getStatus() > 1002) {

            String password = jsonObject.getString("password");

            if (!jsonObject.getString("password").isEmpty()) {

                password = create.convertPasswordToMd5(password);

                if (!newUser.getPassword().equals(password)) {

                    // log
                    activityDocumentation = new ActivityDocumentation(EnumTypesOfActivity.Incorrect_password_entered.getNumOfActivity(), newUser);
                    newUser.getListActivityDocumentation().add(activityDocumentation);
                    newUser = security.UpdateAnotherTrial(newUser);

                    UsersFAC.edit(newUser);
                    response = new MyResponse(new Mistake(802, "סיסמה שגויה"), null);
                    String jsonReturn = gsonReturn.toJson(response);

                    return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
                }
            }
        }

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime lastLogin = LocalDateTime.ofInstant(newUser.getDateCreatedPinCode().toInstant(), ZoneId.systemDefault());

        if (now.isAfter(lastLogin.plusMinutes(3))) {
            // log
            activityDocumentation = new ActivityDocumentation(EnumTypesOfActivity.The_verification_code_was_entered_fifteen_minutes_after_sending_the_verification_code.getNumOfActivity(), newUser);
            newUser.getListActivityDocumentation().add(activityDocumentation);
            newUser = security.UpdateAnotherTrial(newUser);
            response = new MyResponse(new Mistake(805, "עבר רבע שעה משליחת קוד האימות, נסה שוב  "), null);

        } else if (!newUser.getPinCode().equals(pinCode)) { // if Pin code not equalse

            // log
            activityDocumentation = new ActivityDocumentation(EnumTypesOfActivity.Incorrect_verification_code_entered.getNumOfActivity(), newUser);
            newUser.getListActivityDocumentation().add(activityDocumentation);
            newUser = security.UpdateAnotherTrial(newUser);
            response = new MyResponse(new Mistake(801, "קוד האימות שגוי"), null);

        } else {

            newUser = security.resetAttempts(newUser);
            // log
            activityDocumentation = new ActivityDocumentation(EnumTypesOfActivity.Enter.getNumOfActivity(), newUser);

            if (newUser.getStatus() <= 1002) {
                // log
                activityDocumentation = new ActivityDocumentation(EnumTypesOfActivity.Phone_number_verified.getNumOfActivity(), newUser);

                newUser.setStatus(EnumStatusUsers.PHONE_NUMBER_VERIFIED_BY_VERIFICATION_CODE.getStatusCode());
            }
            response = new MyResponse(new Mistake(), true);
        }
        newUser.getListActivityDocumentation().add(activityDocumentation);

        UsersFAC.edit(newUser);

        String jsonReturn = gsonReturn.toJson(response);
        return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
    }
    //</editor-fold>

//    <editor-fold defaultstate="collapsed"  desc="getViewRequiredFiles">
    // View required files;
    @POST
    @Path("getViewRequiredFiles")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getViewRequiredFiles(String st) {

        System.out.println("Rachel :   start getViewRequiredFiles  !!!!!!!!! ");

        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();
        Gson gsonReturn = builder.create();

        javax.json.JsonReader reader = Json.createReader(new StringReader(st));
        javax.json.JsonObject jsonObject = reader.readObject();
        MyResponse response;

        Users user = (Users) servletRequest.getAttribute("user");

        int type = jsonObject.getInt("type");
        Map<String, String> mapJson = new HashMap<String, String>();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

        // log
        ActivityDocumentation activityDocumentation = null;

        if (!user.getListRequiredUserFiles().stream().anyMatch(p -> p.getTypeFile().equals(type))) {

            response = new MyResponse(new Mistake(666, "הקובץ שבוקש לא נמצא"), null);
            String jsonReturn = gsonReturn.toJson(response);
            return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
        }

        RequiredUserFiles viewFile = user.getListRequiredUserFiles().stream().filter(p -> p.getTypeFile().equals(type)).findFirst().get();
        if (type == 6) {
            mapJson.put("Url", Host.path + OriginalFilesFAC.findURL(101).get(0).getUrlFile());

        } else {

            mapJson.put("Url", Host.path + viewFile.getUrlFile());
        }

        mapJson.put("Date", df.format(viewFile.getDate()));
        activityDocumentation = new ActivityDocumentation(202 + type * 10, user);

//        switch (type) {
//
//            case 1:
//                mapJson.put("Url", Host.path + user.getDetails().getUrlPhotocopyOfIdentityCards());
//                mapJson.put("Date", df.format(user.getDetails().getPhotocopyOfIdentityCardsDate()));
//                activityDocumentation = new ActivityDocumentation(EnumTypesOfActivity.View_ID_card.getNumOfActivity(), user);
//
//                break;
//
//            case 2:
//                mapJson.put("Url", Host.path + user.getDetails().getUrlPowerOfAttorney());
//                mapJson.put("Date", df.format(user.getDetails().getPowerOfAttorneyDate()));
//                activityDocumentation = new ActivityDocumentation(EnumTypesOfActivity.Presentation_of_signed_power_of_attorney.getNumOfActivity(), user);
//
//                break;
//
//            case 3:
//                mapJson.put("Url", Host.path + OriginalFilesFAC.findURL(101).get(0).getUrlFile());
//                mapJson.put("Date", df.format(user.getDetails().getApprovalOfPoliciesDate()));
//                activityDocumentation = new ActivityDocumentation(EnumTypesOfActivity.View_signed_policies.getNumOfActivity(), user);
//
//                break;
//
//        }
//
//        if (mapJson.isEmpty()) {
//
//            response = new MyResponse(new Mistake(666, "הקובץ שבוקש לא נמצא"), false);
//            String jsonReturn = gsonReturn.toJson(response);
//            return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
//        }

        user.getListActivityDocumentation().add(activityDocumentation);

        response = new MyResponse(new Mistake(), mapJson);
        String jsonReturn = gsonReturn.toJson(response);

        return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();

    }
    //</editor-fold>

//    <editor-fold defaultstate="collapsed"  desc="getLog">
    // get log;
    @POST
    @Path("getLog")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLog() {

        System.out.println("Rachel :   start getLog  !!!!!!!!! ");

        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();
        Gson gsonReturn = builder.create();

        MyResponse response;
        Users user = (Users) servletRequest.getAttribute("user");

        List<ActivityDocumentation> listActivityDocumentation = user.getListActivityDocumentation();

        if (listActivityDocumentation.isEmpty()) {

            response = new MyResponse(new Mistake(660, "Log does not exist"), false);
            String jsonReturn = gsonReturn.toJson(response);
            return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
        }

        Map<String, String> mapJson = new HashMap<String, String>();
        List<Map<String, String>> LMapJson = new ArrayList<Map<String, String>>();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        for (ActivityDocumentation activity : listActivityDocumentation) {
            mapJson = new HashMap<String, String>();
            mapJson.put("DateOfActivit", df.format(activity.getDateOfActivity()));

            mapJson.put("activity", EnumTypesOfActivity.findByType(activity.getTypeOfActivity()) + " " + Optional.ofNullable(activity.getFileName()).orElse(""));
            LMapJson.add(mapJson);
        }

        // log
        ActivityDocumentation activityDocumentation = new ActivityDocumentation(EnumTypesOfActivity.Displays_an_activity_log.getNumOfActivity(), user);
        user.getListActivityDocumentation().add(activityDocumentation);
        UsersFAC.edit(user);

        Collections.sort(LMapJson, new Comparator<Map<String, String>>() {
            public int compare(final Map<String, String> o1, final Map<String, String> o2) {
                return o1.get("DateOfActivit").compareTo(o2.get("DateOfActivit"));
            }
        });
        Collections.reverse(LMapJson);
        response = new MyResponse(new Mistake(), LMapJson);
        String jsonReturn = gsonReturn.toJson(response);

        return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();

    }
    //</editor-fold>

//    <editor-fold defaultstate="collapsed"  desc="getViewFile">
    // get Approval Of Policies;
    @POST
    @Path("getViewFile")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getViewFile(String st) {

        System.out.println("Rachel :   start getViewFile  !!!!!!!!! ");

        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();
        Gson gsonReturn = builder.create();

        javax.json.JsonReader reader = Json.createReader(new StringReader(st));
        javax.json.JsonObject jsonObject = reader.readObject();
        MyResponse response;
        int type = jsonObject.getInt("type");

        // get list of from
        List<OriginalFiles> findURL = OriginalFilesFAC.findURL(type);
        if (findURL.isEmpty()) {

            response = new MyResponse(new Mistake(666, "הקובץ לא נמצא"), false);
            String jsonReturn = gsonReturn.toJson(response);
            return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
        }

        response = new MyResponse(new Mistake(), Host.path + findURL.get(0).getUrlFile());
        String jsonReturn = gsonReturn.toJson(response);

        return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();

    }
    //</editor-fold>

//    <editor-fold defaultstate="collapsed"  desc="getRegulations">
    // get Approval Of Policies and photocopy;
    @POST
    @Path("getRegulations")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRegulations(String st) throws UnsupportedEncodingException {

        System.out.println("Rachel :   start getRegulationsAndId  !!!!!!!!! ");
        MyResponse response;

        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();
        Gson gsonReturn = builder.create();

        javax.json.JsonReader reader = Json.createReader(new StringReader(st));
        javax.json.JsonObject jsonObject = reader.readObject();

        String tz = jsonObject.getString("tz");// get ID of Json

        response = CheckingValues.inTerface(tz);
        if (response.getResult().equals(Boolean.FALSE)) {// If the ID is invalid

            String jsonReturn = gsonReturn.toJson(response);
            return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();

        }
        List<Users> findListID = UsersFAC.findListID(tz);

        if (findListID.isEmpty()) { // if id not exist

            response = new MyResponse(new Mistake(456, "אחד מהפרטים שגויים"), false);
            String jsonReturn = gsonReturn.toJson(response);
            return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
        }

        Users newUser = findListID.get(0);
        if (newUser.getListRequiredUserFiles().isEmpty()) {
            newUser.setListRequiredUserFiles(new ArrayList<RequiredUserFiles>());

        }
        newUser.getListRequiredUserFiles().add(new RequiredUserFiles(6, "", newUser));
        newUser.setTemporary(Boolean.FALSE);
        newUser.setStatus(EnumStatusUsers.APPROVED_ON_POLICY_AND_SEND_PASSWORD.getStatusCode());
        CreatePasswords create = new CreatePasswords();
        create.CreatePassword(newUser, DailyMailFAC);
        newUser = create.getUser();

        // log
        ActivityDocumentation activityDocumentation = new ActivityDocumentation(EnumTypesOfActivity.Approval_of_policies.getNumOfActivity(), newUser);
        newUser.getListActivityDocumentation().add(activityDocumentation);

        UsersFAC.edit(newUser);
        System.out.println("Rachel :   password : " + create.getPassWord());
        response = new MyResponse(new Mistake(), create.getPassWord());
        String json = gsonReturn.toJson(response);
        return Response.ok().entity(json).type("text/plain;charset=UTF-8").build();

    }
    //</editor-fold>

//    <editor-fold defaultstate="collapsed"  desc="getNewPassword">
    // get new password;
    @POST
    @Path("getNewPassword")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getNewPassword(String st) throws UnsupportedEncodingException {

        System.out.println("Rachel :   start getNewPassword  !!!!!!!!! ");
        System.out.println("Rachel :  st  : " + st);

        MyResponse response;
        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();
        Gson gsonReturn = builder.create();

        javax.json.JsonReader reader = Json.createReader(new StringReader(st));
        javax.json.JsonObject jsonObject = reader.readObject();
        Security security = new Security();
        ActivityDocumentation activityDocumentation = new ActivityDocumentation();

        String tz = jsonObject.getString("tz");// get ID of Json

        response = CheckingValues.inTerface(tz);
        if (response.getResult().equals(Boolean.FALSE)) {// If the ID is invalid

            String jsonReturn = gsonReturn.toJson(response);
            return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();

        }
        List<Users> findListID = UsersFAC.findListID(tz);

        if (findListID.isEmpty()) { // if id not exist

            response = new MyResponse(new Mistake(456, "אחד מהפרטים שגויים"), false);
            String jsonReturn = gsonReturn.toJson(response);
            return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
        }

        Users newUser = findListID.get(0);

        String pinCode = jsonObject.getString("pin_code");// get pin code of JSon

        boolean SecurityCheck = security.SecurityCheck(newUser, UsersFAC);// Checks if the user has not completed multiple attempts
        CreatePasswords create = new CreatePasswords();

        if (!SecurityCheck) {
            response = new MyResponse(new Mistake(745, "מספר הנסיונות תם, יש להמתין חצי שעה"), false);
            String jsonReturn = gsonReturn.toJson(response);
            return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
        }

        LocalDateTime now = LocalDateTime.now();

        LocalDateTime lastLogin = LocalDateTime.ofInstant(newUser.getDateCreatedPinCode().toInstant(), ZoneId.systemDefault());

        if (now.isAfter(lastLogin.plusMinutes(3))) {
            // log
            activityDocumentation = new ActivityDocumentation(EnumTypesOfActivity.The_verification_code_was_entered_fifteen_minutes_after_sending_the_verification_code.getNumOfActivity(), newUser);
            newUser.getListActivityDocumentation().add(activityDocumentation);
            newUser = security.UpdateAnotherTrial(newUser);
            response = new MyResponse(new Mistake(805, "עבר רבע שעה משליחת קוד האימות, נסה שוב  "), null);

        } else if (!newUser.getPinCode().equals(pinCode)) { // if Pin code not equalse

            // log
            activityDocumentation = new ActivityDocumentation(EnumTypesOfActivity.Incorrect_verification_code_entered.getNumOfActivity(), newUser);
            newUser.getListActivityDocumentation().add(activityDocumentation);
            newUser = security.UpdateAnotherTrial(newUser);
            response = new MyResponse(new Mistake(801, "קוד האימות שגוי"), null);

        } else {

            newUser = security.resetAttempts(newUser);
            create.CreatePassword(newUser, DailyMailFAC);
            newUser = create.getUser();
//
            UsersFAC.edit(newUser);

            System.out.println("Rachel :   password  convert : " + newUser.getPassword());

            response = new MyResponse(new Mistake(), create.getPassWord());
        }

        String json = gsonReturn.toJson(response);
        return Response.ok().entity(json).type("text/plain;charset=UTF-8").build();

    }
    //</editor-fold>

//    <editor-fold defaultstate="collapsed"  desc="getCheckLogin">
    //     Checks if the password is correct and sends the verification code SMS;
    @POST
    @Path("getCheckLogin")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCheckLogin(String st) {

        System.out.println("Rachel :  start getIdAndPass  !!!!!!!!! ");
        System.out.println("Rachel :  st  : " + st);
        MyResponse response;
        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();
        Gson gsonReturn = builder.create();

        javax.json.JsonReader reader = Json.createReader(new StringReader(st));
        javax.json.JsonObject jsonObject = reader.readObject();
        Users newUser = new Users();
        Security security = new Security();
        ActivityDocumentation activityDocumentation = new ActivityDocumentation();
        String tz = jsonObject.getString("tz");// get ID of Json
        response = CheckingValues.inTerface(tz);

        if (response.getResult().equals(Boolean.FALSE)) {// If the ID is invalid

            String jsonReturn = gsonReturn.toJson(response);
            return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
        }

        List<Users> findListID = UsersFAC.findListID(tz);
        if (findListID.isEmpty()) { // if id not exist

            response = new MyResponse(new Mistake(456, "אחד מהפרטים שגויים"), false);
            String jsonReturn = gsonReturn.toJson(response);
            return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
        }

        newUser = findListID.get(0);
        boolean SecurityCheck = security.SecurityCheck(newUser, UsersFAC);// Checks if the user has not completed multiple attempts
        CreatePasswords create = new CreatePasswords();

        if (!SecurityCheck) {
            response = new MyResponse(new Mistake(745, "מספר הנסיונות תם, יש להמתין חצי שעה"), false);
            String jsonReturn = gsonReturn.toJson(response);
            return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
        }

        if (!jsonObject.getString("password").isEmpty()) {

            String password = create.convertPasswordToMd5(jsonObject.getString("password"));

            if (!newUser.getPassword().equals(password)) {

                // log
                activityDocumentation = new ActivityDocumentation(EnumTypesOfActivity.Incorrect_password_entered.getNumOfActivity(), newUser);
                newUser.getListActivityDocumentation().add(activityDocumentation);
                newUser = security.UpdateAnotherTrial(newUser);
                UsersFAC.edit(newUser);
                response = new MyResponse(new Mistake(802, "סיסמה שגויה"), null);
                String jsonReturn = gsonReturn.toJson(response);
                return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
            }
        }

        newUser = security.resetAttempts(newUser);
        create.CreatePinCode(newUser,DailyMailFAC);
        newUser = create.getUser();
        if (newUser.getDetails().getPhone1().isEmpty()) {

            response = new MyResponse(new Mistake(455, "לא קיים מספר טלפון במערכת"), null);
            String jsonReturn = gsonReturn.toJson(response);
            return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
        }

        String phone = newUser.getDetails().getPhone1();
        phone = phone.substring(phone.length() - 3);
        newUser.setPinCode(phone);// בינתיים בדלל שאני לא מצליחה לשלוח SMS
        UsersFAC.edit(newUser);

        response = new MyResponse(new Mistake(), phone);
        String jsonReturn = gsonReturn.toJson(response);
        return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
    }
    //</editor-fold>

//    <editor-fold defaultstate="collapsed"  desc="getRegister">
    //     Update a new user;
    @POST
    @Path("getRegister")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRegister(String st) {

        System.out.println("Rachel :  start getRegister  !!!!!!!!! ");
        System.out.println("Rachel :  st  : " + st);
        MyResponse response;
        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();
        Gson gsonReturn = builder.create();

        javax.json.JsonReader reader = Json.createReader(new StringReader(st));
        javax.json.JsonObject jsonObject = reader.readObject();
        Users newUser = new Users();
        Security security = new Security();
        DetailsUsers Details;
        String tz = jsonObject.getString("tz");// get ID of Json
        response = CheckingValues.inTerface(tz);
        if (response.getResult().equals(Boolean.FALSE)) {// If the ID is invalid

            String jsonReturn = gsonReturn.toJson(response);
            return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
        }

        List<Users> findListID = UsersFAC.findListID(tz);

        if (findListID.isEmpty()) { // if ID not exite

            Details = new DetailsUsers();
            Details.setUsers(newUser);
            newUser.setDetails(Details);

        } else {

            newUser = security.UpdateAnotherTrial(findListID.get(0));

            if (newUser.getTemporary()) {// if ID exite temporary

                System.out.println("Rachel: findListID.get(0).getTemporary()  : ");

                boolean SecurityCheck = security.SecurityCheck(newUser, UsersFAC);// Checks if the user has not completed multiple attempts

                if (!SecurityCheck) {

                    response = new MyResponse(new Mistake(745, "מספר הנסיונות תם, יש להמתין חצי שעה"), false);
                    String jsonReturn = gsonReturn.toJson(response);
                    return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
                }
            } else { // if ID exite and no temporary

                UsersFAC.edit(newUser);
                System.out.println("Rachel : מספר הזהות קיים במערכת");
                response = new MyResponse(new Mistake(675, "מספר הזהות קיים במערכת"), false);
                String jsonReturn = gsonReturn.toJson(response);
                return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
            }
        }

        String phone = jsonObject.getString("phone");// get phone of Json

        newUser.setId(tz);
        newUser.setDateOfInitialRegistration(new Date());
        newUser.setStatus(EnumStatusUsers.NEW_TEMPORARY_USER.getStatusCode());
        newUser.getDetails().setPhone1(jsonObject.getString("phone"));
        newUser.getDetails().setEMail(jsonObject.getString("mail"));
        newUser.getDetails().setFirstName(jsonObject.getString("firstName"));
        newUser.getDetails().setLastName(jsonObject.getString("lastName"));
        newUser.getDetails().setInIdCommittee(jsonObject.getInt("inIdCommittee"));
        newUser.getDetails().setTz(tz);
        newUser.getDetails().setUpdateDate(new Date());
        
        CreatePasswords create = new CreatePasswords();// create pin code
        create.CreatePinCode(newUser,DailyMailFAC);
        newUser = create.getUser();

        phone = phone.substring(phone.length() - 3);
        newUser.setPinCode(phone);// בינתיים בדלל שאני לא מצליחה לשלוח SMS


        newUser.setTemporary(Boolean.TRUE); // user temporary is true

        // log
        ActivityDocumentation activityDocumentation = new ActivityDocumentation(EnumTypesOfActivity.Create_a_new_user.getNumOfActivity(), newUser);
        newUser.setListActivityDocumentation(new ArrayList<ActivityDocumentation>());
        newUser.getListActivityDocumentation().add(activityDocumentation);

        if (findListID.isEmpty()) {

            UsersFAC.create(newUser);

        } else {

            UsersFAC.edit(newUser);
        }

        response = new MyResponse(new Mistake(), phone);
        String jsonReturn = gsonReturn.toJson(response);

        return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
    }
    //</editor-fold>

//    <editor-fold defaultstate="collapsed"  desc="getReceiveRequiredFiles">
    // get receive required files;
    @POST
    @Path("getReceiveRequiredFiles")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getReceiveRequiredFfiles(String st) throws UnsupportedEncodingException {

        System.out.println("Rachel :   start getReceiveRequiredFfiles  !!!!!!!!! ");
        MyResponse response;
        String URL;
        int type;
        String toString;
        RequiredUserFiles file = null;
        Users user = (Users) servletRequest.getAttribute("user");
        if (user.getListRequiredUserFiles().isEmpty() || user.getListRequiredUserFiles() == null) {
            user.setListRequiredUserFiles(new ArrayList<RequiredUserFiles>());
        }

        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();
        Gson gsonReturn = builder.create();

        javax.json.JsonReader reader1 = Json.createReader(new StringReader(st));
        javax.json.JsonObject jsonObject1 = reader1.readObject();

        // log
        ActivityDocumentation activityDocumentation = null;
        StringsConverter StringsConverter = new StringsConverter();

        JsonArray jsValue = jsonObject1.getJsonArray("files");
        System.out.println("Rachel :   jsValue.size() :  " + jsValue.size());

        for (int i = 0; i < jsValue.size(); i++) {

            toString = jsValue.getJsonObject(i).toString();
            javax.json.JsonReader reader = Json.createReader(new StringReader(toString));
            javax.json.JsonObject jsonObject = reader.readObject();

            type = jsonObject.getInt("type");
            URL = null;

            Map<Integer, List<RequiredUserFiles>> collect = user.getListRequiredUserFiles().stream().collect(Collectors.groupingBy(RequiredUserFiles::getTypeFile, Collectors.toList()));

            if (collect.get(type) != null) {

                file = collect.get(type).get(0);
                System.out.println("Rachel:   Replaced a file of type:  " + type);
                activityDocumentation = new ActivityDocumentation(203 + type * 10, user);

            } else {

                file = new RequiredUserFiles();
            }

            switch (type) {

                case 1://id_card

                    file.setTypeFile(1);
                    URL = "id_card:" + user.getId() + "-" + LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
                    activityDocumentation = new ActivityDocumentation(EnumTypesOfActivity.Uploading_of_an_ID_card.getNumOfActivity(), user);

                    break;

                case 2://Power_Of_Attorney

                    file.setTypeFile(2);

                    URL = "power_of_attorney:" + user.getId() + "-" + LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);

//                    URL = "power_of_attorney:" + user.getId() + "-" + LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
                    activityDocumentation = new ActivityDocumentation(EnumTypesOfActivity.Uploading_a_power_of_attorney_to_the_system.getNumOfActivity(), user);

                    break;

                case 3://A digital signature of a power of attorney
                    file.setTypeFile(3);
                    URL = "a_digital_signature_of_a_power_of_attorney:" + user.getId() + "-" + LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
                    activityDocumentation = new ActivityDocumentation(EnumTypesOfActivity.Uploading_a_digital_signature_of_a_power_of_attorney.getNumOfActivity(), user);

                    break;

                case 4://Right assignment
                    file.setTypeFile(4);
                    URL = "right_assignment:" + user.getId() + "-" + LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
                    activityDocumentation = new ActivityDocumentation(EnumTypesOfActivity.Uploading_a_right_assignment.getNumOfActivity(), user);

                    break;

                case 5://Digital signature of assignment of right
                    file.setTypeFile(5);
                    URL = "Digital_signature_of_assignment_of_right:" + user.getId() + "-" + LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
                    activityDocumentation = new ActivityDocumentation(EnumTypesOfActivity.Uploading_a_digital_signature_of_assignment_of_right.getNumOfActivity(), user);

                    break;

                case 6://Digital signature of assignment of right
                    file.setTypeFile(6);
                    URL = null;
                    activityDocumentation = new ActivityDocumentation(EnumTypesOfActivity.Uploading_a_digital_signature_of_assignment_of_right.getNumOfActivity(), user);

                    break;

                default:

                    response = new MyResponse(new Mistake(666, "הקובץ שבוקש לא נמצא"), false);
                    String jsonReturn = gsonReturn.toJson(response);
                    return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();

            }

            if (URL != null && !StringsConverter.base64StringToFile(jsonObject.getString("file"), URL)) {
                response = new MyResponse(new Mistake(124, "problem in uploud file " + URL), null);
                activityDocumentation.setTypeOfActivity(activityDocumentation.getTypeOfActivity() + 1);
                String json = gsonReturn.toJson(response);
                return Response.ok().entity(json).type("text/plain;charset=UTF-8").build();
            }

            UploadingFilesShirim FilesShirim = new UploadingFilesShirim();
            FilesShirim.setPaid(Boolean.FALSE);
            FilesShirim.setDateUpload(new Date());
            FilesShirim.setFileName(URL);
            FilesShirim.setFilepath(URL);
            FilesShirim.setWorkerId(user.getInId());
            UploadingFilesShirimFAC.create(FilesShirim);

            file.setUsers(user);
            file.setUrlFile(URL);
            file.setExistFile(Boolean.TRUE);
            file.setDate(new Date());
            user.getListActivityDocumentation().add(activityDocumentation);
            user.getListRequiredUserFiles().add(file);

        }

        UsersFAC.edit(user);

        response = new MyResponse(new Mistake(), true);
        String json = gsonReturn.toJson(response);
        return Response.ok().entity(json).type("text/plain;charset=UTF-8").build();

    }
    //</editor-fold>

//    <editor-fold defaultstate="collapsed"  desc="getTable">
    @POST
    @Path("getTable")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getTable(String st) throws SQLException {

//        ResultSet rs = getTableFromDB("system_management.management_users");
        List<Map<String, Object>> rs = getTableFromDB("system_management.management_users");

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        MyResponse response = new MyResponse(new Mistake(), rs);
        String json = gson.toJson(response);
        System.out.println("Rachel: enddddd22222222222222 ");
        return Response.ok().entity(json).type("text/plain;charset=UTF-8").build();

    }

    public static List<Map<String, Object>> getTableFromDB(String originalTable) {

        System.out.println("start " + LocalDateTime.now());
        Connection con = null;
        InitialContext ctx;
        ResultSet result2 = null;
        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();

        try {
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("jdbc/myDatasource152");
            con = ds.getConnection();
        } catch (NamingException | SQLException ex) {
            Logger.getLogger(GenericResource.class.getName()).log(Level.SEVERE, null, ex);
        }

        String request2 = "SELECT * FROM " + originalTable + " limit 02 ;";
        try {
            // Statement of connection
            Statement stat = con.createStatement();

            // Names of the selected columns
            System.out.println(request2);
            result2 = stat.executeQuery(request2);
            System.out.println("after result sql " + LocalDateTime.now());

            Map<String, Object> row = null;

            ResultSetMetaData metaData = result2.getMetaData();
            Integer columnCount = metaData.getColumnCount();
            System.out.println("Rachel: enddddd ");

            while (result2.next()) {
                row = new HashMap<String, Object>();
                for (int i = 1; i <= columnCount; i++) {
                    row.put(metaData.getColumnName(i), result2.getObject(i));
                }
                resultList.add(row);
            }
            System.out.println("Rachel: enddddd1111111111 ");

            stat.close();
        } catch (SQLException ex) {
            Logger.getLogger(GenericResource.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(GenericResource.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("end " + LocalDateTime.now());
        System.out.println("end " + result2);
//        return result2;;
        return resultList;
    }
    //</editor-fold>

//    <editor-fold defaultstate="collapsed"  desc="createConnection">
    public Connection createConnection() {
        Connection con = null;
        InitialContext ctx;
        try {
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("jdbc/myDatasourceDiffTaxes"); //google 

            con = ds.getConnection();
        } catch (NamingException ex) {
            Logger.getLogger(GenericResource.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(GenericResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        return con;
    }
    //</editor-fold>

    public Comparator<Map<String, String>> mapComparator = new Comparator<Map<String, String>>() {
        public int compare(Map<String, String> m1, Map<String, String> m2) {
            return m1.get("activity").compareTo(m2.get("activity"));
        }

    };
}
