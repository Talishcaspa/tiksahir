/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Resource;

import Entity.ActivityDocumentation;
import Entity.TblAttendance;
import Entity.TblAttendanceFacade;
import Entity.Users;
import Entity.UsersFacade;
import Enum.EnumTypesOfActivity;
import ObjectForJson.MyResponse;
import Tools.Mistake;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Year;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONException;

/**
 * REST Web Service
 *
 * @author rachel
 */
@Path("generic")
public class attendanceResource1 {

    @EJB
    TblAttendanceFacade TblAttendanceFAC;
    @EJB
    UsersFacade UsersFAC;

    @Context
    private UriInfo context;

    @Context
    private HttpServletRequest servletRequest;

    /**
     * Creates a new instance of attendanceResource1
     */
    public attendanceResource1() {
    }

//    <editor-fold defaultstate="collapsed"  desc="getEnter">
    // update enter;
    @POST
    @Path("getEnter")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEnter() {

        System.out.println("Rachel :   start getEnter  !!!!!!!!! ");

        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();
        Gson gsonReturn = builder.create();
        MyResponse response;
        Users user = (Users) servletRequest.getAttribute("user");
        if (user.getListTblAttendance().isEmpty()) {
            user.setListTblAttendance(new ArrayList<TblAttendance>());
        }

        boolean anyMatch = user.getListTblAttendance().stream().anyMatch(p -> !p.getIsExit());
        System.out.println("Rachel :    anyMatch " + anyMatch);

        if (anyMatch) {

            response = new MyResponse(new Mistake(501, "עדיין לא יצאת...."), false);
            String jsonReturn = gsonReturn.toJson(response);
            return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
        }

        LocalDate today = LocalDate.now();

        TblAttendance attendance = new TblAttendance(user);
        attendance.setIsExit(Boolean.FALSE);
        attendance.setSource("app");
        attendance.setStartTime(new Date());
        attendance.setYear(Year.now().getValue());
        attendance.setMonth(today.getMonthValue());
        attendance.setDay(today.getMonthValue());
        attendance.setDayName(today.getDayOfWeek().toString());
        attendance.setEndTime(null);

        user.getListTblAttendance().add(attendance);

        // log
        ActivityDocumentation activityDocumentation = new ActivityDocumentation(EnumTypesOfActivity.Presence_Update_enter.getNumOfActivity(), user);
        user.getListActivityDocumentation().add(activityDocumentation);
        UsersFAC.edit(user);

        response = new MyResponse(new Mistake(), true);
        String jsonReturn = gsonReturn.toJson(response);
        return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();

    }
    //</editor-fold>

//    <editor-fold defaultstate="collapsed"  desc="getCheckEnter">
    // check if enter ;
    @POST
    @Path("getCheckEnter")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCheckEnter() {

        System.out.println("Rachel :   start getCheckEnter  !!!!!!!!! ");

        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();
        Gson gsonReturn = builder.create();

        MyResponse response;
        Users user = (Users) servletRequest.getAttribute("user");

        response = new MyResponse(new Mistake(), CheckEnter(user));
        String jsonReturn = gsonReturn.toJson(response);
        return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();

    }
    //</editor-fold>

//    <editor-fold defaultstate="collapsed"  desc="getCheckExit">
    // check if exit ;
    @POST
    @Path("getCheckExit")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCheckExit() {

        System.out.println("Rachel :   start getCheckExit  !!!!!!!!! ");

        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();
        Gson gsonReturn = builder.create();

        MyResponse response;
        Users user = (Users) servletRequest.getAttribute("user");

//        TblAttendance get = user.getListTblAttendance().get(user.getListTblAttendance().size()-1);
        response = new MyResponse(new Mistake(), CheckExit(user));
        String jsonReturn = gsonReturn.toJson(response);
        return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();

    }
    //</editor-fold>

//    <editor-fold defaultstate="collapsed"  desc="getExit">
    // update exit ;
    @POST
    @Path("getExit")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getExit() {

        System.out.println("Rachel :   start getExit  !!!!!!!!! ");

        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();
        Gson gsonReturn = builder.create();

        MyResponse response;
        Users user = (Users) servletRequest.getAttribute("user");

        boolean anyMatch = user.getListTblAttendance().stream().allMatch(p -> p.getIsExit());

        if (anyMatch) {

            response = new MyResponse(new Mistake(502, "עדיין לא נכנסת...."), false);
            String jsonReturn = gsonReturn.toJson(response);
            return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
        }

        // log
        ActivityDocumentation activityDocumentation = new ActivityDocumentation(EnumTypesOfActivity.Presence_Update_exit.getNumOfActivity(), user);
        user.getListActivityDocumentation().add(activityDocumentation);
        user.getListTblAttendance().get(user.getListTblAttendance().size() - 1).setEndTime(new Date());
        user.getListTblAttendance().get(user.getListTblAttendance().size() - 1).setIsExit(Boolean.TRUE);
        user.getListTblAttendance().get(user.getListTblAttendance().size() - 1).setSource("app");
        UsersFAC.edit(user);// למה לא עובד???

        response = new MyResponse(new Mistake(), true);
        String jsonReturn = gsonReturn.toJson(response);
        return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();

    }
    //</editor-fold>

//    <editor-fold defaultstate="collapsed"  desc="getListAttendance">
    // get list of attendance;
    @POST
    @Path("getListAttendance")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getListAttendance() throws IOException, JSONException, ParseException {

        System.out.println("Rachel :   start getListForm  !!!!!!!!! ");
        MyResponse response;
        String json;
        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();
        Gson gsonReturn = builder.create();

        Users user = (Users) servletRequest.getAttribute("user");
        // get list of attendances

        List<TblAttendance> attendances = user.getListTblAttendance();
        if (user.getListTblAttendance().isEmpty()) {

            response = new MyResponse(new Mistake(556, "לא הזנת עדיין נוכחות"), false);
            json = gsonReturn.toJson(response);
            return Response.ok().entity(json).type("text/plain;charset=UTF-8").build();
        }

        System.out.println("Rachel :   After downloading the table attendances  " + attendances.size());

        DateFormat df = new SimpleDateFormat("HH:mm:ss");

        Map<String, String> mapJson = new HashMap<String, String>();
        List<Map<String, String>> LMapJson = new ArrayList<Map<String, String>>();
        LocalDate date;
        for (TblAttendance attendance : attendances) {

            Date d = new Date();
            date = LocalDate.of(attendance.getYear(), attendance.getMonth(), attendance.getDay());
            mapJson = new HashMap<String, String>();

            mapJson.put("DateStart", date.toString());
            mapJson.put("DateEnd", date.toString());
            mapJson.put("DayName", attendance.getDayName());
            mapJson.put("StartTime", df.format(attendance.getStartTime()));
            mapJson.put("EndTime", attendance.getEndTime() == null ? "לא הוזן" : df.format(attendance.getEndTime()));

            LMapJson.add(mapJson);
        }
        // log
        ActivityDocumentation activityDocumentation = new ActivityDocumentation(EnumTypesOfActivity.Display_presence.getNumOfActivity(), user);
        user.getListActivityDocumentation().add(activityDocumentation);

        response = new MyResponse(new Mistake(), LMapJson);
        json = gsonReturn.toJson(response);
        return Response.ok().entity(json).type("text/plain;charset=UTF-8").build();

    }
    //</editor-fold>

    public Boolean CheckEnter(Users user) {

        System.out.println("Rachel :user.getListTblAttendance()  " + user.getListTblAttendance());

        if (user.getListTblAttendance().isEmpty()) {
            return Boolean.FALSE;
        }

        return user.getListTblAttendance().stream().anyMatch(p -> !p.getIsExit());
    }

    public Boolean CheckExit(Users user) {

        if (user.getListTblAttendance().isEmpty()) {
            return Boolean.TRUE;
        }
        return user.getListTblAttendance().stream().allMatch(p -> p.getIsExit());

    }

}
