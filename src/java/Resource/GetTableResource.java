/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Resource;

import Entity.FormList;
import Entity.FormListFacade;
import Entity.FormMenu;
import Entity.FormMenuFacade;
import Entity.UploadingFilesShirim;
import Entity.UploadingFilesShirimFacade;
import Host.Host;
import Tools.Mistake;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.json.Json;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import ObjectForJson.GetFile;
import ObjectForJson.MyResponse;
import ObjectForJson.NameForm;
import ObjectForJson.SymbolFileAndUser;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Optional;

/**
 * REST Web Service
 *
 * @author rachel
 */
@Path("generic")
public class GetTableResource {

    @EJB
    FormMenuFacade FormMenuFacade;
    @EJB
    FormListFacade FormListFacade;
    @EJB
    UploadingFilesShirimFacade UploadingFilesShirimFacade;

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of GetTableResource
     */
    public GetTableResource() {
    }

    /**
     * Retrieves representation of an instance of Resource.GetTableResource
     *
     * @return an instance of java.lang.String
     */
    
    //    <editor-fold defaultstate="collapsed"  desc="getListFormMenu">
    // get list of object form where is_display_app true;
    @POST
    @Path("getListFormMenu")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getListFormMenu() {

        System.out.println("Rachel :   start getListForm  !!!!!!!!! ");

        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();
        Gson gsonReturn = builder.create();

        // get list of from
        List<FormMenu> FormMenu = FormMenuFacade.findAll();

        List<FormMenu> listFormMenu = new ArrayList<FormMenu>();

        FormMenu.stream().forEach(p -> listFormMenu.add(new FormMenu(p.getKey(), p.getName(), p.getIcon())));
        MyResponse response = new MyResponse(new Mistake(), listFormMenu);
        String json = gsonReturn.toJson(response);
        return Response.ok().entity(json).type("text/plain;charset=UTF-8").build();

    }

    //</editor-fold>
    
    //    <editor-fold defaultstate="collapsed"  desc="getListForm">
    // get list of object form where is_display_app true;
    @POST
    @Path("getListForm")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getListForm(String st) {

        System.out.println("Rachel :   start getListForm  !!!!!!!!! ");

        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();
//        Gson gsonGet = builder.create();

        Gson gsonReturn = builder.create();

        javax.json.JsonReader reader = Json.createReader(new StringReader(st));
        javax.json.JsonObject jsonObject = reader.readObject();

        int type = jsonObject.getInt("type");

        // get list of from
        List<FormList> formList = FormListFacade.findAllIsDisplayAppTrue(type);

        List<NameForm> listNameForm = new ArrayList<NameForm>();

        formList.stream().forEach(p -> listNameForm.add(new NameForm(p.getSymbol(), p.getFormName(), p.getIcon() , p.getPossibilityToUploadFiles())));

        MyResponse response = new MyResponse(new Mistake(), listNameForm);
        String jsonReturn = gsonReturn.toJson(response);

        return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
    }
    //</editor-fold>

    //    <editor-fold defaultstate="collapsed"  desc="getListUploadingFiles">
//    get list of object getListUploadingFiles where symble eqlse what get;
    @POST
    @Path("getListUploadingFiles")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getListOfTableUploading_files_shirim(String st) {

        System.out.println("Rachel :   start getListUploadingFiles  !!!!!!!!! ");

        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();
        Gson gsonReturn = builder.create();

        //  Disassembles the JSON  
        Gson gson = builder.create();
        SymbolFileAndUser JsonGetFile = gson.fromJson(st, SymbolFileAndUser.class);

        // get the selected SYMBOL
        String symbolForm = JsonGetFile.getSymbol();
        System.out.println("Rachel: symbolForm: " + symbolForm);

        int workerId = JsonGetFile.getWorkerId();
        System.out.println("Rachel: workerId: " + workerId);

        int year = JsonGetFile.getYear();
        System.out.println("Rachel: year: " + year);
        List<UploadingFilesShirim> ListUploadingFiles = new ArrayList<UploadingFilesShirim>();
//                  ListUploadingFiles=  UploadingFilesShirimFacade.findByFileSymbolWorkerId(symbolForm, workerId);
        System.out.println("Rachel:   ListUploadingFiles : " + ListUploadingFiles.size());
        ListUploadingFiles = UploadingFilesShirimFacade.findByFileSymbolWorkerId(symbolForm, workerId);

//        if (year == 0) {
//            // get list of UploadingFiles
//
//            ListUploadingFiles = UploadingFilesShirimFacade.findByFileSymbolWorkerId(symbolForm, workerId);
//
//        } else {
//
//            // get list of UploadingFiles by year
//            ListUploadingFiles = UploadingFilesShirimFacade.findByFileSymbolWorkerIdYear(symbolForm, workerId, year);
//            ListUploadingFiles.stream().filter(p -> p.getYear() == year).collect(Collectors.toList());
////         ArrayList ListFileYear = new ArrayList(10);
//            int size = 12;
//            ArrayList<UploadingFilesShirim> ListFileYear = new ArrayList<UploadingFilesShirim>(size);
//            System.out.println("Rachel:   ListFileYear  .size() : " + ListFileYear.size());
//            UploadingFilesShirim uNew= new UploadingFilesShirim();
//            for (int i = 1; i <= size; i++) {
//                int ii = i;
//
//                List<UploadingFilesShirim> filter = ListUploadingFiles.stream().filter(p -> p.getMonth().equals(ii)).collect(Collectors.toList());
//                if (ListFileYear.size()< i) {
//
//                    System.out.println("Rachel :        ListFileYear.size()<i     ");
//                } else {
//                
//                if(filter.size() >0){
//                UploadingFilesShirim u = filter.get(0);
//               
//                    ListFileYear.set(i, u);
//                
//                
//                }
//                else{
//                   System.out.println("Rachel :       filter.size() <0    ");
//
//                ListFileYear.set(i,uNew );
//                }             
//                }
//            }
//            if (ListUploadingFiles.size() > 0) {
//                ListUploadingFiles.stream().forEach(p -> ListFileYear.set(p.getMonth(), p));
//                ListUploadingFiles = ListFileYear;
//
//            }

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        List<GetFile> ListGetFile = new ArrayList<GetFile>();

//        ListUploadingFiles.stream().forEach(p
//                -> {
//            try {
//                ListGetFile.add(
//                        new GetFile(p.getInId(), p.getNote(), p.getFileName(), p.getCompCid(), p.getDay().orElse(0), p.getMonth(), p.getYear(),
//                                p.getFilepath() == null ? null : Host.path + p.getFilepath(), p.getPaid().orElse(Boolean.FALSE),
//                                p.getAmount().orElse(0.0),df.parse(df.format(p.getLastOpeningDate())), p.getIsOpened(), p.getIsAuthorizedToOpen()));
//            } catch (ParseException ex) {
//                Logger.getLogger(GetTableResource.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        });
         ListUploadingFiles.stream().forEach(p
                -> {
                ListGetFile.add(
                        new GetFile(p.getInId(), p.getNote(), p.getFileName(), p.getCompCid(), p.getCompName(), p.getDay().orElse(0), p.getMonth(), p.getYear(),
                                p.getFilepath() == null ? null : Host.path + p.getFilepath(), p.getPaid().orElse(Boolean.FALSE),
                                p.getAmount().orElse(0.0),p.getLastOpeningDate(), p.getIsOpened(),p.getFormList().getPaymentRequired()? p.getIsAuthorizedToOpen() : null));
        });
         
         System.out.println("Rachel:   ListGetFile : " + ListGetFile.size());

        MyResponse response = new MyResponse(new Mistake(), ListGetFile);
        String json = gsonReturn.toJson(response);

        return Response.ok().entity(json).type("text/plain;charset=UTF-8").build();
    }
    //</editor-fold>

    //    <editor-fold defaultstate="collapsed"  desc="getListFilesNotPaid">
//    get list of object getListUploadingFiles where paid equles false ;
    @POST
    @Path("getListFilesNotPaid")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getListFilesNotPaid(String st) {

        System.out.println("Rachel :   start getListUploadingFiles  !!!!!!!!! ");

        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();
        Gson gsonReturn = builder.create();

        //  Disassembles the JSON  
        Gson gson = builder.create();
        SymbolFileAndUser JsonGetFile = gson.fromJson(st, SymbolFileAndUser.class);

        int workerId = JsonGetFile.getWorkerId();
        System.out.println("Rachel: workerId: " + workerId);

        List<UploadingFilesShirim> ListUploadingFiles = new ArrayList<UploadingFilesShirim>();
        ListUploadingFiles = UploadingFilesShirimFacade.findByWorkerIdPaid(workerId);
        System.out.println("Rachel:   ListUploadingFiles : " + ListUploadingFiles.size());

        List<GetFile> ListGetFile = new ArrayList<GetFile>();

        ListUploadingFiles.stream().filter(p -> Optional.ofNullable(p.getFormList()).orElse(null) != null)
                .filter(p -> p.getFormList().getFormMenu().getKey() == 102 || p.getFormList().getFormMenu().getKey() == 103)
                .forEach(p
                        -> ListGetFile.add(
                        new GetFile(p.getInId(), p.getNote(), p.getFormList().getFormName(), p.getFileName(), p.getCompCid(), p.getCompName(), p.getDay().orElse(0), p.getMonth(), p.getYear(),
                                p.getFilepath() == null ? null : Host.path + p.getFilepath(), p.getPaid().orElse(Boolean.FALSE),
                                p.getAmount().orElse(0.0))));

        System.out.println("Rachel:   ListGetFile : " + ListGetFile.size());

        MyResponse response = new MyResponse(new Mistake(), ListGetFile);
        String json = gsonReturn.toJson(response);

        return Response.ok().entity(json).type("text/plain;charset=UTF-8").build();
    }
    //</editor-fold>   

}
