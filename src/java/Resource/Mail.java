/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Resource;

import Entity.DailyMail;
import Entity.DailyMailFacade;
import ObjectForJson.MyResponse;
import Tools.Mistake;
import static Tools.SendMail.SendMail;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.StringReader;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.json.Json;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author rachel
 */
@Path("generic")
public class Mail {

    @EJB
    DailyMailFacade DailyMailFAC;

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of Mail
     */
    public Mail() {
    }

    /**
     * Retrieves representation of an instance of Resource.Mail
     *
     * @return an instance of java.lang.String
     */
    //    <editor-fold defaultstate="collapsed"  desc="getMail">
    @POST
    @Path("getMail")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMail(String st) throws Exception {

        System.out.println("Rachel :   start getMail  !!!!!!!!! ");

//        SendMail.sendSimpleMail("e583250365@gmail.com","","");
        SendMail.sendSimpleMail("hadar@taxmail.co.il", "rachel", "Shafer");

        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();
//        Gson gsonGet = builder.create();

        Gson gsonReturn = builder.create();

        javax.json.JsonReader reader = Json.createReader(new StringReader(st));
        javax.json.JsonObject jsonObject = reader.readObject();

        MyResponse response = new MyResponse(new Mistake(), true);
        String jsonReturn = gsonReturn.toJson(response);

        return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
    }
    //</editor-fold>

    @POST
    @Path("getMailNew")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMailNew(String st) throws Exception {

        System.out.println("Rachel :   start getMailNew  !!!!!!!!! ");

        List<DailyMail> listMails = DailyMailFAC.findAll().stream().filter(p -> p.getStatus() == 0).collect(Collectors.toList());

        // Constructs JSON to return the function
        GsonBuilder builder = new GsonBuilder();
//        Gson gsonGet = builder.create();
        Gson gsonReturn = builder.create();

        javax.json.JsonReader reader = Json.createReader(new StringReader(st));
        javax.json.JsonObject jsonObject = reader.readObject();

        // SMTP info
        String host = "smtp.sendgrid.net";
        String port = "2525";

        String mailFrom = "trinity222";
        String password = "222trinity222";

        // message info
        String mailTo = "rachel.becher@taxmail.co.il";
        String subject = "New email with attachments";
        String message = "this mail";
        String mailToBcc = "rachel.becher@taxmail.co.il";

        // attachments
        String[] attachFiles = new String[1];
        attachFiles[0] = "/var/www/reports/worker_diff_taxes_reports/moduls/uploadFiles/87_1534150241090.JPG";
//		attachFiles[1] = "e:/Test/Music.mp3";
//		attachFiles[2] = "e:/Test/Video.mp4";
        
        
        MyResponse response;
        try {
            SendMail(host, port, mailFrom, password, mailTo, mailToBcc,
                    subject, message, attachFiles);
            System.out.println("Email sent.");
            listMails.forEach(p -> p.setStatus(1));
            listMails.forEach(p -> p.setDateSend(new Date()));
            response = new MyResponse(new Mistake(), "Email sent");

        } catch (Exception ex) {
            System.out.println("Could not send email.");
            listMails.forEach(p -> p.setStatus(2));
            listMails.forEach(p -> p.setDateSend(new Date()));
            ex.printStackTrace();
            response = new MyResponse(new Mistake(119, "Could not send email"), null);
        }

        String jsonReturn = gsonReturn.toJson(response);

        return Response.ok().entity(jsonReturn).type("text/plain;charset=UTF-8").build();
    }
    //</editor-fold>

}
