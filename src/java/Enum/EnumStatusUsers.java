/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Enum;

/**
 *
 * @author rachel
 */
public enum EnumStatusUsers {

    NEW_TEMPORARY_USER(1001, "משתמש זמני חדש"),
    PHONE_NUMBER_VERIFIED_BY_VERIFICATION_CODE(1002,"אומת מספר הטלפון על ידי קוד האימות - 1002"),
    APPROVED_ON_POLICY_AND_SEND_PASSWORD(1003,"תקנון חתום ושליחת סיסמא"),
    NEW_USER(1003, " משתמש חדש"),
    SMS_SENT_THE_PIN_CODE(1004,"נשלח SMS של קוד האימות - 1003"),
    INITIAL_ENTRY_PASS_THE_QUOTA_OF_ATTEMPTS_WAIT(1005,"משתמש חדש, המתן, עברו מספר הנסיונות האפשריים"),
    PASS_THE_QUOTA_OF_ATTEMPTS_WAIT(1006, "עבר מספר הנסיונות האפשרים, המתן");

    private EnumStatusUsers(int StatusCode, String StatusMsg) {
        this.StatusMsg = StatusMsg;
        this.StatusCode = StatusCode;
    }

    private int StatusCode;
    private String StatusMsg;

    public String getStatusMsg() {
        return StatusMsg;
    }

    public int getStatusCode() {
        return StatusCode;
    }

//
//   public enum ErrorCode {
//        NOT_CONNECTED(1001, "‫למערכת‪.‬‬ ‫מחובר‬ ‫אינך‬ ‫‪-‬‬ ‫‪1001‬‬"),
//        NOT_PERMITED_ADD_COMP(1011, "חברה‬ ‫הוספת‬ ‫לבצע‬ ‫מורשה‬ ‫אינך‬ ‫‪-‬‬ ‫‪1011‬‬"),
//        NOT_PERMITED_EDIT_COMP(1012, "חברה‬ ‫פרטי‬ ‫לערוך‬ ‫מורשה‬ ‫אינך‬ ‫‪-‬‬ ‫‪1012‬‬"),
//        NOT_PERMITED_DELETE_COMP(1013, "‫חברה‬ ‫למחוק‬ ‫מורשה‬ ‫אינך‬ ‫‪-‬‬ ‫‪1013‬‬"),
//        NOT_PERMITED_ADD_VER(1021, "‫גרסה‬ ‫הוספת‬ ‫לבצע‬ ‫מורשה‬ ‫אינך‬ ‫‪-‬‬ ‫‪1021‬‬"),
//        NOT_PERMITED_EDIT_VER(1022, "‫גרסה‬ ‫לערוך‬ ‫מורשה‬ ‫אינך‬ ‫‪-‬‬ ‫‪1022‬‬"),
//        NOT_PERMITED_DELETE_VER(1023, "‫גרסה‬ ‫למחוק‬ ‫מורשה‬ ‫אינך‬ ‫‪-‬‬ ‫‪1023‬‬"),
//        UNKNOWN_COLUMN(1040,"1040 - בטבלה המבוקשת חסרה עמודה");
//        
//
//        private final int errCode;
//        private final String errMsg;
//
//        ErrorCode(int code, String msg) {
//            this.errCode = code;
//            this.errMsg = msg;
//        }
//
//        public int getErrCode() {
//            return this.errCode;
//        }
//        
//        public String getErrMsg() {
//            return this.errMsg;
//        }
//    }
}
