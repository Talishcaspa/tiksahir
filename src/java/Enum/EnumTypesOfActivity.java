/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Enum;

/**
 *
 * @author rachel
 */
public enum EnumTypesOfActivity {

    Create_a_new_user(100, "יצירת משתמש חדש"),
        Phone_number_verified(101, "אומת מספר הטלפון"),

    Approval_of_policies(102, "אישור תקנון"),
    Sending_a_new_password(109, "יצירת סיסמה חדשה"),
    Sending_a_verification_code(110, "שליחת קוד אימות "),
    Update_details(200, "עידכון פרטי משתמש "),
    
    Uploading_of_an_ID_card(210, "העלאת צילום תעודת זהות"),
    Uploading_a_power_of_attorney_to_the_system(220, "העלאת ייפוי כח למערכת"),
    Uploading_a_digital_signature_of_a_power_of_attorney(230,"העלאת חתימה דיגיטאלית של ייפוי כח"),
    Uploading_a_right_assignment(240,"העלאה של המחאת זכות"),
    Uploading_a_digital_signature_of_assignment_of_right(250,"העלאת חתימה דיגיטאלית של המחאת זכות"),
    Uploading_a_regulations(260,"העלאת תקנון"),
    
    Problem_uploading_ID_card(211, "בעיה בהעלאת תעודת זהות "),
    Problem_in_uploading_power_of_attorney(221, "בעיה בהעלאה של ייפוי הכח "),
    Problem_in_uploading_a_digital_signature_of_a_power_of_attorney(231,"העלאת חתימה דיגיטאלית של ייפוי כח"),
    Problem_in_uploading_a_right_assignment(241,"העלאה של המחאת זכות"),
    Problem_in_uploading_a_digital_signature_of_assignment_of_right(251,"העלאת חתימה דיגיטאלית של המחאת זכות"),
    Problem_in_uploading_a_regulations(261, "בעיה בהעלאת תקנון"),
    
    View_ID_card(212, "הצגת תעודת זהות "),
    Presentation_of_signed_power_of_attorney(222, "הצגת ייפוי כח חתום "),
    View_a_digital_signature_of_a_power_of_attorney(232, "הצגת חתימה דיגיטאלית של ייפוי כח"),
    View_a_right_assignment(242, "הצגת המחאת זכות "),
    View_a_digital_signature_of_assignment_of_right(252, "הצגת חתימה דיגיטאלית המחאת זכות "),
    View_signed_policies(262, "הצגת תקנון חתום"),

    ID_card_file_has_been_replaced(213, "הוחלפה התעודת זהות "),
    signed_power_of_attorney_file_has_been_replaced(223, "הוחלף הייפוי כח החתום "),
    a_digital_signature_of_a_power_of_attorney_file_has_been_replaced(233, "הוחלפה החתימה הדיגיטאלית של ייפוי כח"),
    a_right_assignment_file_has_been_replaced(243, "הוחלפה ההמחאת זכות"),
    a_digital_signature_of_assignment_of_right_file_has_been_replaced(253, "הוחלפה החתימה הדיגיטאלית של ההמחאת זכות"),
    signed_policies_file_has_been_replaced(263, "הוחלף התקנון החתום"),

    Displays_an_activity_log(513, "הצגת לוג פעילות "),
    Presence_Update_enter(301,"עדכון כניסה"),
    Presence_Update_exit(302,"עדכון יציאה"),
    Display_presence(305,"הצגת נוכחות"),
    Copy_contacts(401, "העתקת אנשי קשר"),
    Open_a_file(500, "פתיחת קובץ"),
    file_uploading(501, "העלאת קובץ"),
    Delete_file(502, "מחיקת קובץ"),
    File_transferred_to_payment(505,"העברת הקובץ לתשלום"),
    The_number_of_attempts_has_to_wait_half_an_hour(700, "עברו מספר הנסיונות, יש להמתין חצי שעה "),
    Incorrect_password_entered(701, "הוכנסה סיסמה שגויה "),
    Incorrect_verification_code_entered(702, "הוכנס קוד אימות שגוי "),
    The_verification_code_was_entered_fifteen_minutes_after_sending_the_verification_code(703, "קוד האימות נכנס לאחר רבע שעה משליחת הקוד "),
    Enter(900, "כניסה");

    private EnumTypesOfActivity(int numOfActivity, String typesOfActivity) {
        this.numOfActivity = numOfActivity;
        this.typesOfActivity = typesOfActivity;
    }

    private int numOfActivity;
    private String typesOfActivity;

    public int getNumOfActivity() {
        return numOfActivity;
    }

    public String getTypesOfActivity() {
        return typesOfActivity;
    }

    public static String findByType(int Type) {
        for (EnumTypesOfActivity v : values()) {
            if (v.getNumOfActivity() == Type) {
                return v.getTypesOfActivity();
            }
        }
        return "פעילות לא ידועה";
    }

}
