/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AttendanceEntity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rivka
 */
@Entity
@Table(name = "erp.tadirut")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tadirut.findAll", query = "SELECT t FROM Tadirut t")
    , @NamedQuery(name = "Tadirut.findByInId", query = "SELECT t FROM Tadirut t WHERE t.inId = :inId")
    , @NamedQuery(name = "Tadirut.findByDescNum", query = "SELECT t FROM Tadirut t WHERE t.descNum = :descNum")
    , @NamedQuery(name = "Tadirut.findByDescription", query = "SELECT t FROM Tadirut t WHERE t.description = :description")
    , @NamedQuery(name = "Tadirut.findByMinutes", query = "SELECT t FROM Tadirut t WHERE t.minutes = :minutes")
    , @NamedQuery(name = "Tadirut.findByHours", query = "SELECT t FROM Tadirut t WHERE t.hours = :hours")
    , @NamedQuery(name = "Tadirut.findByDaysInMonth", query = "SELECT t FROM Tadirut t WHERE t.daysInMonth = :daysInMonth")
    , @NamedQuery(name = "Tadirut.findByMonth", query = "SELECT t FROM Tadirut t WHERE t.month = :month")
    , @NamedQuery(name = "Tadirut.findByDaysInWeek", query = "SELECT t FROM Tadirut t WHERE t.daysInWeek = :daysInWeek")})
public class Tadirut implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "in_id")
    private Integer inId;
    @Column(name = "desc_num")
    private Integer descNum;
    @Size(max = 100)
    @Column(name = "description")
    private String description;
    @Size(max = 45)
    @Column(name = "minutes")
    private String minutes;
    @Size(max = 45)
    @Column(name = "hours")
    private String hours;
    @Size(max = 45)
    @Column(name = "days_in_month")
    private String daysInMonth;
    @Size(max = 45)
    @Column(name = "month")
    private String month;
    @Size(max = 45)
    @Column(name = "days_in_week")
    private String daysInWeek;

    public Tadirut() {
    }

    public Tadirut(Integer inId) {
        this.inId = inId;
    }

    public Integer getInId() {
        return inId;
    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }

    public Integer getDescNum() {
        return descNum;
    }

    public void setDescNum(Integer descNum) {
        this.descNum = descNum;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMinutes() {
        return minutes;
    }

    public void setMinutes(String minutes) {
        this.minutes = minutes;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public String getDaysInMonth() {
        return daysInMonth;
    }

    public void setDaysInMonth(String daysInMonth) {
        this.daysInMonth = daysInMonth;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDaysInWeek() {
        return daysInWeek;
    }

    public void setDaysInWeek(String daysInWeek) {
        this.daysInWeek = daysInWeek;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inId != null ? inId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tadirut)) {
            return false;
        }
        Tadirut other = (Tadirut) object;
        if ((this.inId == null && other.inId != null) || (this.inId != null && !this.inId.equals(other.inId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Tadirut[ inId=" + inId + " ]";
    }
    
}
