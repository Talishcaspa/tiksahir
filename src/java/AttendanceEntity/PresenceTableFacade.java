/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AttendanceEntity;

import Entity.AbstractFacade;
import java.security.Timestamp;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.converter.LocalDateTimeStringConverter;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author rivka
 */
@Stateless
public class PresenceTableFacade extends AbstractFacade<PresenceTable> {

    @PersistenceContext(unitName = "AppTikSachirWsPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PresenceTableFacade() {
        super(PresenceTable.class);
    }

    public int getType(ManagementUsersAttendance user) {


        LocalDateTime d = LocalDate.now().atTime(0, 0);
        Date fromDate = Date.from(d.atZone(ZoneId.systemDefault()).toInstant());

                int type = ((Number) em.createNamedQuery("PresenceTable.findCount").setParameter("fromD", fromDate).setParameter("toD", new Date()).setParameter("idUser", user).getSingleResult()).intValue();

        if (type == 0) {
            type = 1;//this user have to enter entrance the first time.
        }
        else if (type % 2 == 0)//this user have to enter exit.
        {
            type = 1;
        } else//this user have to enter entrance.
        {
            type = 2;
        }

        return type;

    }
    
}
