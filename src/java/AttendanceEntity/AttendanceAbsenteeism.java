/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AttendanceEntity;

import java.io.Serializable;
import java.util.Optional;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rivka
 */
@Entity
@Table(name = "erp.attendance_absenteeism")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AttendanceAbsenteeism.findAll", query = "SELECT a FROM AttendanceAbsenteeism a")
    , @NamedQuery(name = "AttendanceAbsenteeism.findByInId", query = "SELECT a FROM AttendanceAbsenteeism a WHERE a.inId = :inId")
    , @NamedQuery(name = "AttendanceAbsenteeism.findByAbsenteeism", query = "SELECT a FROM AttendanceAbsenteeism a WHERE a.absenteeism = :absenteeism")})
public class AttendanceAbsenteeism implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "in_id")
    private Integer inId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "absenteeism")
    private String absenteeism;

    public AttendanceAbsenteeism() {
    }

    public AttendanceAbsenteeism(Integer inId) {
        this.inId = inId;
    }

    public AttendanceAbsenteeism(Integer inId, String absenteeism) {
        this.inId = inId;
        this.absenteeism = absenteeism;
    }
    
    public Optional<Integer> getInId() {
        return Optional.ofNullable(inId);
    }

//    public Integer getInId() {
//        return inId;
//    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }
    
        public Optional<String> getAbsenteeism() {
        return Optional.ofNullable( absenteeism);
    }

//    public String getAbsenteeism() {
//        return absenteeism;
//    }

    public void setAbsenteeism(String absenteeism) {
        this.absenteeism = absenteeism;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inId != null ? inId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AttendanceAbsenteeism)) {
            return false;
        }
        AttendanceAbsenteeism other = (AttendanceAbsenteeism) object;
        if ((this.inId == null && other.inId != null) || (this.inId != null && !this.inId.equals(other.inId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.AttendanceAbsenteeism[ inId=" + inId + " ]";
    }
    
}
