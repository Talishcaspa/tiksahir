/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AttendanceEntity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rivka
 */
@Entity
@Table(name = "contactApi.contacts")
@XmlRootElement
@NamedQueries({
      @NamedQuery(name = "Contacts.findAll", query = "SELECT c FROM Contacts c")
    , @NamedQuery(name = "Contacts.findByInId", query = "SELECT c FROM Contacts c WHERE c.inId = :inId")
    , @NamedQuery(name = "Contacts.findByDisplayName", query = "SELECT c FROM Contacts c WHERE c.displayName = :displayName")
    , @NamedQuery(name = "Contacts.findByEmail", query = "SELECT c FROM Contacts c WHERE c.email = :email")
    , @NamedQuery(name = "Contacts.findByPhone", query = "SELECT c FROM Contacts c WHERE c.phone = :phone")
    , @NamedQuery(name = "Contacts.findByEmailAndPhone", query = "SELECT c FROM Contacts c WHERE c.email <> '' and (c.phone like '%0529679825%' or c.phone like '%0506455895%') group by c.phone")})
public class Contacts implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "in_id")
    private Integer inId;
//    @Size(max = 120)
//    @Column(name = "resourceAddress")
//    private String resourceAddress;
//    @Size(max = 100)
//    @Column(name = "username")
//    private String username;
//    @Size(max = 120)
//    @Column(name = "resourceName")
//    private String resourceName;
    @Size(max = 120)
    @Column(name = "displayName")
    private String displayName;
//    @Size(max = 120)
//    @Column(name = "familyName")
//    private String familyName;
//    @Size(max = 120)
//    @Column(name = "givenName")
//    private String givenName;
//    @Size(max = 120)
//    @Column(name = "middleName")
//    private String middleName;
//    @Size(max = 120)
//    @Column(name = "displayNameLastFirst")
//    private String displayNameLastFirst;
//    @Size(max = 150)
//    @Column(name = "jobTitle")
//    private String jobTitle;
//    @Size(max = 150)
//    @Column(name = "company")
//    private String company;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 150)
    @Column(name = "email")
    private String email;
//    @Size(max = 100)
//    @Column(name = "email_type")
//    private String emailType;
//    @Size(max = 100)
//    @Column(name = "email_formattedType")
//    private String emailformattedType;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 100)
    @Column(name = "phone")
    private String phone;
//    @Size(max = 100)
//    @Column(name = "phone_canonicalForm")
//    private String phonecanonicalForm;
//    @Size(max = 100)
//    @Column(name = "phone_type")
//    private String phoneType;
//    @Size(max = 100)
//    @Column(name = "phone_formattedType")
//    private String phoneformattedType;
//    @Size(max = 45)
//    @Column(name = "id_worker")
//    private String idWorker;
//    @Size(max = 45)
//    @Column(name = "num_yazran")
//    private String numYazran;
//    @Size(max = 45)
//    @Column(name = "id_project")
//    private String idProject;
//    @Size(max = 45)
//    @Column(name = "cid")
//    private String cid;
//    @Basic(optional = false)
//    @NotNull
//    @Column(name = "comp_in_id")
//    private int compInId;
//    @Size(max = 255)
//    @Column(name = "comp_name")
//    private String compName;
//    @Size(max = 255)
//    @Column(name = "name")
//    private String name;
//    @Size(max = 45)
//    @Column(name = "phone1")
//    private String phone1;
//    @Size(max = 45)
//    @Column(name = "phone2")
//    private String phone2;
//    @Size(max = 45)
//    @Column(name = "phone_2_send")
//    private String phone2Send;
//    @Size(max = 255)
//    @Column(name = "email2")
//    private String email2;
//    @Size(max = 255)
//    @Column(name = "email_2_send")
//    private String email2Send;
//    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
//    @Size(max = 45)
//    @Column(name = "fax")
//    private String fax;
//    @Size(max = 255)
//    @Column(name = "job_description")
//    private String jobDescription;
//    @Size(max = 255)
//    @Column(name = "broker")
//    private String broker;
//    @Size(max = 255)
//    @Column(name = "ahrayot")
//    private String ahrayot;
//    @Size(max = 255)
//    @Column(name = "ekerot")
//    private String ekerot;
//    @Size(max = 45)
//    @Column(name = "send_daily_mail_desc")
//    private String sendDailyMailDesc;
//    @Size(max = 45)
//    @Column(name = "matara")
//    private String matara;


    public Contacts() {
    }

    public Contacts(Integer inId) {
        this.inId = inId;
    }

//    public Contacts(Integer inId, int compInId) {
//        this.inId = inId;
//        this.compInId = compInId;
//    }

    public Integer getInId() {
        return inId;
    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }

//    public String getResourceAddress() {
//        return resourceAddress;
//    }
//
//    public void setResourceAddress(String resourceAddress) {
//        this.resourceAddress = resourceAddress;
//    }
//
//    public String getUsername() {
//        return username;
//    }
//
//    public void setUsername(String username) {
//        this.username = username;
//    }
//
//    public String getResourceName() {
//        return resourceName;
//    }
//
//    public void setResourceName(String resourceName) {
//        this.resourceName = resourceName;
//    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

//    public String getFamilyName() {
//        return familyName;
//    }
//
//    public void setFamilyName(String familyName) {
//        this.familyName = familyName;
//    }
//
//    public String getGivenName() {
//        return givenName;
//    }
//
//    public void setGivenName(String givenName) {
//        this.givenName = givenName;
//    }
//
//    public String getMiddleName() {
//        return middleName;
//    }
//
//    public void setMiddleName(String middleName) {
//        this.middleName = middleName;
//    }
//
//    public String getDisplayNameLastFirst() {
//        return displayNameLastFirst;
//    }
//
//    public void setDisplayNameLastFirst(String displayNameLastFirst) {
//        this.displayNameLastFirst = displayNameLastFirst;
//    }
//
//    public String getJobTitle() {
//        return jobTitle;
//    }
//
//    public void setJobTitle(String jobTitle) {
//        this.jobTitle = jobTitle;
//    }
//
//    public String getCompany() {
//        return company;
//    }
//
//    public void setCompany(String company) {
//        this.company = company;
//    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

//    public String getEmailType() {
//        return emailType;
//    }
//
//    public void setEmailType(String emailType) {
//        this.emailType = emailType;
//    }
//
//    public String getEmailformattedType() {
//        return emailformattedType;
//    }
//
//    public void setEmailformattedType(String emailformattedType) {
//        this.emailformattedType = emailformattedType;
//    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

//    public String getPhonecanonicalForm() {
//        return phonecanonicalForm;
//    }
//
//    public void setPhonecanonicalForm(String phonecanonicalForm) {
//        this.phonecanonicalForm = phonecanonicalForm;
//    }
//
//    public String getPhoneType() {
//        return phoneType;
//    }
//
//    public void setPhoneType(String phoneType) {
//        this.phoneType = phoneType;
//    }
//
//    public String getPhoneformattedType() {
//        return phoneformattedType;
//    }
//
//    public void setPhoneformattedType(String phoneformattedType) {
//        this.phoneformattedType = phoneformattedType;
//    }
//
//    public String getIdWorker() {
//        return idWorker;
//    }
//
//    public void setIdWorker(String idWorker) {
//        this.idWorker = idWorker;
//    }
//
//    public String getNumYazran() {
//        return numYazran;
//    }
//
//    public void setNumYazran(String numYazran) {
//        this.numYazran = numYazran;
//    }
//
//    public String getIdProject() {
//        return idProject;
//    }
//
//    public void setIdProject(String idProject) {
//        this.idProject = idProject;
//    }
//
//    public String getCid() {
//        return cid;
//    }
//
//    public void setCid(String cid) {
//        this.cid = cid;
//    }
//
//    public int getCompInId() {
//        return compInId;
//    }
//
//    public void setCompInId(int compInId) {
//        this.compInId = compInId;
//    }
//
//    public String getCompName() {
//        return compName;
//    }
//
//    public void setCompName(String compName) {
//        this.compName = compName;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getPhone1() {
//        return phone1;
//    }
//
//    public void setPhone1(String phone1) {
//        this.phone1 = phone1;
//    }
//
//    public String getPhone2() {
//        return phone2;
//    }
//
//    public void setPhone2(String phone2) {
//        this.phone2 = phone2;
//    }
//
//    public String getPhone2Send() {
//        return phone2Send;
//    }
//
//    public void setPhone2Send(String phone2Send) {
//        this.phone2Send = phone2Send;
//    }
//
//    public String getEmail2() {
//        return email2;
//    }
//
//    public void setEmail2(String email2) {
//        this.email2 = email2;
//    }
//
//    public String getEmail2Send() {
//        return email2Send;
//    }
//
//    public void setEmail2Send(String email2Send) {
//        this.email2Send = email2Send;
//    }
//
//    public String getFax() {
//        return fax;
//    }
//
//    public void setFax(String fax) {
//        this.fax = fax;
//    }
//
//    public String getJobDescription() {
//        return jobDescription;
//    }
//
//    public void setJobDescription(String jobDescription) {
//        this.jobDescription = jobDescription;
//    }
//
//    public String getBroker() {
//        return broker;
//    }
//
//    public void setBroker(String broker) {
//        this.broker = broker;
//    }
//
//    public String getAhrayot() {
//        return ahrayot;
//    }
//
//    public void setAhrayot(String ahrayot) {
//        this.ahrayot = ahrayot;
//    }
//
//    public String getEkerot() {
//        return ekerot;
//    }
//
//    public void setEkerot(String ekerot) {
//        this.ekerot = ekerot;
//    }
//
//    public String getSendDailyMailDesc() {
//        return sendDailyMailDesc;
//    }
//
//    public void setSendDailyMailDesc(String sendDailyMailDesc) {
//        this.sendDailyMailDesc = sendDailyMailDesc;
//    }
//
//    public String getMatara() {
//        return matara;
//    }
//
//    public void setMatara(String matara) {
//        this.matara = matara;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inId != null ? inId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contacts)) {
            return false;
        }
        Contacts other = (Contacts) object;
        if ((this.inId == null && other.inId != null) || (this.inId != null && !this.inId.equals(other.inId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Contacts[ inId=" + inId + " ]";
    }
    
}
