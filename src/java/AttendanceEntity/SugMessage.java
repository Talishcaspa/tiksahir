/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AttendanceEntity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rivka
 */
@Entity
@Table(name = "erp.sug_message")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SugMessage.findAll", query = "SELECT s FROM SugMessage s")
    , @NamedQuery(name = "SugMessage.findByInId", query = "SELECT s FROM SugMessage s WHERE s.inId = :inId")
    , @NamedQuery(name = "SugMessage.findByUser", query = "SELECT s FROM SugMessage s WHERE s.user = :user")
    , @NamedQuery(name = "SugMessage.findByTaarihHahnassa", query = "SELECT s FROM SugMessage s WHERE s.taarihHahnassa = :taarihHahnassa")
    , @NamedQuery(name = "SugMessage.findByHafala", query = "SELECT s FROM SugMessage s WHERE s.hafala = :hafala")
    , @NamedQuery(name = "SugMessage.findByEmail", query = "SELECT s FROM SugMessage s WHERE s.email = :email")
    , @NamedQuery(name = "SugMessage.findBySms", query = "SELECT s FROM SugMessage s WHERE s.sms = :sms")
    , @NamedQuery(name = "SugMessage.findByRecorded", query = "SELECT s FROM SugMessage s WHERE s.recorded = :recorded")
    , @NamedQuery(name = "SugMessage.findByFax", query = "SELECT s FROM SugMessage s WHERE s.fax = :fax")
    , @NamedQuery(name = "SugMessage.findByEmailTitlle", query = "SELECT s FROM SugMessage s WHERE s.emailTitlle = :emailTitlle")
    , @NamedQuery(name = "SugMessage.findBySMSContent", query = "SELECT s FROM SugMessage s WHERE s.sMSContent = :sMSContent")
    , @NamedQuery(name = "SugMessage.findByRecordedContent", query = "SELECT s FROM SugMessage s WHERE s.recordedContent = :recordedContent")
    , @NamedQuery(name = "SugMessage.findByFaxContent", query = "SELECT s FROM SugMessage s WHERE s.faxContent = :faxContent")
    , @NamedQuery(name = "SugMessage.findByTaarihThilatSendMessage", query = "SELECT s FROM SugMessage s WHERE s.taarihThilatSendMessage = :taarihThilatSendMessage")
    , @NamedQuery(name = "SugMessage.findBySofSendMessage", query = "SELECT s FROM SugMessage s WHERE s.sofSendMessage = :sofSendMessage")
    , @NamedQuery(name = "SugMessage.findByTadirout", query = "SELECT s FROM SugMessage s WHERE s.tadirout = :tadirout")
    , @NamedQuery(name = "SugMessage.findBySMSnumber", query = "SELECT s FROM SugMessage s WHERE s.sMSnumber = :sMSnumber")
    , @NamedQuery(name = "SugMessage.findByFAXnumber", query = "SELECT s FROM SugMessage s WHERE s.fAXnumber = :fAXnumber")
    , @NamedQuery(name = "SugMessage.findByEmailadress", query = "SELECT s FROM SugMessage s WHERE s.emailadress = :emailadress")
    , @NamedQuery(name = "SugMessage.findByRecordToWho", query = "SELECT s FROM SugMessage s WHERE s.recordToWho = :recordToWho")
    , @NamedQuery(name = "SugMessage.findByDescription", query = "SELECT s FROM SugMessage s WHERE s.description = :description")
    , @NamedQuery(name = "SugMessage.findByFAXfn", query = "SELECT s FROM SugMessage s WHERE s.fAXfn = :fAXfn")
    , @NamedQuery(name = "SugMessage.findByFAXffn", query = "SELECT s FROM SugMessage s WHERE s.fAXffn = :fAXffn")
    , @NamedQuery(name = "SugMessage.findByVOICEfn", query = "SELECT s FROM SugMessage s WHERE s.vOICEfn = :vOICEfn")
    , @NamedQuery(name = "SugMessage.findByVOICEffn", query = "SELECT s FROM SugMessage s WHERE s.vOICEffn = :vOICEffn")
    , @NamedQuery(name = "SugMessage.findByEMAILfn", query = "SELECT s FROM SugMessage s WHERE s.eMAILfn = :eMAILfn")
    , @NamedQuery(name = "SugMessage.findByEMAILffn", query = "SELECT s FROM SugMessage s WHERE s.eMAILffn = :eMAILffn")
    , @NamedQuery(name = "SugMessage.findByEmailAtach1Fn", query = "SELECT s FROM SugMessage s WHERE s.emailAtach1Fn = :emailAtach1Fn")
    , @NamedQuery(name = "SugMessage.findByEmailAtach1Ffn", query = "SELECT s FROM SugMessage s WHERE s.emailAtach1Ffn = :emailAtach1Ffn")
    , @NamedQuery(name = "SugMessage.findByEmailAtach2Fn", query = "SELECT s FROM SugMessage s WHERE s.emailAtach2Fn = :emailAtach2Fn")
    , @NamedQuery(name = "SugMessage.findByEmailAtach2Ffn", query = "SELECT s FROM SugMessage s WHERE s.emailAtach2Ffn = :emailAtach2Ffn")
    , @NamedQuery(name = "SugMessage.findBySumContacts", query = "SELECT s FROM SugMessage s WHERE s.sumContacts = :sumContacts")
    , @NamedQuery(name = "SugMessage.findByInidGoogleDetails", query = "SELECT s FROM SugMessage s WHERE s.inidGoogleDetails = :inidGoogleDetails")})
public class SugMessage implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "in_id")
    private Integer inId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "user")
    private String user;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taarih_hahnassa")
    @Temporal(TemporalType.TIMESTAMP)
    private Date taarihHahnassa;
    @Column(name = "hafala")
    private Boolean hafala;
    @Column(name = "Email")
    private Boolean email;
    @Column(name = "SMS")
    private Boolean sms;
    @Column(name = "Recorded")
    private Boolean recorded;
    @Column(name = "FAX")
    private Boolean fax;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 16777215)
    @Column(name = "emailContent")
    private String emailContent;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "emailTitlle")
    private String emailTitlle;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "SMSContent")
    private String sMSContent;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "RecordedContent")
    private String recordedContent;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "faxContent")
    private String faxContent;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taarih_thilat_send_message")
    @Temporal(TemporalType.DATE)
    private Date taarihThilatSendMessage;
    @Basic(optional = false)
    @NotNull
    @Column(name = "sof_send_message")
    @Temporal(TemporalType.DATE)
    private Date sofSendMessage;
//    @Basic(optional = false)
//    @NotNull
//    @Column(name = "tadirout")
//    private int tadirout;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SMS_number")
    private int sMSnumber;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FAX_number")
    private int fAXnumber;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Email_adress")
    private int emailadress;
    @Basic(optional = false)
    @NotNull
    @Column(name = "record_to_who")
    private int recordToWho;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "description")
    private String description;
    @Size(max = 100)
    @Column(name = "FAX_fn")
    private String fAXfn;
    @Size(max = 250)
    @Column(name = "FAX_ffn")
    private String fAXffn;
    @Size(max = 100)
    @Column(name = "VOICE_fn")
    private String vOICEfn;
    @Size(max = 250)
    @Column(name = "VOICE_ffn")
    private String vOICEffn;
    @Size(max = 100)
    @Column(name = "EMAIL_fn")
    private String eMAILfn;
    @Size(max = 250)
    @Column(name = "EMAIL_ffn")
    private String eMAILffn;
    @Size(max = 100)
    @Column(name = "email_atach1_fn")
    private String emailAtach1Fn;
    @Size(max = 250)
    @Column(name = "email_atach1_ffn")
    private String emailAtach1Ffn;
    @Size(max = 100)
    @Column(name = "email_atach2_fn")
    private String emailAtach2Fn;
    @Size(max = 250)
    @Column(name = "email_atach2_ffn")
    private String emailAtach2Ffn;
    @Basic(optional = false)
    @NotNull
    @Column(name = "sum_contacts")
    private int sumContacts;
    @Size(max = 45)
    @Column(name = "inid_google_details")
    private String inidGoogleDetails;
    
    @OneToOne(cascade={CascadeType.PERSIST,CascadeType.REMOVE})
    @JoinColumn(name = "tadirout", referencedColumnName = "in_id")
    private Tadirut tadirout;
    
    @OneToMany(mappedBy = "sugMessageInid",cascade={CascadeType.PERSIST,CascadeType.REMOVE})
    private Collection<SugMessageContacts> sugMessageContacts;

    public SugMessage() {
    }

    public SugMessage(Integer inId) {
        this.inId = inId;
    }

    public SugMessage(Integer inId, String user, Date taarihHahnassa, String emailContent, String emailTitlle, String sMSContent, String recordedContent, String faxContent, Date taarihThilatSendMessage, Date sofSendMessage, Tadirut tadirout, int sMSnumber, int fAXnumber, int emailadress, int recordToWho, String description, int sumContacts) {
        this.inId = inId;
        this.user = user;
        this.taarihHahnassa = taarihHahnassa;
        this.emailContent = emailContent;
        this.emailTitlle = emailTitlle;
        this.sMSContent = sMSContent;
        this.recordedContent = recordedContent;
        this.faxContent = faxContent;
        this.taarihThilatSendMessage = taarihThilatSendMessage;
        this.sofSendMessage = sofSendMessage;
        this.tadirout = tadirout;
        this.sMSnumber = sMSnumber;
        this.fAXnumber = fAXnumber;
        this.emailadress = emailadress;
        this.recordToWho = recordToWho;
        this.description = description;
        this.sumContacts = sumContacts;
    }

    public Integer getInId() {
        return inId;
    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Date getTaarihHahnassa() {
        return taarihHahnassa;
    }

    public void setTaarihHahnassa(Date taarihHahnassa) {
        this.taarihHahnassa = taarihHahnassa;
    }

    public Boolean getHafala() {
        return hafala;
    }

    public void setHafala(Boolean hafala) {
        this.hafala = hafala;
    }

    public Boolean getEmail() {
        return email;
    }

    public void setEmail(Boolean email) {
        this.email = email;
    }

    public Boolean getSms() {
        return sms;
    }

    public void setSms(Boolean sms) {
        this.sms = sms;
    }

    public Boolean getRecorded() {
        return recorded;
    }

    public void setRecorded(Boolean recorded) {
        this.recorded = recorded;
    }

    public Boolean getFax() {
        return fax;
    }

    public void setFax(Boolean fax) {
        this.fax = fax;
    }

    public String getEmailContent() {
        return emailContent;
    }

    public void setEmailContent(String emailContent) {
        this.emailContent = emailContent;
    }

    public String getEmailTitlle() {
        return emailTitlle;
    }

    public void setEmailTitlle(String emailTitlle) {
        this.emailTitlle = emailTitlle;
    }

    public String getSMSContent() {
        return sMSContent;
    }

    public void setSMSContent(String sMSContent) {
        this.sMSContent = sMSContent;
    }

    public String getRecordedContent() {
        return recordedContent;
    }

    public void setRecordedContent(String recordedContent) {
        this.recordedContent = recordedContent;
    }

    public String getFaxContent() {
        return faxContent;
    }

    public void setFaxContent(String faxContent) {
        this.faxContent = faxContent;
    }

    public Date getTaarihThilatSendMessage() {
        return taarihThilatSendMessage;
    }

    public void setTaarihThilatSendMessage(Date taarihThilatSendMessage) {
        this.taarihThilatSendMessage = taarihThilatSendMessage;
    }

    public Date getSofSendMessage() {
        return sofSendMessage;
    }

    public void setSofSendMessage(Date sofSendMessage) {
        this.sofSendMessage = sofSendMessage;
    }

    
//    public int getTadirout() {
//        return tadirout;
//    }
//
//    public void setTadirout(int tadirout) {
//        this.tadirout = tadirout;
//    }
    
    public Tadirut getTadirout() {
        return tadirout;
    }
    
    public void setTadirout(Tadirut tadirout) {
        this.tadirout = tadirout;
    }

    public int getSMSnumber() {
        return sMSnumber;
    }

    public void setSMSnumber(int sMSnumber) {
        this.sMSnumber = sMSnumber;
    }

    public int getFAXnumber() {
        return fAXnumber;
    }

    public void setFAXnumber(int fAXnumber) {
        this.fAXnumber = fAXnumber;
    }

    public int getEmailadress() {
        return emailadress;
    }

    public void setEmailadress(int emailadress) {
        this.emailadress = emailadress;
    }

    public int getRecordToWho() {
        return recordToWho;
    }

    public void setRecordToWho(int recordToWho) {
        this.recordToWho = recordToWho;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFAXfn() {
        return fAXfn;
    }

    public void setFAXfn(String fAXfn) {
        this.fAXfn = fAXfn;
    }

    public String getFAXffn() {
        return fAXffn;
    }

    public void setFAXffn(String fAXffn) {
        this.fAXffn = fAXffn;
    }

    public String getVOICEfn() {
        return vOICEfn;
    }

    public void setVOICEfn(String vOICEfn) {
        this.vOICEfn = vOICEfn;
    }

    public String getVOICEffn() {
        return vOICEffn;
    }

    public void setVOICEffn(String vOICEffn) {
        this.vOICEffn = vOICEffn;
    }

    public String getEMAILfn() {
        return eMAILfn;
    }

    public void setEMAILfn(String eMAILfn) {
        this.eMAILfn = eMAILfn;
    }

    public String getEMAILffn() {
        return eMAILffn;
    }

    public void setEMAILffn(String eMAILffn) {
        this.eMAILffn = eMAILffn;
    }

    public String getEmailAtach1Fn() {
        return emailAtach1Fn;
    }

    public void setEmailAtach1Fn(String emailAtach1Fn) {
        this.emailAtach1Fn = emailAtach1Fn;
    }

    public String getEmailAtach1Ffn() {
        return emailAtach1Ffn;
    }

    public void setEmailAtach1Ffn(String emailAtach1Ffn) {
        this.emailAtach1Ffn = emailAtach1Ffn;
    }

    public String getEmailAtach2Fn() {
        return emailAtach2Fn;
    }

    public void setEmailAtach2Fn(String emailAtach2Fn) {
        this.emailAtach2Fn = emailAtach2Fn;
    }

    public String getEmailAtach2Ffn() {
        return emailAtach2Ffn;
    }

    public void setEmailAtach2Ffn(String emailAtach2Ffn) {
        this.emailAtach2Ffn = emailAtach2Ffn;
    }

    public int getSumContacts() {
        return sumContacts;
    }

    public void setSumContacts(int sumContacts) {
        this.sumContacts = sumContacts;
    }

    public String getInidGoogleDetails() {
        return inidGoogleDetails;
    }

    public void setInidGoogleDetails(String inidGoogleDetails) {
        this.inidGoogleDetails = inidGoogleDetails;
    }

    public Collection<SugMessageContacts> getSugMessageContacts() {
        return sugMessageContacts;
    }

    public void setSugMessageContacts(Collection<SugMessageContacts> sugMessageContacts) {
        this.sugMessageContacts = sugMessageContacts;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inId != null ? inId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SugMessage)) {
            return false;
        }
        SugMessage other = (SugMessage) object;
        if ((this.inId == null && other.inId != null) || (this.inId != null && !this.inId.equals(other.inId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.SugMessage[ inId=" + inId + " ]";
    }

    
}
