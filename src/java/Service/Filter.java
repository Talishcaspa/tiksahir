/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import Entity.ManagementUsers;
import Entity.ManagementUsersFacade;
import Entity.Users;
import Entity.UsersFacade;
import javax.ejb.EJB;
import javax.servlet.http.HttpSession;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author rivka
 */
@Provider
public class Filter implements ContainerRequestFilter {

    @EJB
    ManagementUsersFacade usersFacade;
@EJB
UsersFacade UsersFacade;
    public static final String AUTHENTICATION_HEADER = "Authorization";

    @Override
    public void filter(ContainerRequestContext requestContext) {

//        ManagementUsers user = usersFacade.getUserDetails("rivka", "zh26ZF33", "8543");
                
        String authCredentials = requestContext.getHeaderString(AUTHENTICATION_HEADER);

        if (authCredentials == null) {
            System.out.println("Rachel: header is null");
            throw new WebApplicationException(Response.Status.UNAUTHORIZED);
        }

//        AuthenticationService authenticationService = new AuthenticationService(usersFacade); 
        AuthenticationService authenticationService = new AuthenticationService(UsersFacade);

        Users user = authenticationService.authenticate(authCredentials);

        if (user.getInId() == -1) {//there is no such a user.
//        if (user.getInId().orElse(0)== -1) {//there is no such a user.  // my add or else

            System.out.println("Rachel :   this fell!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  ");
//            throw new WebApplicationException(Response.Status.BAD_GATEWAY);

            throw new WebApplicationException(Response.Status.UNAUTHORIZED);
        }

        requestContext.setProperty("user", user);

    }

}
