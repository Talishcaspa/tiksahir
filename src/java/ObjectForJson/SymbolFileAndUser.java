/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ObjectForJson;

/**
 *
 * @author rachel
 */
public class SymbolFileAndUser {
        
    private String symbol;
    
    private int workerId;
    
    private int year;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public int getWorkerId() {
        return workerId;
    }

    public void setWorkerId(int workerId) {
        this.workerId = workerId;
    }

    public SymbolFileAndUser() {
    }

    public SymbolFileAndUser(String symbol, int workerId, int year) {
        this.symbol = symbol;
        this.workerId = workerId;
        this.year=year;
    }

    
    
}
