package ObjectForJson;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import javax.persistence.Column;

/**
 *
 * @author rachel
 */
public class ManagementUsers {

    public ManagementUsers() {
    }

    private Integer inId;
    private String tz;
    private String firstName;
    private String lastName;
    private String userPass;
    private String pinCode;
    private String mail;
    private String phone;
    private Boolean digitalSignature;
    private Boolean approvalOfPolicies;
    private Boolean rightAssignment;
    private int inIdCommittee;

    public Boolean getPhotocopyOfIdentityCards() {
        return photocopyOfIdentityCards;
    }

    public void setPhotocopyOfIdentityCards(Boolean photocopyOfIdentityCards) {
        this.photocopyOfIdentityCards = photocopyOfIdentityCards;
    }
    private Boolean photocopyOfIdentityCards;


    public Boolean getDigitalSignature() {
        return digitalSignature;
    }

    public void setDigitalSignature(Boolean digitalSignature) {
        this.digitalSignature = digitalSignature;
    }

    public Boolean getApprovalOfPolicies() {
        return approvalOfPolicies;
    }

    public void setApprovalOfPolicies(Boolean approvalOfPolicies) {
        this.approvalOfPolicies = approvalOfPolicies;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    

    public Integer getInId() {
        return inId;
    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }

    public String getTz() {
        return tz;
    }

    public void setTz(String tz) {
        this.tz = tz;
    }

    public String getFullName() {
        return firstName;
    }

    public void setFullName(String firstName) {
        this.firstName = firstName;
    }

    public String getUserName() {
        return lastName;
    }

    public void setUserName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserPass() {
        return userPass;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

     public int getInIdCommittee() {
        return inIdCommittee;
    }

    public void setInIdCommittee(int inIdCommittee) {
        this.inIdCommittee = inIdCommittee;
    }
        
    public Boolean getRightAssignment() {
        return rightAssignment;
    }

    public void setRightAssignment(Boolean rightAssignment) {
        this.rightAssignment = rightAssignment;
    }
    
    public ManagementUsers(String tz, String firstName, String lastName, String userPass, String pinCode) {
        this.tz = tz;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userPass = userPass;
        this.pinCode = pinCode;
    }

    public ManagementUsers(String tz, int inId, String firstName, String lastName, String userPass, String pinCode) {
        this.tz = tz;
        this.inId = inId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userPass = userPass;
        this.pinCode = pinCode;

    }

    public ManagementUsers(String tz, int inId, String firstName, String lastName, String mail, String phone, boolean approvalOfPolicies, boolean digitalSignature, Boolean photocopyOfIdentityCards, Boolean rightAssignment,int inIdCommittee ) {
     
        this.tz = tz;
        this.inId = inId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.mail = mail;
        this.approvalOfPolicies= approvalOfPolicies;
        this.digitalSignature = digitalSignature;
        this.photocopyOfIdentityCards = photocopyOfIdentityCards;
        this.rightAssignment= rightAssignment;
        this.inIdCommittee = inIdCommittee;
    }
 public ManagementUsers(String tz, int inId, String firstName, String lastName, String mail, String phone,int inIdCommittee ) {
     
        this.tz = tz;
        this.inId = inId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.mail = mail;
        this.inIdCommittee = inIdCommittee;
    }

}
