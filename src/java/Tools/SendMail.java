/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tools;

import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * @author rachel
 */
public class SendMail {
    
    public static boolean Send( String toAddress,String subject1, String message1){
        
        String host = "smtp.sendgrid.net";
        String port = "2525";

        String mailFrom = "mokedApp";
        String password = "222trinity222";

        // message info
        String mailTo = toAddress;
        String mailToBcc = "rachel.becher@taxmail.co.il";

        String subject = subject1;
        String message = message1;
        
        try {
            SendMail(host, port, mailFrom, password, mailTo, mailToBcc,
                    subject, message, null);
            System.out.println("Email sent.");
return true;
        } catch (Exception ex) {
            System.out.println("Could not send email.");
            ex.printStackTrace();
return  false;
        }
    }
    

    public static void SendMail(String host, String port,
            final String nameFrom, final String password, String toAddress, String toAddressBcc,
            String subject, String message, String[] attachFiles)
            throws AddressException, MessagingException {
        // sets SMTP server properties
        Properties properties = new Properties();
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
//        properties.put("mail.user", userName);

//        properties.put("mail.password", password);

        // creates a new session with an authenticator
        Authenticator auth = new Authenticator() {
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("trinity222", "222trinity222");
            }
        };
        Session session = Session.getInstance(properties, auth);

        // creates a new e-mail message
        Message msg = new MimeMessage(session);

        msg.setFrom(new InternetAddress(nameFrom));
        InternetAddress[] toAddresses = {new InternetAddress(toAddress)};
        InternetAddress[] toAddressBcces = {new InternetAddress(toAddressBcc)};
        msg.setRecipients(Message.RecipientType.TO, toAddresses);
        msg.setRecipients(Message.RecipientType.BCC, toAddressBcces);
        
        msg.setFrom(new InternetAddress("mokedApp@gmail.com"));
        msg.setSubject(subject);
        msg.setSentDate(new Date());

        // creates message part
        MimeBodyPart messageBodyPart = new MimeBodyPart();
//        messageBodyPart.setContent(message, "text/html");
   messageBodyPart.setContent(message, "text/html;charset=UTF-8");
//      messageBodyPart.setContent("<img src=cid:/var/www/reports/worker_diff_taxes_reports/moduls/uploadFiles/87_1534150241090.JPG>", "text/html");
        // creates multi-part
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);

        // adds attachments
        if (attachFiles != null && attachFiles.length > 0) {
            for (String filePath : attachFiles) {
                MimeBodyPart attachPart = new MimeBodyPart();

                try {
                    attachPart.attachFile(filePath);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

                multipart.addBodyPart(attachPart);
            }
        }
        
//          // adds inline image attachments
//        if (mapInlineImages != null && mapInlineImages.size() > 0) {
//            Set<String> setImageID = mapInlineImages.keySet();
//             
//            for (String contentId : setImageID) {
//                MimeBodyPart imagePart = new MimeBodyPart();
//                imagePart.setHeader("Content-ID", "<" + contentId + ">");
//                imagePart.setDisposition(MimeBodyPart.INLINE);
//                 
//                String imageFilePath = mapInlineImages.get(contentId);
//                try {
//                    imagePart.attachFile(imageFilePath);
//                } catch (IOException ex) {
//                    ex.printStackTrace();
//                }
// 
//                multipart.addBodyPart(imagePart);
//            }
//        }

        // sets the multi-part as e-mail's content
        msg.setContent(multipart);

        // sends the e-mail
        Transport.send(msg);

    }

}
