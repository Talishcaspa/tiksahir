/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tools;

import Entity.ActivityDocumentation;
import Entity.DailyMail;
import Entity.DailyMailFacade;
import Entity.Users;
import Enum.EnumTypesOfActivity;
import Host.Host;
import static Resource.SendMail.sendSimpleMail;
import static Tools.SendMail.Send;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.RandomStringUtils;

/**
 *
 * @author rachel
 */
public class CreatePasswords {

    private Users user;
    private String passWord;

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public CreatePasswords() {

    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public void CreatePinCode(Users user, DailyMailFacade DailyMailFAC) {

        this.user = user;
        String pinCode = RandomStringUtils.randomAlphanumeric(6);

        // create date of create pin code
        user.setDateCreatedPinCode(new Date());
        user.setPinCode(pinCode);

        // לבינתיים
        String phone = user.getDetails().getPhone1();
        pinCode = phone.substring(phone.length() - 3);

        // שליחת MAIL
        DailyMail mail = new DailyMail();
        mail.setFromAddress("mokedApp@oweme.co.il");
        mail.setToAddress(user.getDetails().getEMail());
        mail.setCcArray("  ");
        mail.setBccArray("rachel.becher@taxmail.com");
        mail.setDateInsert(new Date());
        mail.setSubject("קוד אימות");
        mail.setFile("  ");
        mail.setStatus(0);
        mail.setError("  ");
        mail.setDateSend(new Date());
        mail.setMessage("<html><head><style>*{ color: #003366;font-family: verdana;}</style></head>"
                + "<body>"
                + "<br /><h3 >שלום " + user.getDetails().getFirstName() + ",</h3>"
                + "<p >נוצר לך קוד אימות חדש לאפליקציית שכיר</p>"
                + "<p>קוד האימות שלך הוא : " + pinCode + "<p>"
                + "<br />"
                + "<br />"
                + "<img src=\"http://35.195.156.199/lgn/worker_diff_taxes_reports/moduls/uploadFiles/logo_oweme\" alt=\\\"s5_logo\\\" style=\\\"float:right\\\" height=\"40\" width=\"150\">"
                + " </body>"
                + "</html>");
        try {
            Send(user.getDetails().getEMail(), mail.getSubject(), mail.getMessage());

        } catch (Exception ex) {
            Logger.getLogger(CreatePasswords.class.getName()).log(Level.SEVERE, null, ex);
        }

        DailyMailFAC.create(mail);

// שליחת SMS
        Security security = new Security();
        user = security.UpdateAnotherTrial(user);

        // log
        ActivityDocumentation activityDocumentation = new ActivityDocumentation(EnumTypesOfActivity.Sending_a_verification_code.getNumOfActivity(), user);
        activityDocumentation.setUsers(user);
        if (user.getListActivityDocumentation() == null) {
            user.setListActivityDocumentation(new ArrayList<ActivityDocumentation>());
        } else {
            user.getListActivityDocumentation().add(activityDocumentation);
        }
    }

    public void CreatePassword(Users user, DailyMailFacade DailyMailFAC) throws UnsupportedEncodingException {

//        this.user = user;
        passWord = RandomStringUtils.randomAlphabetic(3) + RandomStringUtils.randomNumeric(3);

        // create date of create pin code
        user.setDateCreatedPassword(new Date());

        user.setNumberOfTries(Optional.ofNullable(user.getNumberOfTries()).orElse(0) + 1);
        user.setLastTrialDate(new Date());

        // שליחת MAIL
        DailyMail mail = new DailyMail();
        mail.setFromAddress("mokedApp@oweme.co.il");
        mail.setToAddress(user.getDetails().getEMail());
        mail.setCcArray("  ");
        mail.setBccArray("  ");
        mail.setDateInsert(new Date());
        mail.setSubject("סיסמא חדשה");
        mail.setFile("  ");
        mail.setStatus(0);
        mail.setError("  ");
        mail.setDateSend(new Date());
        mail.setMessage("<html><head><style>*{ color: #003366;font-family: verdana;}</style></head>"
                + "<body>"
                + "<br /><h3 >שלום " + user.getDetails().getFirstName() + ",</h3>"
                + "<p >אפליקציית תיק שכיר יצרה לך סיסמה חדשה </p>"
                + "<p>הסיסמא שלך היא: " + passWord + "<p>"
                + "<br />"
                + "<br />"
                + "<img src=\"http://35.195.156.199/lgn/worker_diff_taxes_reports/moduls/uploadFiles/logo_oweme\" alt=\\\"s5_logo\\\" style=\\\"float:right\\\" height=\"40\" width=\"150\">"
                + " </body>"
                + "</html>");

        try {
            Send(user.getDetails().getEMail(), mail.getSubject(), mail.getMessage());

        } catch (Exception ex) {
            Logger.getLogger(CreatePasswords.class.getName()).log(Level.SEVERE, null, ex);
        }

        DailyMailFAC.create(mail);
        Security security = new Security();
        user = security.UpdateAnotherTrial(user);

        // log
        ActivityDocumentation activityDocumentation = new ActivityDocumentation(EnumTypesOfActivity.Sending_a_new_password.getNumOfActivity(), user);
        user.getListActivityDocumentation().add(activityDocumentation);

        System.out.println("Rachel :    new  password     :" + passWord);

        String testString = passWord;
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(testString.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String hashtext = number.toString(16);
            user.setPassword(hashtext);

            System.out.println("Rachel :   hashtext   :  " + hashtext);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(CreatePasswords.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.user = user;
    }

    public String convertPasswordToMd5(String originalPassword) {

        String newPassword = "";

        System.out.println("Rachel :   original  Password   :  " + originalPassword);

        MessageDigest md;

        try {
            md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(originalPassword.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            newPassword = number.toString(16);

            System.out.println("Rachel :   new  Password   :  " + newPassword);

        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(CreatePasswords.class.getName()).log(Level.SEVERE, null, ex);
        }

        return newPassword;

    }

    public boolean SecurityCheck(Users user) {

        this.user = user;
        boolean check;

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime lastLogin = LocalDateTime.ofInstant(Optional.ofNullable(user.getLastTrialDate()).orElse(new Date("01/01/10 00:00:00")).toInstant(), ZoneId.systemDefault());

        if (Optional.ofNullable(user.getNumberOfTries()).orElse(0) > 4 && lastLogin.plusMinutes(30).isAfter(now)) {// If there have not been 4 attempts yet and 3 minutes have not passed since the last attempt
            // משנה סטטוס שצריך לחכות חצי שעה
            check = Boolean.FALSE;

        } else {

            check = Boolean.TRUE;
        }

        return check;

    }
}
