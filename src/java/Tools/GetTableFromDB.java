/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tools;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 * @author rachel
 */
public class GetTableFromDB {

    public GetTableFromDB() {
    }

    public List<Map<String, Object>> GetTableFromDB(String originalTable) {
        System.out.println("start " + LocalDateTime.now());
        Connection con = null;
        InitialContext ctx;
        ResultSet result2 = null;
        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();

        try {
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("jdbc/myDatasource152");
            con = ds.getConnection();
        } catch (NamingException | SQLException ex) {
            Logger.getLogger(GetTableFromDB.class.getName()).log(Level.SEVERE, null, ex);
        }

        String request2 = "SELECT * FROM " + originalTable;
        try {
            // Statement of connection
            Statement stat = con.createStatement();

            // Names of the selected columns
            System.out.println(request2);
            result2 = stat.executeQuery(request2);
            System.out.println("after result sql " + LocalDateTime.now());

            Map<String, Object> row = null;

            ResultSetMetaData metaData = result2.getMetaData();
            Integer columnCount = metaData.getColumnCount();

            while (result2.next()) {
                row = new HashMap<String, Object>();
                for (int i = 1; i <= columnCount; i++) {
                    row.put(metaData.getColumnName(i), result2.getObject(i));
                }
                resultList.add(row);
            }

            stat.close();
        } catch (SQLException ex) {
            Logger.getLogger(GetTableFromDB.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(GetTableFromDB.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("end " + LocalDateTime.now());
        System.out.println("end " + result2);

        return resultList;
    }
}
