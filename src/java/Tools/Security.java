/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tools;

import Entity.ActivityDocumentation;
import Entity.Users;
import Entity.UsersFacade;
import Enum.EnumTypesOfActivity;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;

/**
 *
 * @author rachel
 */
public class Security {

    Users user;
    UsersFacade UsersFAC;

    public Security() {
    }

    public Users UpdateAnotherTrial(Users user) {

        user.setNumberOfTries(Optional.ofNullable(user.getNumberOfTries()).orElse(0) + 1);
        user.setLastTrialDate(new Date());

        return user;
    }

    public boolean SecurityCheck(Users user, UsersFacade UsersFAC) {

        this.user = user;
        boolean check = Boolean.TRUE;

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime lastLogin = LocalDateTime.ofInstant(Optional.ofNullable(user.getLastTrialDate()).orElse(new Date("01/01/10 00:00:00")).toInstant(), ZoneId.systemDefault());

        System.out.println("Rachel :  user.getNumberOfTries() " + user.getNumberOfTries());
        System.out.println("Rachel :   lastLogin  : " + lastLogin);
        System.out.println("Rachel :  lastLogin.plusMinutes(3) :  " + lastLogin.plusMinutes(3));
        System.out.println("Rachel : now :  " + now);

        if (lastLogin.plusMinutes(30).isBefore(now)) {
            user.setNumberOfTries(0);
            check = Boolean.TRUE;
        }
        //        else if(Optional.ofNullable(user.getNumberOfTries()).orElse(0) > 4 && lastLogin.plusMinutes(30).isAfter(now)) {// If there have not been 4 attempts yet and 3 minutes have not passed since the last attempt
        else if (Optional.ofNullable(user.getNumberOfTries()).orElse(0) > 4) {// If there have not been 4 attempts yet and 3 minutes have not passed since the last attempt
            UpdateAnotherTrial(user);
// משנה סטטוס שצריך לחכות חצי שעה
            check = Boolean.FALSE;
            // log
            ActivityDocumentation activityDocumentation = new ActivityDocumentation(EnumTypesOfActivity.The_number_of_attempts_has_to_wait_half_an_hour.getNumOfActivity(), user);
            activityDocumentation.setUsers(user);
            user.getListActivityDocumentation().add(activityDocumentation);

        } else {

            check = Boolean.TRUE;
        }

        UsersFAC.edit(user);

        return check;

    }
    
    public Users resetAttempts(Users user){
 
        user.setNumberOfTries(0);
        user.setLastTrialDate(new Date());
    return user;
    }

}
