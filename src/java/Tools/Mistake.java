/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tools;

/**
 *
 * @author rachel
 */
public class Mistake {
    
    
    // num of error
    private Integer numError = 0;
    // Description of the error
    private String DescriptionError = "success :) ";

    public Integer getnumError() {
        return numError;
    }

    public void setnumError(Integer numError) {
        this.numError = numError;
    }

    public String getDescriptionError() {
        return DescriptionError;
    }

    public void setDescriptionError(String DescriptionError) {
        this.DescriptionError = DescriptionError;
    }

    public Mistake() {
    }

    public Mistake(Integer numError, String DescriptionError) {

        this.DescriptionError = DescriptionError;
        this.numError = numError;
    }
}
